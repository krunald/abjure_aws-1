<?php

namespace Database\Seeders;

use App\Models\Hospital\Onboarding\HospitalSpecialistAvailable;
use Illuminate\Database\Seeder;

class HospitalAvailableSpecialitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = ['intensivist', 'cardiology', 'nephrology', 'neurology', 'Internal Medicine', 'Gastroenterology', 'General Surgery', 'Orthopedic Surgery', 'Interventional Radiology', 'anesthesiology'];

        $i = 1;
        foreach ($data as $d) {
            HospitalSpecialistAvailable::create([
                'speciality' => $d,
                'status' => 1,
                'order' => $i
            ]);
            $i++;
        }
    }
}
