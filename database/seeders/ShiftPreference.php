<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class ShiftPreference extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = ['DAY SHIFT', 'NIGHT SHIFT', '24HR SHIFT'];

        $i = 1;
        foreach ($data as $d) {
            \App\Models\Physician\Account\ShiftPreference::create([
                'shift' => $d,
                'status' => 1,
                'order' => $i
            ]);
            $i++;
        }
    }
}
