<?php

namespace Database\Seeders;

use App\Models\Speciality;
use Illuminate\Database\Seeder;

class SpecialitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['name' => 'Internship'],
            ['name' => 'Internship and residency'],
            ['name' => 'Residency'],
            ['name' => 'Fellowship']
        ];
        collect($data)->map(function ($record) {
            Speciality::create($record);
        });

    }
}
