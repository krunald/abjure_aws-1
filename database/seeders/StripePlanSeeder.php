<?php

namespace Database\Seeders;

use App\Models\StripePlans;
use Illuminate\Database\Seeder;

class StripePlanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'product_id' => 'prod_LnoUh8T8hI4gbk',
                'name' => 'Premium',
                'default_price' => 'price_1L6CrDKrvmJx6ecLbV97UwHK',
                'interval' => 'month',
                'unit_amount' => '2000',
                'unit_amount_decimal' => '20.00',
                'description' => 'Premium Plan'
            ],
            [
                'product_id' => 'prod_LzmbY7JXTxEUkl',
                'name' => 'Daily',
                'default_price' => 'price_1LHn27KrvmJx6ecLk9HzaEhO',
                'interval' => 'day',
                'unit_amount' => '1000',
                'unit_amount_decimal' => '10.00',
                'description' => 'Daily Plan'
            ]
        ];

        collect($data)->map(function ($record) {
            StripePlans::create($record);
        });
    }
}
