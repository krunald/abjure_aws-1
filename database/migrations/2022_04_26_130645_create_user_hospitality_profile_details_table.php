<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserHospitalityProfileDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_hospitality_profile_details', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id')->index('fk_user_hospitality_profile_details_1_idx');
            $table->tinyInteger('icu_management');
            $table->string('procedures')->index();
            $table->boolean('max_desired_patient_load_flexible')->nullable();
            $table->integer('max_desired_patient_load_numbers')->nullable();
            $table->tinyInteger('high_volume_care_part_of_practice');
            $table->timestamps();

            $table->foreign('user_id', 'fk_user_hospitality_profile_details_user_id')
                ->references('id')->on('users')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_hospitality_profile_details');
    }
}
