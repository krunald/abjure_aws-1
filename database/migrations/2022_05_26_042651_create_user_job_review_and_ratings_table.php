<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserJobReviewAndRatingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_job_review_and_ratings', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_jobs_id')->index();
            $table->double('rating');
            $table->text('experience_with_doctor')->nullable();
            $table->enum('quality_service_by_doctor',['EXCELLENT','VERY_GOOD','SATISFACTORY','BELOW_EXPECTATION','POOR'])->nullable();
            $table->text('doctors_availability')->nullable();
            $table->enum('likely_to_recommend_doctor',['HIGLY_RECOMMEND','RECOMMEND','NEUTRAL','WILL_NOT_RECOMMEND'])->nullable();
            $table->timestamps();

            $table->foreign('user_jobs_id')
                ->references('id')->on('user_jobs')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_job_review_and_ratings');
    }
}
