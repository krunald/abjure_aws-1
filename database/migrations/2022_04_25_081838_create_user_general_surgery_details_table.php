<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserGeneralSurgeryDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_general_surgery_details', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id')->index('fk_user_skills_details_1_idx');
            $table->tinyInteger('cholecstectomies')->nullable();
            $table->tinyInteger('appendectomies')->nullable();
            $table->tinyInteger('hernia_repairs')->nullable();
            $table->tinyInteger('robotic_surgery')->nullable();
            $table->tinyInteger('cesarean_section')->nullable();
            $table->tinyInteger('egd_and_colonoscopy')->nullable();
            $table->tinyInteger('vascular_access')->nullable();
            $table->enum('trauma_center_level', ['1', '2', '3', '4', '5'])->nullable();
            $table->timestamps();

            $table->foreign('user_id', 'fk_user_skills_details_user_id')
                ->references('id')->on('users')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_general_surgery_details');
    }
}
