<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_details', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id')->index('user_details_user_id_foreign');
            $table->bigInteger('phone_no')->nullable();
            $table->string('stripe_user_id')->nullable();
            $table->string('stripe_customer_id')->nullable();
            $table->tinyInteger('is_stripe_connected')->default('0')->comment('0 = not connected to stripe, 1 = connected to stripe');
            $table->tinyInteger('is_stripe_verify')->default('0')->comment('0 = not verify to stripe, 1 = verify to stripe');
            $table->tinyInteger('is_first')->default(0)->comment('0 = first time, 1 = not first time');
            $table->tinyInteger('is_verified')->default('0')->comment('0 = not verified, 1 = verified');
            $table->tinyInteger('is_approved')->default('0')->comment('0 = not approved, 1 = approved');
            $table->tinyInteger('status')->default('1')->comment('0 = inactive, 1 = active');
            $table->tinyInteger('is_google_sync')->default('0')->comment('0 = no, 1 = yes');
            $table->tinyInteger('is_outlook_sync')->default('0')->comment('0 = no, 1 = yes');
            $table->tinyInteger('is_apple_sync')->default('0')->comment('0 = no, 1 = yes');
            $table->timestamps();

            $table->foreign('user_id','user_details_user_id_foreign')
                ->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_details');
    }
}
