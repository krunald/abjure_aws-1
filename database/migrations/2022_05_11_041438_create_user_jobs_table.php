<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_jobs', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id')->index('user_jobs_user_id_foreign');
            $table->unsignedBigInteger('job_id')->index('user_jobs_job_id_foreign');
            $table->enum('application_status', ['APPLIED', 'SCREENING', 'INTERVIEW', 'APPROVED', 'OFFER_APPROVED', 'REJECTED', 'COMPLETED'])->default('APPLIED');
            $table->double('user_rating')->nullable();
            $table->text('pros_with_jobs')->nullable();
            $table->text('cons_with_jobs')->nullable();
            $table->boolean('status')->default('1')->comment('0 = inactive, 1 = active');
            $table->timestamps();

            $table->foreign('job_id', 'user_jobs_job_id_foreign')
                ->references('id')->on('jobs')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('user_id', 'user_jobs_user_id_foreign')
                ->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_jobs');
    }
}
