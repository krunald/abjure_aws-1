<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class AlterTableUsersChangeExpertiseEnum extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("ALTER TABLE `users` CHANGE `expertise` `expertise` ENUM('HOSPITALIST','GENERAL','GENERAL_SURGERY') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL;");
        DB::statement("UPDATE `users` SET `expertise` = 'GENERAL_SURGERY' WHERE `users`.`expertise` = 'GENERAL';");
        DB::statement("ALTER TABLE `users` CHANGE `expertise` `expertise` ENUM('HOSPITALIST','GENERAL_SURGERY') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("ALTER TABLE `users` CHANGE `expertise` `expertise` ENUM('HOSPITALIST','GENERAL','GENERAL_SURGERY') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL;");
        DB::statement("UPDATE `users` SET `expertise` = 'GENERAL' WHERE `users`.`expertise` = 'GENERAL_SURGERY';");
        DB::statement("ALTER TABLE `users` CHANGE `expertise` `expertise` ENUM('HOSPITALIST','GENERAL') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL;");
    }
}
