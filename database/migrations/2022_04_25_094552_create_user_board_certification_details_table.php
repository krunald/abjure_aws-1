<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserBoardCertificationDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_board_certification_details', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id')->index('fk_user_board_certification_details_user_id_foreign_key_idx');
            $table->tinyInteger('are_you_board_certified')->comment('1=yes,0=no');
            $table->string('issuer_name')->nullable();
            $table->string('speciality')->nullable();
            $table->date('date_certified')->nullable();
            $table->date('date_recertified')->nullable();
            $table->date('expiration_date')->nullable();
            $table->timestamps();

            $table->foreign('user_id', 'fk_user_board_certification_details_user_id_foreign_key_idx')
                ->references('id')->on('users')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_board_certification_details');
    }
}
