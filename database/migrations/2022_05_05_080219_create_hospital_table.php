<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateHospitalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hospital', function (Blueprint $table) {
            $table->id();
            $table->uuid('uuid')->unique();
            $table->string('email');
            $table->string('name');
            $table->string('licence_number');
            $table->string('address');
            $table->string('contact_person');
            $table->string('contact_person_number');
            $table->string('contact_number')->nullable();
            $table->string('password');

            $table->boolean('is_verified')->default(0);

            $table->boolean('is_free_user')->nullable();
            $table->timestamp('free_trial_starts_at')->nullable();
            $table->timestamp('free_trial_ends_at')->nullable();
            $table->boolean('is_free_plan_expired')->default(0);


            $table->unsignedBigInteger('stripe_subscription_id')->nullable()->index();
            $table->string('stripe_id')->nullable()->index();
            $table->string('pm_type')->nullable();
            $table->string('pm_last_four', 4)->nullable();
            $table->timestamp('trial_ends_at')->nullable();

            $table->boolean('status')->default(1);
            $table->timestamps();

            $table->foreign('stripe_subscription_id')->references('id')->on('subscriptions')->onDelete('CASCADE')->onUpdate('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hospital');
    }
}
