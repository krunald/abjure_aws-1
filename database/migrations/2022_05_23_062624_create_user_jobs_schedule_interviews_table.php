<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserJobsScheduleInterviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_jobs_schedule_interviews', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_job_id')->index();
            $table->unsignedBigInteger('user_id')->index();
            $table->unsignedBigInteger('hospital_id')->index();
            $table->date('interview_date');
            $table->time('interview_time');
            $table->enum('interview_type', ['PORTAL', 'OTHER']);
            $table->enum('other_interview_type', ['AUDIO_CALL', 'VIDEO_CALL', 'IN_PERSON'])->nullable();
            $table->text('video_call_link')->nullable();
            $table->timestamps();

            $table->foreign('hospital_id')
                ->references('id')->on('hospital')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('user_job_id')
                ->references('id')->on('user_jobs')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_jobs_schedule_interviews');
    }
}
