<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserClinicalCertificationDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_clinical_certification_details', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id')->index('fk_user_clinical_certification_details_user_id_foreign_key_id');
            $table->tinyInteger('bls_certification')->nullable();
            $table->date('bls_certification_expiration')->nullable();
            $table->tinyInteger('acls_certification')->nullable();
            $table->date('acls_certification_expiration')->nullable();
            $table->tinyInteger('atls_certification')->nullable();
            $table->date('atls_certification_expiration')->nullable();
            $table->tinyInteger('pals_certification')->nullable();
            $table->date('pals_certification_expiration')->nullable();
            $table->timestamps();

            $table->foreign('user_id', 'fk_user_clinical_certification_details_user_id_foreign_key_idx')
                ->references('id')->on('users')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_clinical_certification_details');
    }
}
