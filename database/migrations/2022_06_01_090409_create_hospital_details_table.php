<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHospitalDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hospital_details', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('hospital_id')->index();
            $table->string('stripe_user_id')->nullable();
            $table->string('stripe_customer_id')->nullable();
            $table->boolean('is_stripe_connected')->default(0);
            $table->boolean('is_stripe_verify')->default(0);
            $table->boolean('is_first')->default(0);
            $table->boolean('is_verified')->default(0);
            $table->boolean('is_approved')->default(0);
            $table->boolean('is_google_sync')->default(0);
            $table->boolean('is_outlook_sync')->default(0);
            $table->boolean('is_apple_sync')->default(0);
            $table->boolean('status')->default('1');
            $table->timestamps();

            $table->foreign('hospital_id')->references('id')->on('hospital')->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hospital_details');
    }
}
