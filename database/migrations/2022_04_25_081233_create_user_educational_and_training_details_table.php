<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserEducationalAndTrainingDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_educational_and_training_details', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id')->index('fk_educational_and_training_details_1_id');
            $table->enum('type', ["UNIVERSITY", "SCHOOL", "MEDICAL_SCHOOL", "FIFTHWAY", "TRAINING"])->nullable();
            $table->unsignedBigInteger('training_specialty')->nullable()->index('fk_educational_and_training_details_speciality_id_idx');
            $table->string('name')->nullable();
            $table->string('address')->nullable();
            $table->date('start_date')->nullable();
            $table->date('end_date')->nullable();
            $table->timestamps();

            $table->foreign('user_id', 'fk_educational_and_training_details_1_idx')
                ->references('id')->on('users')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('training_specialty', 'fk_educational_and_training_details_speciality_id_idx')
                ->references('id')->on('specialities')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_educational_and_training_details');
    }
}
