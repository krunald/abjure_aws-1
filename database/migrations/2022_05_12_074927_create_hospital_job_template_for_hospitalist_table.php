<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHospitalJobTemplateForHospitalistTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('hospital_job_template_for_hospitalist');
        Schema::create('hospital_job_template_for_hospitalist', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('template_id')->index();
            $table->boolean('family_practice_physicians_apply');
            $table->smallInteger('hospital_census');
            $table->smallInteger('avg_admissions_per_provider_by_day');
            $table->smallInteger('avg_er_admissions_per_provider_by_night');
            $table->smallInteger('expected_patient_load');
            $table->smallInteger('hospitalist_in_day');
            $table->smallInteger('hospitalist_in_night');
            $table->smallInteger('mid_levels_in_day');
            $table->smallInteger('mid_levels_in_night');
            $table->boolean('swing_shift_available');
            $table->enum('backup_plan', ['NO_MORE_ADMISSION_ALLOWED', 'BACKUP_PROVIDER_AVAILABLE', 'POIPLFAC', 'PREAPLAC', 'PRTIPLWC',]);
            $table->boolean('mid_level_available');
            $table->enum('mid_level_available_for_shift', ['DAY', 'NIGHT', 'BOTH'])->nullable();
            $table->enum('runs_code', ['ER_PHYSICIAN', 'INTENSIVIST', 'HOSPITALIST']);
            $table->boolean('respiratory_therapist');
            $table->boolean('respiratory_therapist_allowed_intubate')->nullable();
            $table->text('procedures_required');
            $table->enum('contract_period_type', ['SHORT_TERM', 'LONG_TERM']);
            $table->boolean('flexible_shift_scheduling');
            $table->timestamps();

            $table->foreign('template_id')
                ->references('id')->on('hospital_job_templates')
                ->onDelete('CASCADE')
                ->onUpdate('CASCADE');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hospital_job_template_for_hospitalist');
    }
}
