<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserReferencesDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_references_details', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id')->index('fk_user_references_details_user_id_foreign_key_id');
            $table->string('name')->nullable();
            $table->string('address')->nullable();
            $table->string('speciality')->nullable();
            $table->bigInteger('contact_no')->nullable();
            $table->string('email')->nullable();
            $table->string('fax_no')->nullable();
            $table->timestamps();

            $table->foreign('user_id', 'fk_user_references_details_user_id_foreign_key_idx')
                ->references('id')->on('users')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_references_details');
    }
}
