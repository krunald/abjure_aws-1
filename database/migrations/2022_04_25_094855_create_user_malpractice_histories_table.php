<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserMalpracticeHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_malpractice_histories', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id')->index('fk_user_malpractice_history_user_id_foreign_key_idx');
            $table->tinyInteger('ever_denied_professional_liability_insurance')->nullable();
            $table->tinyInteger('claim_suits_settlements_raised')->nullable();
            $table->date('date_of_incident')->nullable();
            $table->string('location')->nullable();
            $table->string('allegation')->nullable();
            $table->enum('allegation_status',['DISMISSED','SETTLED','JUDGEMENT','PENDING'])->nullable();
            $table->string('settlement_amount')->nullable();
            $table->tinyInteger('tofile_or_currently_pending_suits_claims')->nullable();
            $table->timestamps();

            $table->foreign('user_id', 'fk_user_malpractice_history_user_id_foreign_key_id')
                ->references('id')->on('users')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_malpractice_histories');
    }
}
