<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHospitalJobTemplateForGeneralSurgeryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('hospital_job_template_for_general_surgery');
        Schema::create('hospital_job_template_for_general_surgery', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('template_id')->index();
            $table->boolean('is_verified_clinic')->default(1);
            $table->tinyInteger('clinic_days_per_week')->nullable();
            $table->tinyInteger('avg_no_of_patients_per_day')->nullable();
            $table->string('emr_used_in_clinic')->nullable();
            $table->tinyInteger('avg_no_of_surgeries_per_or_day');
            $table->enum('anesthesia_care', ['ANESTHESIOLOGIST', 'CRNA', 'BOTH']);
            $table->boolean('upper_lower_endoscopy')->default(1);
            $table->enum('trauma_center_level', ['1', '2', '3', '4', '5']);
            $table->smallInteger('avg_no_of_trauma_patients_per_month');
            $table->boolean('c_section')->default(1);
            $table->boolean('dialysis_access')->default(1);
            $table->enum('dialysis_access_value', ['CATHETER_ONLY', 'CATHETER', 'AV_GRAFT/FISTULA'])->nullable();
            $table->timestamps();

            $table->foreign('template_id')
                ->references('id')->on('hospital_job_templates')
                ->onDelete('CASCADE')
                ->onUpdate('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hospital_job_template_for_general_surgery');
    }
}
