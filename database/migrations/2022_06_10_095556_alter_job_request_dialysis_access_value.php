<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class AlterJobRequestDialysisAccessValue extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('job_details_general_surgeons', function (Blueprint $table) {
            if (Schema::hasColumn('job_details_general_surgeons', 'dialysis_access_value')) {
                DB::statement("ALTER TABLE `job_details_general_surgeons` CHANGE `dialysis_access_value` `dialysis_access_value` ENUM('CATHETER_ONLY','AV_GRAFT/FISTULA','CATHETER','AV_GRAFT') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL;");
                DB::statement("UPDATE `job_details_general_surgeons` SET `dialysis_access_value` = 'AV_GRAFT/FISTULA' WHERE `job_details_general_surgeons`.`dialysis_access_value` = 'AV_GRAFT';");
                DB::statement("ALTER TABLE `job_details_general_surgeons` CHANGE `dialysis_access_value` `dialysis_access_value` ENUM('CATHETER_ONLY','AV_GRAFT/FISTULA','CATHETER') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL;");
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('job_details_general_surgeons', 'dialysis_access_value')) {
            DB::statement("ALTER TABLE `job_details_general_surgeons` CHANGE `dialysis_access_value` `dialysis_access_value` ENUM('CATHETER_ONLY','AV_GRAFT/FISTULA','CATHETER','AV_GRAFT') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL;");
            DB::statement("UPDATE `job_details_general_surgeons` SET `dialysis_access_value` = 'AV_GRAFT' WHERE `job_details_general_surgeons`.`dialysis_access_value` = 'AV_GRAFT/FISTULA';");
            DB::statement("ALTER TABLE `job_details_general_surgeons` CHANGE `dialysis_access_value` `dialysis_access_value` ENUM('CATHETER_ONLY','AV_GRAFT','CATHETER') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL;");
        }
    }
}
