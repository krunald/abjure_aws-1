<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class AlterJobDetailsGeneralSurgeonsDialysisAccessValueEnum extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('job_details_general_surgeons', function () {
            if (Schema::hasColumn('job_details_general_surgeons', 'dialysis_access_value')) {
                DB::statement("ALTER TABLE `job_details_general_surgeons` CHANGE `dialysis_access_value` `dialysis_access_value` ENUM('CATHETER_ONLY','AV_GRAFT/FISTULA','CATHETER','BOTH') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL;");
            }
        });
        if (Schema::hasColumn('hospital_job_template_for_general_surgery', 'dialysis_access_value')) {
            DB::statement("ALTER TABLE `hospital_job_template_for_general_surgery` CHANGE `dialysis_access_value` `dialysis_access_value` ENUM('CATHETER_ONLY','AV_GRAFT/FISTULA','CATHETER','BOTH') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL;");
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
