<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserProfessionalDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_professional_details', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id')->index('fk_user_professional_details_1_idx');
            $table->tinyInteger('ever_convicted')->nullable()->default(0)->comment('0=no,1=yes');
            $table->string('ever_convicted_explanation')->nullable();
            $table->tinyInteger('ever_diminished_clinical_privileges')->nullable()->default(0)->comment('0=no,1=yes');
            $table->string('ever_diminished_clinical_privileges_explanation')->nullable();
            $table->tinyInteger('ever_license_certificate_revoked')->nullable()->default(0)->comment('0=no,1=yes');
            $table->string('ever_license_certificate_revoked_explanation')->nullable();
            $table->tinyInteger('participation_revoked')->nullable()->default(0)->comment('0=no,1=yes');
            $table->string('participation_revoked_explanation')->nullable();
            $table->tinyInteger('alcoholism_or_substance_abuse')->nullable()->default(0)->comment('0=no,1=yes');
            $table->string('alcoholism_or_substance_abuse_explanation')->nullable();
            $table->tinyInteger('seeked_medical_treatment')->nullable()->default(0)->comment('0=no,1=yes');
            $table->string('seeked_medical_treatment_explanation')->nullable();
            $table->tinyInteger('health_condition_affecting_medical_staff_duties')->nullable()->default(0)->comment('0=no,1=yes');
            $table->string('health_condition_affecting_medical_staff_duties_explanation')->nullable();
            $table->tinyInteger('use_of_illegal_substance')->default(0)->comment('0=no,1=yes');
            $table->string('use_of_illegal_substance_explanation')->nullable();
            $table->timestamps();

            $table->foreign('user_id', 'fk_user_professional_details_1_idx')
                ->references('id')->on('users')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_professional_details');
    }
}
