<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHospitalHighlightsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hospital_highlights', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('hospital_id')->index();
            $table->smallInteger('number_of_beds')->unsigned()->nullable();
            $table->smallInteger('icu_beds')->unsigned()->nullable();
            $table->string('emr_used_for')->nullable();
            $table->integer('annual_er_visits')->unsigned()->nullable();
            $table->enum('paging_system', ['IN_HOUSE_MESSAGING_APPLICATION', 'PHYSICIAN_CELL_PHONE', 'PAGER']);
            $table->enum('icu_status', ['OPEN', 'CLOSED']);
            $table->boolean('has_stroke_center');
            $table->boolean('pci_available');
            $table->tinyInteger('trauma_center_level');
            $table->boolean('robotic_surgery_platform_available');
            $table->boolean('hemodialysis');
            $table->enum('consultant_accessibility_after_working_hours', ['EXCELLENT', 'GOOD', 'SATISFACTORY', 'BELOW_EXPECTATIONS']);
            $table->smallInteger('credentialing_length')->unsigned()->nullable();
            $table->boolean('credentialing_available');
            $table->smallInteger('time_taken_for_privileges')->unsigned()->nullable();

            $table->timestamps();

            $table->foreign('hospital_id')->references('id')->on('hospital')->onDelete('CASCADE')->onUpdate('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hospital_highlights');
    }
}
