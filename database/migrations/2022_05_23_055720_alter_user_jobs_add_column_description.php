<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class AlterUserJobsAddColumnDescription extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_jobs', function (Blueprint $table) {
            if (!Schema::hasColumn('user_jobs', 'description')) {
                $table->text('description')->after('application_status')->nullable();
            }
        });
        Schema::table('jobs', function (Blueprint $table) {
            if (Schema::hasColumn('jobs', 'experience_required')) {
                $table->dropColumn('experience_required');
            }
        });
        DB::statement("ALTER TABLE `job_payments` CHANGE `amount` `amount` DOUBLE NULL DEFAULT NULL;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_jobs', function (Blueprint $table) {
            if (Schema::hasColumn('user_jobs', 'description')) {
                $table->dropColumn('description');
            }
        });
    }
}
