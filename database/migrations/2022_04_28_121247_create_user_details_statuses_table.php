<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserDetailsStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_details_statuses', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id')->index('fk_user_details_user_id_foreign_key_idx');
            $table->tinyInteger('personal_details')->nullable();
            $table->tinyInteger('education_and_training')->nullable();
            $table->tinyInteger('certification_and_license')->nullable();
            $table->tinyInteger('work_history')->nullable();
            $table->tinyInteger('disciplinary_actions')->nullable();
            $table->tinyInteger('malpractice_history_details')->nullable();
            $table->tinyInteger('user_references')->nullable();
            $table->tinyInteger('profile_expertise_details')->nullable();
            $table->timestamps();

            $table->foreign('user_id', 'fk_user_details_user_id_foreign_key_idx')
                ->references('id')->on('users')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_details_statuses');
    }
}
