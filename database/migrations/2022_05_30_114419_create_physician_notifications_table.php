<?php

use App\Utils\AppConstant;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePhysicianNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('physician_notifications', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('sender_id')->index()->comment('Hospital id');
            $table->unsignedBigInteger('receiver_id')->index()->comment('User/Physician id');
            $table->string('notification_type');
            $table->string('title');
            $table->text('notified_on_id')->nullable();
            $table->boolean('is_read')->comment('1 = Read the message,0 = Pending message read')->default(AppConstant::STATUS_INACTIVE);
            $table->boolean('status')->default(AppConstant::STATUS_ACTIVE);
            $table->timestamps();

            $table->foreign('sender_id')->on('hospital')->references('id')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('receiver_id')->on('users')->references('id')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('physician_notifications');
    }
}
