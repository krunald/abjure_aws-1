<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserLicenseIssuerStatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_license_issuer_states', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id')->index('fk_license_issuer_state_user_id_foreign_key_idx');
            $table->string('license_state')->nullable();
            $table->string('license_number')->nullable();
            $table->tinyInteger('license_status')->nullable();
            $table->date('license_expiration_date')->nullable();
            $table->timestamps();

            $table->foreign('user_id', 'fk_license_issuer_state_user_id_foreign_key_id')
                ->references('id')->on('users')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_license_issuer_states');
    }
}
