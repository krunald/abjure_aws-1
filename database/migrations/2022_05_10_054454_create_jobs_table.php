<?php

use App\Models\Hospital\Jobs\Jobs;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jobs', function (Blueprint $table) {
            $table->id();
            $table->uuid('uuid')->unique();
            $table->string('name')->nullable();
            $table->unsignedBigInteger('hospital_id')->nullable()->index('jobs_hospital_id_foreign');
            $table->enum('job_type', [Jobs::GENERAL_SURGERY, Jobs::HOSPITALIST]);
            $table->tinyInteger('min_experience');
            $table->tinyInteger('max_experience');
            $table->integer('slots')->nullable();
            $table->enum('job_post_status', [Jobs::ACTIVE, Jobs::INACTIVE, Jobs::EXPIRED, Jobs::CLOSED, Jobs::COMPLETED])->default(Jobs::ACTIVE);
            $table->boolean('is_filled')->default(false);
            $table->boolean('status')->default('1')->comment('0=inactive,1=active');

            $table->foreign('hospital_id', 'jobs_hospital_id_foreign')
                ->references('id')->on('hospital')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        Schema::dropIfExists('jobs');
    }
}
