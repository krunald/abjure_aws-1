<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->uuid('uuid')->unique();
            $table->string('suffix')->nullable();
            $table->string('first_name');
            $table->string('middle_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('temp_password')->nullable();
            $table->string('forget_password_code')->nullable();
            $table->enum('title', ['MD', 'DO']);
            $table->enum('expertise', ["HOSPITALIST", "GENERAL"]);
            $table->string('message')->nullable();
            $table->tinyInteger('is_verified')->default('0')->comment('0 = unverified, 1 = verified');
            $table->tinyInteger('user_is_active')->default('1')->comment('0 = inactive, 1 = active');
            $table->rememberToken();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
