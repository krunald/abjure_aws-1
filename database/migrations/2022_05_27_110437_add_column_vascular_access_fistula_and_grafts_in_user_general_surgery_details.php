<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnVascularAccessFistulaAndGraftsInUserGeneralSurgeryDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_general_surgery_details', function (Blueprint $table) {
            $table->boolean('vascular_access_fistula_and_grafts')->after('vascular_access');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_general_surgery', function (Blueprint $table) {
            //
        });
    }
}
