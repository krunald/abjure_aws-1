<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserDisciplinaryActionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_disciplinary_actions', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id')->index('user_disciplinary_actions_user_id_foreign');
            $table->boolean('ever_convicted');
            $table->string('ever_convicted_explanation')->nullable();
            $table->boolean('ever_denied_clinical_privileges');
            $table->string('ever_denied_clinical_privileges_explanation')->nullable();
            $table->boolean('ever_denied_professional_liability_insurance');
            $table->string('ever_denied_professional_liability_insurance_explanation')->nullable();
            $table->boolean('ever_denied_or_suspended_license_in_any_jurisdiction');
            $table->string('ever_denied_or_suspended_license_in_any_jurisdiction_explanation')->nullable();
            $table->boolean('ever_denied_or_suspended_participation');
            $table->string('ever_denied_or_suspended_participation_explanation')->nullable();
            $table->boolean('ever_treated_alcoholism_and_substance_abuse');
            $table->string('ever_treated_alcoholism_and_substance_abuse_explanation')->nullable();
            $table->boolean('ever_been_advised_to_seek_treatment');
            $table->string('ever_been_advised_to_seek_treatment_explanation')->nullable();
            $table->boolean('condition_that_can_affect_practicing_medicine');
            $table->string('condition_that_can_affect_practicing_medicine_explanation')->nullable();
            $table->boolean('presently_involved_in_use_of_illegal_substance');
            $table->string('presently_involved_in_use_of_illegal_substance_explanation')->nullable();
            $table->timestamps();

            $table->foreign('user_id','user_disciplinary_actions_user_id_foreign')
                ->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_disciplinary_actions');
    }
}
