<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserEducationRelatedSubDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_education_related_sub_details', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id')->index('user_education_related_sub_details_user_id_foreign');
            $table->tinyInteger('foreign_medical_graduate')->nullable();
            $table->bigInteger('ecfmg_certificate_number')->nullable();
            $table->string('ecfmg_certificate')->nullable();
            $table->tinyInteger('attended_fifth_way_program')->nullable();
            $table->timestamps();
            $table->foreign('user_id','user_education_related_sub_details_user_id_foreign')
                ->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_education_related_sub_details');
    }
}
