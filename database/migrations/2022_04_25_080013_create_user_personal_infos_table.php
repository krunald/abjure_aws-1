<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserPersonalInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_personal_infos', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id')->index('personal_user_info_user_id_foreign');
            $table->date('dob')->nullable();
            $table->string('address');
            $table->double('latitude')->nullable();
            $table->double('longitude')->nullable();
            $table->bigInteger('contact_no');
            $table->string('formatted_contact_no');
            $table->integer('experience_month');
            $table->integer('experience_year');
            $table->enum('citizen', ["US_CITIZEN", "GREEN_CARD", "H-1B", "OTHER"]);
            $table->string('npi');
            $table->string('profile_pic_path')->nullable();
            $table->timestamps();

            $table->foreign('user_id', 'personal_user_info_user_id_foreign')
                ->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_personal_infos');
    }
}
