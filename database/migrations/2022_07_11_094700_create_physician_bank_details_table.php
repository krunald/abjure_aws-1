<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePhysicianBankDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('physician_bank_details', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->index()->constrained()->cascadeOnDelete()->cascadeOnUpdate();
            $table->string('company/individual_name');
            $table->string('bank_name');
            $table->enum('account_type', ['CHECKING_ACCOUNT', 'SAVINGS_ACCOUNT']);
            $table->string('routing_number');
            $table->string('account_number');
            $table->string('social_security_number')->nullable();
            $table->string('tax_identification_number')->nullable();
            $table->boolean('authorized')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('physician_bank_details');
    }
}
