<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHospitalJobTemplatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('hospital_job_templates');
        Schema::enableForeignKeyConstraints();
        Schema::create('hospital_job_templates', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('hospital_id');
            $table->string('template_name');
            $table->string('job_title');
            $table->enum('job_type', ['GENERAL_SURGERY', 'HOSPITALIST']);
            $table->smallInteger('slots');
            $table->tinyInteger('min_experience');
            $table->tinyInteger('max_experience');
            $table->text('description');
            $table->boolean('status')->default(1);
            $table->timestamps();

            $table->foreign('hospital_id')
                ->references('id')->on('hospital')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hospital_job_templates');
    }
}
