<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJobDetailsGeneralSurgeonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_details_general_surgeons', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('job_id')->nullable()->index('job_detail_generals_jobs_id_foreign');

            $table->boolean('is_verified_clinic');
            $table->bigInteger('clinic_days_per_week')->nullable();
            $table->bigInteger('avg_no_of_patients_per_day')->nullable();
            $table->bigInteger('avg_no_of_surgeries_per_or_day');
            $table->text('emr_used_in_clinic')->nullable();
            $table->enum('anesthesia_care',['CRNA','ANESTHESIOLOGIST','BOTH']);
            $table->boolean('upper_lower_endoscopy')->default(0)->comment('0/false =no, 1/true =yes');
            $table->enum('trauma_center_level', ['1', '2', '3', '4', '5']);
            $table->bigInteger('avg_no_of_trauma_patients_per_month');
            $table->boolean('c_section')->comment('0/false = no, 1/true = yes');
            $table->boolean('dialysis_access')->comment('0/false = no, 1/true = yes');
            $table->enum('dialysis_access_value',['CATHETER_ONLY', 'CATHETER', 'AV_GRAFT/FISTULA'])->nullable();
            $table->string('state')->nullable();
            $table->timestamps();


            $table->foreign('job_id', 'job_detail_generals_jobs_id_foreign')
                ->references('id')->on('jobs')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_details_general_surgeons');
    }
}
