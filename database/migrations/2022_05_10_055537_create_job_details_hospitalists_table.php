<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJobDetailsHospitalistsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_details_hospitalists', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('job_id')->index('job_detail_hospitalists_jobs_id_foreign');
            $table->boolean('family_practice_physicians_apply');
            $table->integer('hospital_census');
            $table->integer('avg_admissions_per_provider_by_day');
            $table->integer('avg_er_admissions_per_provider_by_night');
            $table->integer('expected_patient_load');
            $table->integer('hospitalist_in_day');
            $table->integer('hospitalist_in_night');
            $table->integer('mid_levels_in_day');
            $table->integer('mid_levels_in_night');
            $table->boolean('swing_shift_available');
            $table->enum('backup_plan',[
                'NO_MORE_ADMISSION_ALLOWED',
                'BACKUP_PROVIDER_AVAILABLE',
                'POIPLFAC',
                'PREAPLAC',
                'PRTIPLWC',
                ]);
            $table->boolean('mid_level_available');
            $table->enum('mid_level_available_for_shift', ['DAY','NIGHT','BOTH'])->nullable();
            $table->enum('runs_code',['ER_PHYSICIAN','INTENSIVIST','HOSPITALIST']);
            $table->boolean('respiratory_therapist');
            $table->boolean('respiratory_therapist_allowed_intubate')->nullable();
            $table->text('procedures_required');
            $table->enum('contract_period_type',['SHORT_TERM','LONG_TERM']);
            $table->boolean('flexible_shift_scheduling');
            $table->string('state')->nullable();
            $table->timestamps();

            $table->foreign('job_id', 'job_detail_hospitalists_jobs_id_foreign')
                ->references('id')->on('jobs')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_details_hospitalists');
    }
}
