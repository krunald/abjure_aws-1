<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserJobsInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_jobs_invoices', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id')->index('user_jobs_details_user_id_foreign');
            $table->unsignedBigInteger('user_jobs_id')->index('user_jobs_details_user_jobs_id_foreign');
            $table->enum('invoice_status', ['PENDING', 'SENT', 'PAID']);
            $table->boolean('want_to_add_overtime')->nullable()->comment('0/false = yes, 1/true = no');
            $table->double('overtime_hours')->nullable();
            $table->date('date');
            $table->double('rate');
            $table->time('hours')->nullable();
            $table->double('total_amount');
            $table->boolean('acknowledge')->nullable()->comment('0/false = pending, 1/true = received');
            $table->timestamps();

            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('user_jobs_id')
                ->references('id')->on('user_jobs')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_jobs_invoices');
    }
}
