<?php


return [
    'STATUS_ACTIVE' => 1,
    'STATUS_INACTIVE' => 0,
    'STATUS_FAIL' => 'fail',
    'STATUS_OK' => 200,
    'APP_URL' => env('APP_URL'),
    'PHYSICIAN_URL' => env('PHYSICIAN_URL'),
    'HOSPITAL_URL' => env('HOSPITAL_URL'),
    'STRIPE_SECRET' => env('STRIPE_SECRET'),
    'STRIPE_KEY' => env('STRIPE_KEY'),
    'COUNTRY' => env('COUNTRY'),
];
