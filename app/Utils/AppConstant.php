<?php

namespace App\Utils;

class AppConstant
{

    const TITLE_403 = '403 Access Denied';
    const TITLE_403_BODY = 'It\'s better to be slow and careful in the right direction than to be fast and careless on the wrong path. Be sure that you are on the right path before you begin to take your steps! ';
    const TITLE_404 = '404 Not Found';
    const TITLE_404_BODY = '404 ';
    const TITLE_500 = '500 Internal Server Error';
    const TITLE_500_BODY = '500 Body';
    const TITLE_409 = '409 Conflict';
    const TITLE_409_BODY = '409';


    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;
    const FREE_SIGN_TIME = 24;
    const STATUS_FAIL = 'fail';
    const STATUS_OK = 'ok';

    const RENT_HOURS = 48;


    // API status codes
    const OK = 200;
    const CREATED = 201;
    const BAD_REQUEST = 400;
    const UNAUTHORIZED = 401;
    const FORBIDDEN = 403;
    const NOT_FOUND = 404;
    const METHOD_NOT_ALLOWED = 405;
    const CONFLICT = 409;
    const UNPROCESSABLE_REQUEST = 422;
    const INTERNAL_SERVER_ERROR = 500;
    const TOKEN_INVALID = 503;

    const BASE_URL = 'http://localhost:8000';

    const OS_TYPE = ['android', 'ios'];

    const OS_ANDROID = "android";
    const OS_IOS = "ios";

    // Queue Type

    const DEFAULT_QUEUE = 'default';
    const TRIP_QUEUE = 'trip';
//    const OTP_QUEUE = 'OTPQueue';
    //const SEND_TRIP_NOTIFICATION_QUEUE = 'SendTripNotificationQueue'; 2
    //const SEND_ACCEPT_TRIP_NOTIFICATION_QUEUE = 'SendAcceptTripNotificationQueue'; 2
//    const SEND_CANCEL_TRIP_NOTIFICATION_QUEUE = 'SendCancelTripNotificationQueue'; 2
//    const SEND_PICKUP_NOTE_NOTIFICATION_QUEUE = 'SendPickupNoteNotificationQueue'; 1
//    const SEND_PICKUP_LOCATION_NOTIFICATION_QUEUE = 'SendPickupLocationNotificationQueue'; 1
//    const SEND_START_TRIP_NOTIFICATION_QUEUE = 'SendStartTripNotificationQueue'; 1
//    const SEND_DRIVER_NOT_FOUND_NOTIFICATION_QUEUE = 'SendDriverNotFoundNotificationQueue'; //1
//    const SEND_VOUCHER_CODE_QUEUE = 'SendVoucherCodeQueue'; 1
//    const SEND_TRIP_CLEANING_FEE_STATUS_NOTIFICATION_QUEUE = 'SendTripCleaningFeeStatusNotificationQueue'; 3
//    const SEND_DRIVER_BIRTHDAY_NOTIFICATION_QUEUE = 'SendDriverBirthdayNotificationQueue'; 1
//    const SEND_DRIVER_EXPIRY_DOCUMENTS_NOTIFICATION_QUEUE = 'SendDriverExpiryDocumentsNotificationQueue'; 14

    const OTP_VALIDITY = 120;


    const USER_GUARD = 'users';
    const DRIVER_GUARD = 'drivers';

    // upload Disk
    const UPLOADIMAGEDISK = 'public';
    const UPLOADDOCDISK = 'private';

    const NO_MORE_ADMISSION_ALLOWED = 'no more admissions allowed';
    const BACKUP_PROVIDER_AVAILABLE = 'Back-up provider available';
    const POIPLFAC = 'physician can opt to increase patient load for additional compensation';
    const PREAPLAC = 'physician required to exceed agreed patient load for additional compensation';
    const PRTIPLWC = 'patient required to exceed agreed patient load without additional compensation';

    //notification status start
    const JOB_APPLIED = 'JOB_APPLIED';
    const REVIEW_GIVEN = 'REVIEW_GIVEN';
    const JOB_UPDATED = 'JOB_UPDATED';
    const JOB_COMPLETED = 'JOB_COMPLETED';
    const JOB_APPROVED = 'JOB_APPROVED';
    const JOB_APPLICATION_STATUS_UPDATED = 'JOB_APPLICATION_STATUS_UPDATED';
    const JOB_APPLICATION_STATUS_APPROVED = 'JOB_APPLICATION_STATUS_APPROVED';
    const PAYMENT_SENT = 'PAYMENT_SENT';
    const PAYMENT_RECEIVED = 'PAYMENT_RECEIVED';
    const PAYMENT_ACKNOWLEDGED = 'PAYMENT_ACKNOWLEDGED';
    const INVOICE_PENDING = 'INVOICE_PENDING';
    const INVOICE_PAID = 'INVOICE_PAID';
    //notification status completed
}
