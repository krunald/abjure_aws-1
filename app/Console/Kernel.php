<?php

namespace App\Console;

use App\Console\Commands\ChangeUserJobStatus;
use App\Console\Commands\CompletedJobsCron;
use App\Console\Commands\DisableFreePlan;
use App\Console\Commands\ExpiredJobsCron;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        ExpiredJobsCron::class,
        ChangeUserJobStatus::class,
        CompletedJobsCron::class,
        DisableFreePlan::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('freePlan:disable')->everyMinute();
        $schedule->command('expired_jobs')->daily();
        $schedule->command('change_user_job_status')->daily();
        $schedule->command('completed_job')->daily();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
