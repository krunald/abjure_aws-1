<?php

namespace App\Console\Commands;

use App\Models\Physician\Jobs\UserJobs;
use Carbon\Carbon;
use Illuminate\Console\Command;

class ChangeUserJobStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'change_user_job_status:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command will change the application status of user applied jobs to completed after the last day of job.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $userJobs = UserJobs::where(function ($q) {
            $q->where('application_status', '!=', 'EXPIRED');
            $q->where('application_status', '!=', 'COMPLETED');
        })->with('jobs')->get();

        $userJobs->map(function ($userJob) {
            if ($userJob->jobs->description && $userJob->jobs->jobPayment && !empty($userJob->jobs->jobSchedules->toArray())) {
                $lastJobData = $userJob->jobs->jobSchedules->sortBy('end_date')->last();
                if (isset($lastJobData)) {
                    $scheduleDate = Carbon::createFromFormat('Y-m-d', $lastJobData->end_date, 'UTC')->toDateString();
                    $currentDate = Carbon::now()->toDateString();
                    if ($scheduleDate > $currentDate) {
                        $userJob->application_status = UserJobs::COMPLETED;
                        $userJob->save();
                    }
                }
            }
        });
        return 0;
    }
}
