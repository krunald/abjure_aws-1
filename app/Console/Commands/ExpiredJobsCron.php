<?php

namespace App\Console\Commands;

use App\Models\Hospital\Jobs\Jobs;
use Carbon\Carbon;
use Illuminate\Console\Command;

class ExpiredJobsCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'expired_jobs:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command is to expire the jobs';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
//        Jobs will be marked as expired when and when the last date of job is passed and no user has applied for that job
        $jobs = Jobs::where([['job_post_status', '!=', Jobs::EXPIRED], ['job_post_status', '!=', Jobs::COMPLETED]])->with(['jobSchedules', 'userJobs'])->get();
        $jobs->map(function ($job) {
            if (empty($job->userJobs) && !empty($job->jobSchedules)) {
                $lastJobData = $job->jobSchedules->sortBy('end_date')->sortBy('end_time')->last();
                if (isset($lastJobData)) {
                    $scheduleDateTime = Carbon::createFromFormat('Y-m-d H:i:s', $lastJobData->end_date . ' ' . $lastJobData->end_time, 'UTC')->toDateTimeString();
                    $currentDateTime = Carbon::now()->toDateTimeString();
                    if ($scheduleDateTime < $currentDateTime) {
                        $job->job_post_status = Jobs::EXPIRED;
                        $job->save();
                    }
                }
            }
        });
        return 0;
    }
}
