<?php

namespace App\Console\Commands;

use App\Models\Hospital\Jobs\Jobs;
use Carbon\Carbon;
use Illuminate\Console\Command;

class CompletedJobsCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'completed_job:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $jobs = Jobs::where([['job_post_status', '!=', 'EXPIRED'], ['job_post_status', '!=', 'COMPLETED']])->with(['jobSchedules'])->get();
        $jobs->map(function ($job) {
            if (!empty($job->userJobs) &&!empty($job->jobSchedules)) {
                $lastJobData = $job->jobSchedules->sortBy('end_date')->sortBy('end_time')->last();
                if (isset($lastJobData)) {
                    $scheduleDateTime = Carbon::createFromFormat('Y-m-d H:i:s', $lastJobData->end_date . ' ' . $lastJobData->end_time, 'UTC')->toDateTimeString();
                    $currentDateTime = Carbon::now()->toDateTimeString();
                    if ($scheduleDateTime < $currentDateTime) {
                        $job->job_post_status = Jobs::COMPLETED;
                        $job->save();
                    }
                }
            }
        });
        return 0;
    }
}
