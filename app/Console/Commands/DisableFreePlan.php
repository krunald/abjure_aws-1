<?php

namespace App\Console\Commands;

use App\Models\Hospital\Onboarding\Hospital;
use Carbon\Carbon;
use Illuminate\Console\Command;

class DisableFreePlan extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'freePlan:disable';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Disable free plan after one month';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return void
     */
    public function handle()
    {
        Hospital::where([
            ['is_free_user', '=', 1],
            ['free_trial_ends_at', '<', Carbon::now()],
        ])->update(['is_free_plan_expired' => 1]);
    }
}
