<?php

namespace App\Mail\Hospital;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class VerifyEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $hospital;


    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($hospital)
    {
        $this->hospital = $hospital;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('email.verify.verifyEmail')
            ->with('content', $this->hospital)->subject('Hospital Email Verification');
    }
}
