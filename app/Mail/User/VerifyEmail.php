<?php

namespace App\Mail\User;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class VerifyEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $physician;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($physician)
    {
        $this->physician = $physician;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('email.verify.verifyEmail')
            ->with('content', $this->physician)->subject('Physician Email Verification');
    }
}
