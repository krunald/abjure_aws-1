<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class HospitalForgotPasswordMail extends Mailable
{
    use Queueable, SerializesModels;
    public $hospital;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($hospital)
    {
        $this->hospital = $hospital;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('email.passwordReset')
            ->with('content', $this->hospital);
    }
}
