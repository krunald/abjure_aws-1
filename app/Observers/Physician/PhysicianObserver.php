<?php

namespace App\Observers\Physician;

use App\Jobs\Physician\SendVerificationMailJob;
use App\Mail\User\VerifyEmail;
use App\Models\Physician\Onboarding\PhysicianVerifyEmail;
use App\Models\Physician\Onboarding\User;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class PhysicianObserver
{
    /**
     * Handle the User "created" event.
     *
     * @param User $user
     * @return void
     */
    public function created(User $user)
    {
        $physician = $user;
        $resetToken = Str::random(60);

        PhysicianVerifyEmail::updateOrCreate(
            [
                'email' => $physician['email']
            ],
            [
                'token' => $resetToken,
            ]
        );

        //Generate, the email verification link. The token generated is embedded in the link
        $link = config('appconstants.APP_URL') . '/api/user/verify/email?token=' . $resetToken . '&email=' . urlencode($physician['email']);
        $physician['link'] = $link;
        $physician['name'] = $physician['first_name'] . ' ' . $physician['last_name'];
        SendVerificationMailJob::dispatch($physician)->onQueue('emailVerification');
    }

    /**
     * Handle the User "updated" event.
     *
     * @param User $user
     * @return void
     */
    public function updated(User $user)
    {
        //
    }

    /**
     * Handle the User "deleted" event.
     *
     * @param User $user
     * @return void
     */
    public function deleted(User $user)
    {
        //
    }

    /**
     * Handle the User "restored" event.
     *
     * @param User $user
     * @return void
     */
    public function restored(User $user)
    {
        //
    }

    /**
     * Handle the User "force deleted" event.
     *
     * @param User $user
     * @return void
     */
    public function forceDeleted(User $user)
    {
        //
    }
}
