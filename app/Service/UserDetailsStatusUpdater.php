<?php

namespace App\Service;

use App\Models\Physician\Onboarding\UserDetailsStatus;

class UserDetailsStatusUpdater
{
    protected $userId;
    protected $fieldName;

    public function __construct($userId,$fieldName)
    {
        $this->userId=$userId;
        $this->fieldName=$fieldName;
        $this->updateUserDetailsStatus();
    }

    public function updateUserDetailsStatus(){
        UserDetailsStatus::whereUserId($this->userId)->updateOrCreate(['user_id'=>$this->userId],[$this->fieldName => 1]);
    }
}
