<?php

namespace App\Providers;

use App\Events\HospitalCreated;
use App\Events\Physician\PhysicianCreated;
use App\Listeners\HospitalCreatedListner;
use App\Models\Physician\Onboarding\User;
use App\Observers\Physician\PhysicianObserver;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        HospitalCreated::class => [
            HospitalCreatedListner::class
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        User::observe(PhysicianObserver::class);
    }
}
