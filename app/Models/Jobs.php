<?php
//
//namespace App\Models;
//
//use App\Models\Hospital\Onboarding\Hospital;
//use App\Models\Physician\Jobs\JobDescription;
//use App\Models\Physician\Jobs\JobDetailsGeneralSurgeon;
//use App\Models\Physician\Jobs\JobDetailsHospitalist;
//use App\Models\Physician\Jobs\JobPayment;
//use App\Models\Physician\Jobs\JobSchedule;
//use App\Models\Physician\Jobs\UserJobs;
//use Illuminate\Database\Eloquent\Factories\HasFactory;
//use Illuminate\Database\Eloquent\Model;
//use Illuminate\Database\Eloquent\Relations\BelongsTo;
//use Illuminate\Database\Eloquent\Relations\HasMany;
//use Illuminate\Database\Eloquent\Relations\HasOne;
//
//class Jobs extends Model
//{
//    use HasFactory;
//
//    public const HOSPITALIST = 'HOSPITALIST';
//    public const GENERAL_SURGERY = 'GENERAL_SURGERY';
//
//    protected $fillable = [
//        'hospital_id',
//        'name',
//        'job_type',
//        'slots',
//        'min_experience',
//        'max_experience',
//    ];
//
//    public function hospital(): BelongsTo
//    {
//        return $this->belongsTo(Hospital::class, 'hospital_id', 'id');
//    }
//    public function userJobs(): HasMany
//    {
//        return $this->hasMany(UserJobs::class, 'job_id', 'id');
//    }
//
//    public function jobDescription(): HasOne
//    {
//        return $this->hasOne(JobDescription::class, 'job_id', 'id');
//    }
//
//    public function jobDetailsGeneralSurgeon(): HasOne
//    {
//        return $this->hasOne(JobDetailsGeneralSurgeon::class, 'job_id', 'id');
//    }
//
//    public function jobDetailsHospitalist(): HasOne
//    {
//        return $this->hasOne(JobDetailsHospitalist::class, 'job_id', 'id');
//    }
//
//    public function jobSchedules(): HasMany
//    {
//        return $this->hasMany(JobSchedule::class, 'job_id', 'id','jobs_job_schedule');
//    }
//
//    public function jobPayment(): HasOne
//    {
//        return $this->hasOne(JobPayment::class, 'job_id', 'id');
//    }
//}
