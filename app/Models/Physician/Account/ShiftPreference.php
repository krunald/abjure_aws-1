<?php

namespace App\Models\Physician\Account;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ShiftPreference extends Model
{
    use HasFactory;

    protected $fillable = [
        'shift',
        'status',
        'order'
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'order',
        'status',
        'pivot'
    ];
}
