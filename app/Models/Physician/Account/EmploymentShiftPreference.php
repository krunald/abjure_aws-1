<?php

namespace App\Models\Physician\Account;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class EmploymentShiftPreference extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'shift',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'user_id'
    ];
}
