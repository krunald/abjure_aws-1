<?php

namespace App\Models\Physician\Account;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PhysicianBankDetails extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'company/individual_name',
        'bank_name',
        'account_type',
        'routing_number',
        'account_number',
        'social_security_number',
        'tax_identification_number',
        'authorized'
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
    ];

    protected $casts = [
        'authorized' => 'bool'
    ];
}
