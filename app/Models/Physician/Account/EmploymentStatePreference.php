<?php

namespace App\Models\Physician\Account;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EmploymentStatePreference extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'state_id',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'user_id',
    ];
}
