<?php

namespace App\Models\Physician\common;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PhysicianNotificationPreference extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'email',
        'text_message',
    ];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];
}
