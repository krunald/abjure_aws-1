<?php

namespace App\Models\Physician\Calendar;

use App\Models\Physician\Onboarding\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class PhysicianAvailability extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'shift',
        'availability_date',
    ];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
