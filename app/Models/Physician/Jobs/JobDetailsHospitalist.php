<?php

namespace App\Models\Physician\Jobs;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JobDetailsHospitalist extends Model
{
    use HasFactory;

    protected $fillable = [
        'job_id',
        'family_practice_physicians_apply',
        'hospital_census',
        'avg_admissions_per_provider_by_day',
        'avg_er_admissions_per_provider_by_night',
        'expected_patient_load',
        'hospitalist_in_day',
        'hospitalist_in_night',
        'mid_levels_in_day',
        'mid_levels_in_night',
        'swing_shift_available',
        'backup_plan',
        'mid_level_available',
        'runs_code',
        'respiratory_therapist',
        'respiratory_therapist_allowed_intubate',
        'procedures_required',
        'contract_period_type',
        'flexible_shift_scheduling',
    ];

    protected $casts =[

    ];

    public function getProceduresRequiredAttribute($value)
    {
        return explode(',', $value);
    }

    public function setProceduresRequiredAttribute($value)
    {
        $this->attributes['procedures_required'] = implode(",", $value);
    }
}
