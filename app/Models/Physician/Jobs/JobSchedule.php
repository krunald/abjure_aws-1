<?php

namespace App\Models\Physician\Jobs;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JobSchedule extends Model
{
    use HasFactory;

    protected $fillable = [
        'job_id',
        'start_date',
        'end_date',
        'shift',
        'start_time',
        'end_time',
    ];

}
