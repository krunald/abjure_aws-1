<?php

namespace App\Models\Physician\Jobs;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JobDescription extends Model
{
    use HasFactory;

    protected $table='job_descriptions';

    protected $fillable=['job_id','description'];
}
