<?php

namespace App\Models\Physician\Jobs;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserJobsInvoice extends Model
{
    use HasFactory;

    public const PENDING = 'PENDING';
    public const PAID = 'PAID';

    protected $fillable = [
        'user_id',
        'user_jobs_id',
        'invoice_status',
        'want_to_add_overtime',
        'overtime_hours',
        'date',
        'hours',
        'rate',
        'total_amount',
        'acknowledge',
    ];
    protected $casts = [
        'want_to_add_overtime' => 'bool',
        'total_amount' => 'double',
    ];

    public function userJob()
    {
        return $this->belongsTo(UserJobs::class, 'user_jobs_id', 'id');
    }

    public function getTotalAmountAttribute($value): float
    {
        return round($value, 2);
    }
}
