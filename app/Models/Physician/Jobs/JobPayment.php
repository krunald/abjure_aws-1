<?php

namespace App\Models\Physician\Jobs;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JobPayment extends Model
{
    use HasFactory;

    protected $fillable=[
        'job_id',
        'payment_type',
        'amount',
    ];
}
