<?php

namespace App\Models\Physician\Jobs;

use App\Models\Hospital\Jobs\Jobs;
use App\Models\Physician\Onboarding\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserJobsSaved extends Model
{
    use HasFactory;

    protected $table = 'user_jobs_saved';
    protected $fillable = [
        'user_id',
        'job_id',
    ];

    public function jobs()
    {
        return $this->hasMany(Jobs::class, 'id', 'job_id');
    }
    public function users()
    {
        return $this->hasMany(User::class, 'id', 'user_id');
    }

}
