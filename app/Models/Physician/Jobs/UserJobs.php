<?php

namespace App\Models\Physician\Jobs;

use App\Models\Hospital\Jobs\Jobs;
use App\Models\Hospital\Jobs\UserJobsScheduleInterview;
use App\Models\Physician\Onboarding\User;
use App\Models\Physician\Onboarding\UserJobsDetails;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;

class UserJobs extends Model
{
    use HasFactory;

    public const APPLIED = 'APPLIED';
    public const SCREENING = 'SCREENING';
    public const INTERVIEW = 'INTERVIEW';
    public const OFFER_APPROVED = 'OFFER_APPROVED';
    public const REJECTED = 'REJECTED';
    public const COMPLETED = 'COMPLETED';
    public const PENDING = 'PENDING';

    public const APPLICATION_STATUS = [
        "APPLIED" => 'APPLIED',
        "SCREENING" => 'SCREENING',
        "INTERVIEW" => 'INTERVIEW',
        "OFFER_APPROVED" => 'OFFER_APPROVED',
        "REJECTED" => 'REJECTED',
        "COMPLETED" => 'COMPLETED',
    ];

    protected $fillable = [
        'user_id',
        'job_id',
        'application_status',
        'user_rating',
        'description',
        'user_review',
        'pros_with_jobs',
        'cons_with_jobs',
        'invoice_status',
        'acknowledge',
        'status',
    ];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    public function jobs(): BelongsTo
    {
        return $this->belongsTo(Jobs::class, 'job_id', 'id');
    }

    public function users(): HasOne
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function userScheduledInterviewDetail()
    {
        return $this->hasOne(UserJobsScheduleInterview::class, 'user_job_id', 'id');
    }

    public function usersJobsInvoice()
    {
        return $this->hasOne(UserJobsInvoice::class, 'user_jobs_id', 'id');
    }
    public function usersJobsDetails()
    {
        return $this->hasOne(UserJobsDetails::class, 'user_jobs_id', 'id');
    }
}
