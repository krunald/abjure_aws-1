<?php

namespace App\Models\Physician\Onboarding;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserHospitalityProfileDetails extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'user_id',
        'icu_management',
        'procedures',
        'max_desired_patient_load_flexible',
        'max_desired_patient_load_numbers',
        'high_volume_care_part_of_practice',
    ];


    protected $casts = [
        'icu_management' => 'bool',
        'max_desired_patient_load' => 'bool',
        'high_volume_care_part_of_practice' => 'bool',
    ];


    public function getProceduresAttribute($value)
    {
        return explode(',', $value);
    }

    public function setProceduresAttribute($value)
    {
        $this->attributes['procedures'] = implode(",", $value);
    }
}
