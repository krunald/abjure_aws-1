<?php

namespace App\Models\Physician\Onboarding;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserGeneralSurgeryDetails extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'user_id',
        'cholecstectomies',
        'appendectomies',
        'hernia_repairs',
        'robotic_surgery',
        'cesarean_section',
        'egd_and_colonoscopy',
        'vascular_access',
        'vascular_access_fistula_and_grafts',
        'trauma_center_level',
    ];

    protected $casts = [
        'cholecstectomies' => 'bool',
        'appendectomies' => 'bool',
        'hernia_repairs' => 'bool',
        'robotic_surgery' => 'bool',
        'cesarean_section' => 'bool',
        'egd_and_colonoscopy' => 'bool',
        'vascular_access' => 'bool',
        'vascular_access_fistula_and_grafts' => 'bool',
    ];
}
