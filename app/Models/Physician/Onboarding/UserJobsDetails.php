<?php

namespace App\Models\Physician\Onboarding;

use App\Models\Physician\Jobs\UserJobs;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserJobsDetails extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_jobs_id',
        'invoice_status',
        'want_to_add_overtime',
        'overtime_hours',
        'acknowledge',
    ];
    protected $casts = [
        'want_to_add_overtime' => 'bool'
    ];

    public function userJob()
    {
        return $this->belongsTo(UserJobs::class, 'user_jobs_id', 'id');
    }
}
