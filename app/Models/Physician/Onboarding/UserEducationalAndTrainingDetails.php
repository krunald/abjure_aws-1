<?php

namespace App\Models\Physician\Onboarding;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserEducationalAndTrainingDetails extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'type',
        'training_specialty',
        'name',
        'address',
        'start_date',
        'end_date'
    ];

    protected $casts = [
        'start_date' => 'date:Y-m-d',
        'end_date' => 'date:Y-m-d',
    ];

    public function setStartDate($value)
    {
        return $this->attribute['start_date'] = Carbon::createFromFormat('Y-m-d', $value)->toDateString();
    }

    public function setEndDate($value)
    {
        return $this->attribute['end_date'] = Carbon::createFromFormat('Y-m-d', $value)->toDateString();
    }

    public function User()
    {
        $this->belongsTo(User::class, 'user_id', 'id');
    }
}
