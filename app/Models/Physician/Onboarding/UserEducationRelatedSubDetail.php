<?php

namespace App\Models\Physician\Onboarding;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserEducationRelatedSubDetail extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'foreign_medical_graduate',
        'ecfmg_certificate_number',
        'ecfmg_certificate',
        'attended_fifth_way_program',
    ];

    protected $casts = [
        'foreign_medical_graduate' => 'bool',
        'ecfmg_certificate' => 'bool',
        'attended_fifth_way_program' => 'bool',
    ];

    public function User()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
