<?php

namespace App\Models\Physician\Onboarding;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PhysicianVerifyEmail extends Model
{
    use HasFactory;

    protected $fillable = [
        'email', 'token', 'code'
    ];
}
