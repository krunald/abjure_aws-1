<?php

namespace App\Models\Physician\Onboarding;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserProfessionalDetails extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'user_id',
        'ever_convicted',
        'ever_convicted_explanation',
        'ever_diminished_clinical_privileges',
        'ever_diminished_clinical_privileges_explanation',
        'ever_license_certificate_revoked',
        'ever_license_certificate_revoked_explanation',
        'participation_revoked',
        'participation_revoked_explanation',
        'alcoholism_or_substance_abuse',
        'alcoholism_or_substance_abuse_explanation',
        'seeked_medical_treatment',
        'seeked_medical_treatment_explanation',
        'health_condition_affecting_medical_staff_duties',
        'health_condition_affecting_medical_staff_duties_explanation',
        'use_of_illegal_substance',
        'use_of_illegal_substance_explanation',
    ];
}
