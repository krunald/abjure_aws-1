<?php

namespace App\Models\Physician\Onboarding;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserBoardCertificationDetails extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'user_id',
        'are_you_board_certified',
        'issuer_name',
        'speciality',
        'date_certified',
        'date_recertified',
        'expiration_date',
    ];

    protected $casts=[
        'are_you_board_certified'=>'bool'
    ];
}
