<?php

namespace App\Models\Physician\Onboarding;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserClinicalCertificationDetails extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'user_id',
        'certification_number',
        'bls_certification',
        'bls_certification_expiration',
        'acls_certification',
        'acls_certification_expiration',
        'atls_certification',
        'atls_certification_expiration',
        'pals_certification',
        'pals_certification_expiration',
    ];

    protected $casts=[
        'bls_certification'=>'bool',
        'acls_certification'=>'bool',
        'atls_certification'=>'bool',
        'pals_certification'=>'bool'
    ];
}
