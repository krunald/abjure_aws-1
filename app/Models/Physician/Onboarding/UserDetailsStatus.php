<?php

namespace App\Models\Physician\Onboarding;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserDetailsStatus extends Model
{
    use HasFactory;

    const PERSONAL_DETAILS='personal_details';
    const EDUCATION_AND_TRAINING='education_and_training';
    const CERTIFICATION_AND_LICENSE='certification_and_license';
    const WORK_HISTORY='work_history';
    const DISCIPLINARY_ACTIONS='disciplinary_actions';
    const MALPRACTICE_HISTORY_DETAILS='malpractice_history_details';
    const USER_REFERENCES='user_references';
    const PROFILE_EXPERTISE_DETAILS='profile_expertise_details';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'personal_details',
        'education_and_training',
        'certification_and_license',
        'work_history',
        'disciplinary_actions',
        'malpractice_history_details',
        'user_references',
        'profile_expertise_details',
    ];

}
