<?php

namespace App\Models\Physician\Onboarding;

use App\Models\Hospital\Jobs\Jobs;
use App\Models\Physician\Account\ShiftPreference;
use App\Models\Physician\Jobs\UserJobs;
use App\Models\Physician\Jobs\UserJobsSaved;
use App\Models\StatesList;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Cashier\Billable;
use Tymon\JWTAuth\Contracts\JWTSubject;


class User extends Authenticatable implements JWTSubject
{
    use HasFactory, Notifiable;
    use Billable;

    protected $rules = [
        'first_name' => 'required',
        'last_name' => 'required',
        'middle_name' => 'required',
        'email' => 'required',
        'password' => 'required',
        'suffix' => 'required',
        'title' => 'required',
        'expertise' => 'required|in:HOSPITALIST,GENERAL_SURGERY',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email',
        'uuid',
        'password',
        'stripe_connect_accounts_id',
        'suffix',
        'title',
        'first_name',
        'middle_name',
        'last_name',
        'email',
        'email_verified_at',
        'password',
        'expertise',
        'status',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'temp_password',
        'forget_password_code'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }

    public function getFirstNameAttribute($value): string
    {
        return ucfirst($value);
    }

    public function getLastNameAttribute($value): string
    {
        return ucfirst($value);
    }

    public function getMiddleNameAttribute($value): string
    {
        return ucfirst($value);
    }

    public function userSavedJobs(): BelongsToMany
    {
        return $this->belongsToMany(Jobs::class, UserJobsSaved::class, 'user_id', 'job_id')->withTimestamps();
    }

    public function personInfo(): HasOne
    {
        return $this->hasOne(UserPersonalInfos::class);
    }

    public function educationalAndTraingDetails(): HasMany
    {
        return $this->hasMany(UserEducationalAndTrainingDetails::class);
    }

    public function educationaRelatedSubDetails(): HasOne
    {
        return $this->hasOne(UserEducationRelatedSubDetail::class);
    }

    public function licensure(): HasMany
    {
        return $this->hasMany(UserLicenseIssuerStates::class);
    }

    public function workHistory(): HasMany
    {
        return $this->hasMany(UserWorkExperience::class);
    }

    public function malPractice(): HasMany
    {
        return $this->hasMany(UserMalpracticeHistory::class);
    }

    public function referencesDetails(): HasMany
    {
        return $this->hasMany(UserReferencesDetails::class);
    }

    public function boardCertification(): HasOne
    {
        return $this->hasOne(UserBoardCertificationDetails::class);
    }

    public function clinicalCertification(): HasOne
    {
        return $this->hasOne(UserClinicalCertificationDetails::class);
    }

    public function licenseIssuerDetails(): HasMany
    {
        return $this->hasMany(UserLicenseIssuerStates::class);
    }

    public function userJobs(): HasMany
    {
        return $this->hasMany(UserJobs::class);
    }

    public function disciplinaryAction(): HasOne
    {
        return $this->hasOne(UserDisciplinaryActions::class);
    }

    public function generalSuregery(): HasOne
    {
        return $this->hasOne(UserGeneralSurgeryDetails::class);
    }

    public function hospitalist(): HasOne
    {
        return $this->hasOne(UserHospitalityProfileDetails::class);
    }

    public function userDetailsStatus(): HasOne
    {
        return $this->hasOne(UserDetailsStatus::class);
    }

    public function shifts(): BelongsToMany
    {
        return $this->belongsToMany(ShiftPreference::class, 'employment_shift_preferences', 'user_id', 'shift_id');
    }

    public function states(): BelongsToMany
    {
        return $this->belongsToMany(StatesList::class, 'employment_state_preferences', 'user_id', 'state_id');
    }
}
