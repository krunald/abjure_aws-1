<?php

namespace App\Models\Physician\Onboarding;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserLicenseIssuerStates extends Model
{
    use HasFactory;

    protected $table = 'user_license_issuer_states';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'license_state',
        'license_number',
        'license_status',
        'license_expiration_date',
    ];

    protected $casts = [
        'license_status' => 'bool',
    ];

    public function User()
    {
        $this->belongsTo(User::class, 'user_id', 'id');
    }
}
