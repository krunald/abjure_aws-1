<?php

namespace App\Models\Physician\Onboarding;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserDisciplinaryActions extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'ever_convicted',
        'ever_convicted_explanation',
        'ever_denied_clinical_privileges',
        'ever_denied_clinical_privileges_explanation',
        'ever_denied_professional_liability_insurance',
        'ever_denied_professional_liability_insurance_explanation',
        'ever_denied_or_suspended_license_in_any_jurisdiction',
        'ever_denied_or_suspended_license_in_any_jurisdiction_explanation',
        'ever_denied_or_suspended_participation',
        'ever_denied_or_suspended_participation_explanation',
        'ever_treated_alcoholism_and_substance_abuse',
        'ever_treated_alcoholism_and_substance_abuse_explanation',
        'ever_been_advised_to_seek_treatment',
        'ever_been_advised_to_seek_treatment_explanation',
        'condition_that_can_affect_practicing_medicine',
        'condition_that_can_affect_practicing_medicine_explanation',
        'presently_involved_in_use_of_illegal_substance',
        'presently_involved_in_use_of_illegal_substance_explanation',
    ];
    protected $casts = [
        'ever_convicted' => 'bool',
        'ever_denied_clinical_privileges' => 'bool',
        'ever_denied_professional_liability_insurance' => 'bool',
        'ever_denied_or_suspended_license_in_any_jurisdiction' => 'bool',
        'ever_denied_or_suspended_participation' => 'bool',
        'ever_treated_alcoholism_and_substance_abuse' => 'bool',
        'ever_been_advised_to_seek_treatment' => 'bool',
        'condition_that_can_affect_practicing_medicine' => 'bool',
        'presently_involved_in_use_of_illegal_substance' => 'bool',
    ];
}
