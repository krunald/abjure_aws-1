<?php

namespace App\Models\Physician\Onboarding;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserDetails extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'stripe_customer_id',
        'stripe_user_id',
        'phone_no',
        'is_first',
        'is_verified',
        'is_approved',
        'is_stripe_connected',
        'is_stripe_verify',
        'is_google_sync',
        'is_outlook_sync',
        'is_apple_sync',
        'status',
        'user_is_active',
    ];
}
