<?php

namespace App\Models\Physician\Onboarding;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class UserPersonalInfos extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'user_id',
        'dob',
        'contact_no',
        'formatted_contact_no',
        'address',
        'latitude',
        'longitude',
        'citizen',
        'permanent_resident',
        'j1_visa',
        'experience_month',
        'experience_year',
        'npi',
        'profile_pic_path',

    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'dob' => 'date:Y-m-d',
    ];

    public function getDobAttribute($value)
    {
        return Carbon::parse($value)->format('d-m-Y');
    }

    public function setDobAttribute($value)
    {
        $this->attributes['dob'] = Carbon::parse($value)->format('Y-m-d H:i:s');
    }
}
