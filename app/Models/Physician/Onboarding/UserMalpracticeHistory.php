<?php

namespace App\Models\Physician\Onboarding;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserMalpracticeHistory extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'user_id',
        'ever_denied_professional_liability_insurance',
        'claim_suits_settlements_raised',
        'date_of_incident',
        'location',
        'allegation',
        'allegation_status',
        'settlement_amount',
        'tofile_or_currently_pending_suits_claims',
    ];

    protected $casts=[
        'ever_denied_professional_liability_insurance'=>'bool',
        'claim_suits_settlements_raised'=>'bool',
        'tofile_or_currently_pending_suits_claims'=>'bool',
    ];

    public function User(){
        $this->belongsTo(User::class,'user_id','id');
    }
}
