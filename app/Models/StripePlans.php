<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StripePlans extends Model
{
    use HasFactory;

    protected $fillable = [
        'product_id',
        'name',
        'default_price',
        'interval',
        'unit_amount',
        'unit_amount_decimal',
        'description'
    ];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];
}
