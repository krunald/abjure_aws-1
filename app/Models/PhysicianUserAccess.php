<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PhysicianUserAccess extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id', 'access_token', 'fcm_token'
    ];

    public function user()
    {
        return $this->hasOne(PhysicianUserAccess::class, 'id', 'id');
    }
}
