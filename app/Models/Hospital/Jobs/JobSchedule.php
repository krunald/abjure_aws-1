<?php

namespace App\Models\Hospital\Jobs;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class JobSchedule extends Model
{
    use HasFactory;

    protected $fillable = [
        'job_id',
        'start_date',
        'end_date',
        'shift',
        'start_time',
        'end_time',
    ];
 public function jobs(): BelongsTo
    {
        return $this->belongsTo(Jobs::class, 'job_id', 'id', );
    }
}
