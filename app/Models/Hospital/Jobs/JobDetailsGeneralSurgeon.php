<?php

namespace App\Models\Hospital\Jobs;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JobDetailsGeneralSurgeon extends Model
{
    use HasFactory;

    protected $fillable=[
        'job_id',
        'is_verified_clinic',
        'clinic_days_per_week',
        'avg_no_of_patients_per_day',
        'avg_no_of_surgeries_per_or_day',
        'anesthesia_care',
        'emr_used_in_clinic',
        'upper_lower_endoscopy',
        'trauma_center_level',
        'avg_no_of_trauma_patients_per_month',
        'c_section',
        'dialysis_access',
        'dialysis_access_value',
        'status',
        'state'
    ];

    protected $casts=[
        'is_verified_clinic'=>'bool',
        'upper_lower_endoscopy'=>'bool',
        'c_section'=>'bool',
        'dialysis_access'=>'bool',
    ];
}
