<?php

namespace App\Models\Hospital\Jobs;

use App\Models\Hospital\Onboarding\Hospital;
use App\Models\Physician\Jobs\UserJobs;
use App\Models\Physician\Jobs\UserJobsSaved;
use App\Models\Physician\Onboarding\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Jobs extends Model
{
    use HasFactory;

    public const HOSPITALIST = 'HOSPITALIST';
    public const GENERAL_SURGERY = 'GENERAL_SURGERY';
    public const FREE_JOB_COUNT = 5;
    public const INACTIVE = 'INACTIVE';
    public const ACTIVE = 'ACTIVE';
    public const CLOSED = 'FILLED';
    public const EXPIRED = 'EXPIRED';
    public const COMPLETED = 'COMPLETED';

    protected $fillable = [
        'uuid',
        'hospital_id',
        'name',
        'job_type',
        'slots',
        'job_post_status',
        'min_experience',
        'max_experience',
        'status',
        'is_filled',
    ];
    protected $appends = ["avg_user_rating"];
    protected $hidden = [
        'pivot'
    ];

    public function hospital(): BelongsTo
    {
        return $this->belongsTo(Hospital::class, 'hospital_id', 'id');
    }

    public function userJobs(): HasMany
    {
        return $this->hasMany(UserJobs::class, 'job_id', 'id');
    }
    public function singleUserJob(): HasOne
    {
        return $this->hasOne(UserJobs::class, 'job_id', 'id');
    }

    public function getAvgUserRatingAttribute(): float
    {
        return round($this->userJobs()->average('user_rating'));
    }

    public function description(): HasOne
    {
        return $this->hasOne(JobDescription::class, 'job_id', 'id');
    }

    public function jobDetailsGeneralSurgeon(): HasOne
    {
        return $this->hasOne(JobDetailsGeneralSurgeon::class, 'job_id', 'id');
    }

    public function jobDetailsHospitalist(): HasOne
    {
        return $this->hasOne(JobDetailsHospitalist::class, 'job_id', 'id');
    }

    public function jobSchedules(): HasMany
    {
        return $this->hasMany(JobSchedule::class, 'job_id', 'id');
    }

    public function jobPayment(): HasOne
    {
        return $this->hasOne(JobPayment::class, 'job_id', 'id');
    }

    public function userSavedJobs(): BelongsToMany
    {
        return $this->belongsToMany(User::class, UserJobsSaved::class, 'job_id', 'user_id')->withTimestamps();
    }
}
