<?php

namespace App\Models\Hospital\Jobs;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserJobReviewAndRating extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_jobs_id',
        'rating',
        'experience_with_doctor',
        'quality_service_by_doctor',
        'doctors_availability',
        'likely_to_recommend_doctor',
    ];
}
