<?php

namespace App\Models\Hospital\Jobs;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JobPayment extends Model
{
    use HasFactory;

    protected $fillable = [
        'job_id',
        'payment_type',
        'amount',
    ];

    public function getAmountAttribute($value): float
    {
        return round($value, 2);
    }
}
