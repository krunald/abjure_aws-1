<?php

namespace App\Models\Hospital\Jobs;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;

class JobsAlgolia extends Model
{
    use HasFactory;
    use Searchable;

    protected $table = "jobs_algolia";
    protected $fillable = [
        'id',
        'job_id',
        'hospital_id',
        'job_details',
        'job_schedules',
        'job_payment',
        'job_description'
    ];
}
