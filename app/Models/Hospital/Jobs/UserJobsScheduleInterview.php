<?php

namespace App\Models\Hospital\Jobs;

use App\Models\Hospital\Onboarding\Hospital;
use App\Models\Physician\Jobs\UserJobs;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserJobsScheduleInterview extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_job_id',
        'user_id',
        'hospital_id',
        'interview_date',
        'interview_time',
        'interview_type',
        'other_interview_type',
        'video_call_link'
    ];

    public function hospital()
    {
        return $this->belongsTo(Hospital::class,'hospital_id','id');
    }
    public function userJob()
    {
        return $this->belongsTo(UserJobs::class,'user_job_id','id');
    }
}
