<?php

namespace App\Models\Hospital\Onboarding;

use App\Events\HospitalCreated;
use App\Models\Hospital\Jobs\Jobs;
use App\Models\Hospital\Subscription\HospitalDetails;
use App\Models\Hospital\Subscription\Subscription;
use App\Models\Hospital\Subscription\SubscriptionItem;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Cashier\Billable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class Hospital extends Authenticatable implements JWTSubject
{
    use HasFactory;
    use Billable;

    protected $guard = 'hospital';
    protected $table = 'hospital';
    protected $fillable = [
        'uuid',
        'email',
        'name',
        'licence_number',
        'address',
        'contact_person',
        'contact_person_number',
        'contact_number',
        'password',
        'status',

        'is_free_user',
        'free_trial_starts_at',
        'free_trial_ends_at',
        'is_free_plan_expired',

        'stripe_subscription_id',
        'stripe_id',
        'pm_type',
        'pm_last_four',
        'is_verified',
        'trial_ends_at'
    ];

    protected $hidden = [
        'password',
        'created_at',
        'updated_at'
    ];

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims(): array
    {
        return [];
    }

    protected $dispatchesEvents = [
        'created' => HospitalCreated::class
    ];

    public function speciality(): BelongsToMany
    {
        return $this->belongsToMany(HospitalSpecialistAvailable::class, 'hospital_specialities', 'hospital_id', 'hospital_specialist_available_id');
    }

    public function highlights(): HasOne
    {
        return $this->hasOne(HospitalHighlights::class, 'hospital_id', 'id');
    }

    public function hospitalDetails(): HasOne
    {
        return $this->hasOne(HospitalDetails::class, 'hospital_id', 'id');
    }
    public function jobs(): HasMany
    {
        return $this->hasMany(Jobs::class);
    }

    public function subscriptionDetails(): HasOne
    {
        return $this->hasOne(Subscription::class, 'hospital_id', 'id');
    }

    public function subscriptionItems(): HasOne
    {
        return $this->hasOne(SubscriptionItem::class, 'subscription_id', 'stripe_subscription_id');
    }
}
