<?php

namespace App\Models\Hospital\Onboarding;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HospitalAccess extends Model
{
    use HasFactory;
    protected $table = 'hospital_accesses';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'hospital_id', 'access_token', 'fcm_token'
    ];

    public function user(){
        return $this->hasOne(HospitalAccess::class, 'id', 'id');
    }
}
