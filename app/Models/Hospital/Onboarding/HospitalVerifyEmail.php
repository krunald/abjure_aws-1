<?php

namespace App\Models\Hospital\Onboarding;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HospitalVerifyEmail extends Model
{
    use HasFactory;

    protected $fillable = [
        'email', 'token'
    ];

    public function hospitalAccess()
    {
        return $this->hasOne(HospitalAccess::class, 'id', 'id');
    }
}
