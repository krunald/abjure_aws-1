<?php

namespace App\Models\Hospital\Onboarding;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class HospitalHighlights extends Model
{
    use HasFactory;

    protected $table = "hospital_highlights";
    protected $fillable = [
        'hospital_id',
        'number_of_beds',
        'icu_beds',
        'emr_used_for',
        'annual_er_visits',
        'paging_system',
        'icu_status',
        'has_stroke_center',
        'pci_available',
        'trauma_center_level',
        'robotic_surgery_platform_available',
        'hemodialysis',
        'consultant_accessibility_after_working_hours',
        'credentialing_length',
        'credentialing_available',
        'time_taken_for_privileges'
    ];

    protected $casts = [
        'has_stroke_center' => 'bool',
        'pci_available' => 'bool',
        'robotic_surgery_platform_available' => 'bool',
        'hemodialysis' => 'bool',
        'credentialing_available' => 'bool',
    ];

    public function speciality(): HasMany
    {
        return $this->hasMany(HospitalSpeciality::class, 'hospital_id', 'hospital_id');
    }

}
