<?php

namespace App\Models\Hospital\Onboarding;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

class HospitalSpeciality extends Model
{
    use HasFactory;

    protected $fillable = [
        'hospital_specialist_available_id',
        'hospital_id'
    ];

    public function specialityName(): HasOne
    {
        return $this->hasOne(HospitalSpecialistAvailable::class, 'id', 'hospital_specialist_available_id');
    }
}
