<?php

namespace App\Models\Hospital\Onboarding;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HospitalSpecialistAvailable extends Model
{
    use HasFactory;

    protected $table = "hospital_specialist_available";
    protected $fillable = [
        'speciality',
        'status',
        'order'
    ];

    protected $hidden = [
        'updated_at',
        'created_at',
        'order',
        'status'
    ];

    public function getSpecialityAttribute($value)
    {
        return ucfirst($value);
    }
}
