<?php

namespace App\Models\Hospital\Subscription;

use App\Models\Hospital\Onboarding\Hospital;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravel\Cashier\Cashier;

class HospitalDetails extends Model
{
    use HasFactory;

    protected $fillable = [
        'hospital_id',
        'stripe_customer_id',
        'stripe_user_id',
        'is_first',
        'is_verified',
        'is_approved',
        'is_stripe_connected',
        'is_stripe_verify',
        'is_google_sync',
        'is_outlook_sync',
        'is_apple_sync',
        'status',
    ];
}
