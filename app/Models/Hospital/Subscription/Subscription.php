<?php

namespace App\Models\Hospital\Subscription;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Subscription extends Model
{
    use HasFactory;

    protected $fillable = [
        'hospital_id',
        'name',
        'stripe_id',
        'stripe_status',
        'stripe_price',
        'quantity',
        'trial_ends_at',
        'ends_at'
    ];

    public function subscriptionItemDetails(): HasOne
    {
        return $this->hasOne(SubscriptionItem::class, 'subscription_id', 'id');
    }
}
