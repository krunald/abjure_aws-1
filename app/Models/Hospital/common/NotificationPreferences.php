<?php

namespace App\Models\Hospital\common;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NotificationPreferences extends Model
{
    use HasFactory;

    protected $table = "hospital_notification_preferences";
    protected $fillable = [
        'hospital_id',
        'email',
        'text_message',
    ];

    protected $casts = [
        'email' => 'bool',
        'text_message' => 'bool',
    ];

    protected $hidden = [
        'id',
        'hospital_id',
        'created_at',
        'updated_at'
    ];
}
