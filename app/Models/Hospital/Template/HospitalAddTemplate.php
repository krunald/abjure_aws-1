<?php

namespace App\Models\Hospital\Template;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class HospitalAddTemplate extends Model
{
    use HasFactory;

    protected $table = "hospital_job_templates";

    protected $fillable = [
        'hospital_id',
        'template_name',
        'job_title',
        'job_type',
        'slots',
        'min_experience',
        'max_experience',
        'description',
        'status'
    ];



    public function jobDetailsGeneralSurgeon(): HasOne
    {
        return $this->hasOne(GeneralSurgeryTemplate::class, 'template_id', 'id');
    }

    public function jobDetailsHospitalist(): HasOne
    {
        return $this->hasOne(HospitalistTemplate::class, 'template_id', 'id');
    }

}
