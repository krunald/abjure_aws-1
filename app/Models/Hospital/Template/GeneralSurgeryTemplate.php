<?php

namespace App\Models\Hospital\Template;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class GeneralSurgeryTemplate extends Model
{
    use HasFactory;

    protected $table = "hospital_job_template_for_general_surgery";
    protected $fillable = [
        'template_id',
        'is_verified_clinic',
        'clinic_days_per_week',
        'avg_no_of_patients_per_day',
        'avg_no_of_surgeries_per_or_day',
        'emr_used_in_clinic',
        'anesthesia_care',
        'upper_lower_endoscopy',
        'trauma_center_level',
        'avg_no_of_trauma_patients_per_month',
        'c_section',
        'dialysis_access',
        'dialysis_access_value',
    ];

    protected $casts = [
        'is_verified_clinic' => 'bool',
        'upper_lower_endoscopy' => 'bool',
        'c_section' => 'bool',
        'dialysis_access' => 'bool',
    ];

    public function templateType(): BelongsTo
    {
        return $this->belongsTo(HospitalAddTemplate::class, 'template_id', 'id');
    }

}
