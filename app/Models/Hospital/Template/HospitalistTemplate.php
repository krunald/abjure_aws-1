<?php

namespace App\Models\Hospital\Template;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HospitalistTemplate extends Model
{
    use HasFactory;

    protected $table = "hospital_job_template_for_hospitalist";

    protected $fillable = [
        'template_id',
        'family_practice_physicians_apply',
        'hospital_census',
        'avg_admissions_per_provider_by_day',
        'avg_er_admissions_per_provider_by_night',
        'expected_patient_load',
        'hospitalist_in_day',
        'hospitalist_in_night',
        'mid_levels_in_day',
        'mid_levels_in_night',
        'swing_shift_available',
        'backup_plan',
        'mid_level_available',
        'mid_level_available_for_shift',
        'runs_code',
        'respiratory_therapist',
        'respiratory_therapist_allowed_intubate',
        'procedures_required',
        'contract_period_type',
        'flexible_shift_scheduling',
    ];

    protected $casts = [
        'family_practice_physicians_apply' => 'bool',
        'swing_shift_available' => 'bool',
        'mid_level_available' => 'bool',
        'respiratory_therapist' => 'bool',
        'respiratory_therapist_allowed_intubate' => 'bool',
        'flexible_shift_scheduling' => 'bool',
    ];

    public function getProceduresRequiredAttribute($value)
    {
        return explode(',', $value);
    }

    public function setProceduresRequiredAttribute($value)
    {
        $this->attributes['procedures_required'] = implode(",", $value);
    }
}
