<?php

namespace App\Models\Hospital;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PhysicianNotification extends Model
{
    use HasFactory;

    protected $fillable = [
        'sender_id',
        'receiver_id',
        'title',
        'notification_type',
        'notified_on_id',
        'is_read',
        'status',
    ];

    protected $casts = [
        'is_read' =>'bool',
    ];
}
