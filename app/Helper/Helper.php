<?php

namespace App\Helper;

use App\Models\Hospital\PhysicianNotification;
use App\Models\Physician\HospitalNotification;
use App\Utils\AppConstant;
use Carbon\Carbon;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;

class Helper
{
    public static function setUTCDate($date)
    {
        $date_format = date("Y-m-d", strtotime($date));
        $utc_date_time = Carbon::createFromFormat('Y-m-d', $date_format, 'UTC');
        $utc_date_time->setTimezone('UTC');
        return $utc_date_time;
    }

    public static function setUTCDateTime($date, $time)
    {
        $date_format = date("Y-m-d", strtotime($date));
        $time_format = date("H:i:s", strtotime($time));

        $utc_date_time = Carbon::createFromFormat('Y-m-d H:i:s', $date_format . " " . $time_format, 'UTC');
        $utc_date_time->setTimezone('UTC');

        return $utc_date_time;
    }

    public static function sendNotificationToHospital(int $hospitalId, $name, $jobName, $notificationType, $dataObject): void
    {
        $title = '';
        switch ($notificationType) {
            case 'JOB_APPLIED':
                $title = 'New Application for Physician Job: ' . $jobName;
                break;
            case 'PAYMENT_RECEIVED':
                $title = $name . ' you have Received Payment for Job: ' . $jobName;
                break;
            case 'INVOICE_PENDING':
                $title = 'You have received new invoice request its in invoice pending';
                break;
            default:
                break;
        }

        $notificationData = [
            'sender_id' => Auth::id(),
            'receiver_id' => $hospitalId,
            'title' => $title,
            'notification_type' => $notificationType,
            'notified_from' => get_class($dataObject),
            'notified_on_id' => $dataObject->id
        ];
        HospitalNotification::create($notificationData);
    }

    public static function sendNotificationToPhysician(int $physicianId, $name, $jobName, $notificationType, $dataObject): void
    {
        $title = '';
        switch ($notificationType) {
            case AppConstant::REVIEW_GIVEN:
                $title = 'Hospital ' . $name . ' gave you review on your profile.';
                break;
            case AppConstant::PAYMENT_RECEIVED:
                $title = 'You have Received Payment for Job: ' . $jobName . '.';
                break;
            case AppConstant::JOB_COMPLETED:
                $title = 'Jobs is completed by Hospital' . $name . '.';
                break;
            case AppConstant::JOB_APPROVED:
                $title = 'Jobs Approved by Hospital' . $name . '.';
                break;
            default:
                break;
        }

        $notificationData = [
            'sender_id' => Auth::id(),
            'receiver_id' => $physicianId,
            'title' => $title,
            'notification_type' => $notificationType,
            'notified_from' => get_class($dataObject),
            'notified_on_id' => $dataObject->id
        ];
        PhysicianNotification::create($notificationData);
    }

    public static function collectionPaginate($items, $perPage = 10, $page = null, $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);
        return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page);
    }
}
