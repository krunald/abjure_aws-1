<?php

namespace App\Http\Controllers\API\V1\Physician\Home;

use App\Http\Controllers\Controller;
use App\Models\Hospital\Jobs\Jobs;
use App\Models\Hospital\Onboarding\Hospital;
use App\Models\Hospital\Onboarding\HospitalSpecialistAvailable;
use App\Traits\ApiResponse;
use App\Utils\AppConstant;
use Exception;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Database\QueryException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Response;

class HomeController extends Controller
{
    use ApiResponse;

    public $token = null;

    /**
     * Show the application dashboard.
     *
     * @return Renderable
     */
    public function index(): Renderable
    {
        return view('home');
    }

    /**
     * @return JsonResponse
     */
    public function recommendation(): JsonResponse
    {
        try {
            $physician = auth()->user()->with(['personInfo', 'hospitalist', 'generalSuregery', 'licenseIssuerDetails'])->first();

            //fetch physician's total experience
            $physicianExperience = abs($physician->personInfo->experience_year . "." . $physician->personInfo->experience_month);

            $query = Jobs::where('job_type', $physician->expertise)->when($physician->expertise === Jobs::HOSPITALIST, function ($q) use ($physician) {
                $q->whereHas('jobDetailsHospitalist', function ($q) use ($physician) {
                    foreach ($physician->hospitalist->procedures as $p) {
                        $q->orWhere('procedures_required', 'like', '%' . $p . '%');
                    }

                    /*foreach ($physician->licenseIssuerDetails as $l) {
                        $q->orWhere('state', $l->license_state);
                    }*/
                });
                $q->with('jobDetailsHospitalist');
            })->when($physician->expertise === Jobs::GENERAL_SURGERY, function ($q) use ($physician) {
                $q->orWhereHas('jobDetailsGeneralSurgeon', function ($q) use ($physician) {
                    $q->orWhere('upper_lower_endoscopy', $physician->generalSuregery->upper_lower_endoscopy);
                    $q->orWhere('c_section', $physician->generalSuregery->cesarean_section);
                    $q->orWhere('dialysis_access', $physician->generalSuregery->vascular_access);
                    $q->orWhere('dialysis_access', $physician->generalSuregery->vascular_access);
                });

                /*foreach ($physician->licenseIssuerDetails as $l) {
                    $q->orWhere('state', $l->license_state);
                }*/

                $q->with('jobDetailsGeneralSurgeon');
            });

            $query->has('description')->has('jobSchedules')->has('jobPayment');
            $query->with([
                'hospital',
                'userSavedJobs',
                'singleUserJob' => function ($q) use ($physician) {
                    $q->whereUserId($physician->id);
                }
            ])->withCount('userJobs');

            $query->where(function ($q) use ($physicianExperience, $physician) {
                $q->where('job_type', $physician->expertise)
                    ->orWhere('max_experience', '<=', $physicianExperience);
            });

            $paginate = $request->paginate ?? 10;
            $paginatedList = $query->paginate($paginate);

            $pagination = [
                "total" => $paginatedList->total(),
                "current_page" => $paginatedList->currentPage(),
                "next_page_url" => $paginatedList->nextPageUrl(),
                "previous_page_url" => $paginatedList->previousPageUrl(),
            ];

            $recommendation = $query->get();

            $this->setMeta('message', __('messages.physician.recommendation_list_fetched'));
            $this->setMeta('status', AppConstant::STATUS_OK);
            $this->setPaginate($pagination);
            $this->setData('recommendation_list', $recommendation);
            return response()->json($this->setResponse(), AppConstant::OK);
        } catch (QueryException|Exception $e) {
            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function fetchDefaults(Request $request, $type): JsonResponse
    {
        try {
            $sorting = $request->get('q', 'ASC');
            switch ($type) {
                case 'speciality':
                    $query = HospitalSpecialistAvailable::query();
                    break;
                default:
                    $query = Hospital::query();
                    break;
            }
            $data = $query->orderBy('order', $sorting)->get();

            $this->setMeta('message', __('messages.hospital.list_fetched'));
            $this->setMeta('status', AppConstant::STATUS_OK);
            $this->setData('hospital', $data);
            return response()->json($this->setResponse(), AppConstant::OK);
        } catch (QueryException|Exception $e) {
            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }

    }

    public function healthCHeck(): JsonResponse
    {
        $this->setMeta('status', Response::HTTP_OK);
        return response()->json($this->setResponse(), Response::HTTP_OK);
    }

}
