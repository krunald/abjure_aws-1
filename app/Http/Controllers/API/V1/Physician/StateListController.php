<?php

namespace App\Http\Controllers\API\V1\Physician;

use App\Http\Controllers\Controller;
use App\Models\StatesList;
use App\Traits\ApiResponse;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class StateListController extends Controller
{
    use ApiResponse;

    public function index()
    {
        $states = StatesList::all();
        $this->setMeta('status', Response::HTTP_OK);
        $this->setData('specialities', $states);
        return response()->json($this->setResponse(), Response::HTTP_OK);
    }
}
