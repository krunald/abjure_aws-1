<?php

namespace App\Http\Controllers\API\V1\Physician\Jobs;

use App\Helper\Helper;
use App\Http\Controllers\Controller;
use App\Http\Requests\API\V1\Physician\Jobs\GenerateUserJobInvoiceRequest;
use App\Http\Requests\API\V1\Physician\Jobs\UserJobsCreateRequest;
use App\Http\Requests\API\V1\Physician\Jobs\UserJobsFeebackCreateRequest;
use App\Models\Hospital\Jobs\Jobs;
use App\Models\Hospital\Jobs\UserJobsScheduleInterview;
use App\Models\Hospital\Onboarding\Hospital;
use App\Models\Physician\HospitalNotification;
use App\Models\Physician\Jobs\UserJobs;
use App\Models\Physician\Jobs\UserJobsInvoice;
use App\Models\Physician\Jobs\UserJobsSaved;
use App\Models\Physician\Onboarding\User;
use App\Models\Physician\Onboarding\UserJobsDetails;
use App\Traits\ApiResponse;
use App\Utils\AppConstant;
use Carbon\Carbon;
use DateTime;
use Exception;
use Illuminate\Database\QueryException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Collection;

class UserJobsController extends Controller
{
    use ApiResponse;

    public function index(Request $request): JsonResponse
    {
        try {
            $hospitalExpertise = Auth::user()->expertise;
            $filter = isset($request['filter']) ? explode(",", $request['filter']) : [];
            if (isset($request['q']['status']) && $request['q']['status'] === 'SAVED') {
                if ($hospitalExpertise === Jobs::HOSPITALIST) {
                    $jobType = 'jobDetailsHospitalist';
                } else {
                    $jobType = 'jobDetailsGeneralSurgeon';
                }

                $query = User::whereId(Auth::id())->first()->userSavedJobs()->with([
                    $jobType, 'jobPayment', 'jobSchedules',
                    'singleUserJob' => function ($q) {
                        $q->whereUserId(Auth::id());
                    },
                    'description', 'hospital.highlights', 'hospital.jobs.userJobs'
                ])->withCount('userJobs');
                $paginate = $request->paginate ?? 10;
                $paginatedList = $query->paginate($paginate);
                $query = $query->get();

                $pagination = [
                    "total" => $paginatedList->total(),
                    "total_pages" => $paginatedList->lastPage(),
                    "current_page" => $paginatedList->currentPage(),
                    "next_page_url" => $paginatedList->nextPageUrl(),
                    "previous_page_url" => $paginatedList->previousPageUrl(),
                ];
            } else {
                $jobType = 'jobs.jobDetailsGeneralSurgeon';
                if ($hospitalExpertise === Jobs::HOSPITALIST) {
                    $jobType = 'jobs.jobDetailsHospitalist';
                }

                $query = UserJobs::whereUserId(Auth::id())->with([$jobType, 'users.userSavedJobs', 'jobs.userSavedJobs'
                    , 'jobs.hospital.highlights', 'jobs.jobPayment', 'jobs.description', 'jobs.jobSchedules', 'jobs' => function ($q) {
                        $q->withCount('userJobs');
                    }])->when(isset($request['q']['status']), function ($q) use (&$request, $filter) {
                    $q->where(function ($q) use (&$request, $filter) {
                        $requestedData = $request['q']['status'];
                        if ($request['q']['status'] === Jobs::ACTIVE) {
                            $requestedData = UserJobs::OFFER_APPROVED ?? $request['q']['status'];
                            $q->where('application_status', $requestedData);
                        }
                        if ($requestedData === UserJobs::APPLIED) {
                            if (!empty($filter)) {
                                $q->whereIn('application_status', array_merge($filter));
                            } else {
                                $q->whereIn('application_status', [UserJobs::APPLICATION_STATUS['APPLIED'], UserJobs::APPLICATION_STATUS['SCREENING'], UserJobs::APPLICATION_STATUS['INTERVIEW'], UserJobs::APPLICATION_STATUS['REJECTED']]);
                            }
                        }
                        if ($requestedData === UserJobs::COMPLETED) {
                            $q->where('application_status', $requestedData);
                        }
                    });
                });

                $query = collect($query->get())->map(function ($q) {
                    $q['is_saved'] = UserJobsSaved::where(['job_id' => $q->job_id, 'user_id' => Auth::id()])->exists();
                    return $q;
                })->sortBy('id');
                $myCollectionObj = collect($query);

                $data = Helper::collectionPaginate($myCollectionObj);

                $pagination = [
                    "total" => $data->total(),
                    "total_pages" => $data->lastPage(),
                    "current_page" => $data->resolveCurrentPage(),
                    "next_page_url" => $data->nextPageUrl(),
                    "previous_page_url" => $data->previousPageUrl(),
                ];
                $query = $data->items();
            }
            $this->setData('user_jobs', $query);
            $this->setMeta('status', AppConstant::STATUS_OK);
            $this->setPaginate($pagination);
            return response()->json($this->setResponse(), AppConstant::OK);

        } catch (QueryException|Exception $e) {
            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }


    public function invoiceList(Request $request): JsonResponse
    {
        try {
            $userJobInvoices = UserJobs::whereUserId(Auth::id())->whereHas('usersJobsInvoice', function ($q) use ($request) {
                $q->when(isset($request['status']), function ($q) use ($request) {
                    $q->where(function ($q) use ($request) {
                        $q->whereIn('invoice_status', explode(",", $request['status']));
                    });
                });
            });

            $userJobInvoices->with(['usersJobsInvoice', 'jobs.hospital.highlights', 'jobs.description', 'jobs.jobPayment', 'jobs.jobSchedules', 'jobs.jobDetailsGeneralSurgeon', 'jobs.jobDetailsHospitalist', 'jobs.userJobs', 'jobs' => function ($q) {
                return $q->withCount('userJobs');
            }]);

            $paginate = $request->paginate ?? 10;
            $paginatedList = $userJobInvoices->paginate($paginate);

            $pagination = [
                "total" => $paginatedList->total(),
                "total_pages" => $paginatedList->lastPage(),
                "current_page" => $paginatedList->currentPage(),
                "next_page_url" => $paginatedList->nextPageUrl(),
                "previous_page_url" => $paginatedList->previousPageUrl(),
            ];
            $this->setMeta('status', AppConstant::STATUS_OK);
//            $this->setData('user_jobs', $query->get()); //changing on jun 20,2022 for listing of all apppied, saved, active jobs
            $this->setData('users_jobs', $userJobInvoices->orderBy('id', 'DESC')->get());
            $this->setPaginate($pagination);
            return response()->json($this->setResponse(), AppConstant::OK);

        } catch (QueryException|Exception $e) {
            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function show(int $userJobId): JsonResponse
    {
        try {
            $userJob = UserJobs::whereId($userJobId)->with(['users', 'users.personInfo', 'jobs.hospital', 'jobs.jobSchedules', 'jobs.jobPayment', 'usersJobsInvoice'])->first();
            if (!$userJob) {
                $this->setMeta('status', AppConstant::STATUS_FAIL);
                $this->setMeta('message', __('messages.recordNotFound'));
                return response()->json($this->setResponse(), Response::HTTP_NOT_FOUND);
            }
            if ($userJob->application_status === UserJobs::APPLIED || $userJob->application_status === UserJobs::SCREENING || $userJob->application_status === UserJobs::REJECTED) {
                $this->setMeta('status', AppConstant::STATUS_FAIL);
                $this->setMeta('message', "You cannot generate invoice for jobs whose status is not OFFER Approved or Completed");
                return response()->json($this->setResponse(), Response::HTTP_UNPROCESSABLE_ENTITY);
            }

            $userJobSchedule = $userJob->jobs->jobSchedules->sortBy('start_date')->first();
            $lastUserJobSchedule = $userJob->jobs->jobSchedules->sortBy('end_date')->last();
//            $startDate = Carbon::parse($userJobSchedule->start_date);
//            $endDate = Carbon::parse($userJobSchedule->end_date);
//            $startTime = Carbon::parse($userJobSchedule->start_time);
//            $endTime = Carbon::parse($userJobSchedule->end_time);

            $totalHours = 0;
            $totalMinutes = 00;
            if ($userJobSchedule && $lastUserJobSchedule) {
                $datetime1 = new DateTime($userJobSchedule->start_date . ' ' . $userJobSchedule->start_time);
                $datetime2 = new DateTime($lastUserJobSchedule->end_date . ' ' . $lastUserJobSchedule->end_time);
                $interval = $datetime1->diff($datetime2);
                $totalDays = $interval->days + 1;
                $totalHours += $totalDays * $interval->h;
                $totalMinutes += $interval->i;
//
                $totalDiffInHours = $interval->h . ':' . sprintf("%02d", $interval->i);
                $totalWorkedDays = $totalDays;
//            $totalDiffInHours = gmdate('H.i', $startTime->diffInSeconds($endTime));
//            $totalWorkedHours = (double)$totalWorkedDays * (double)$totalDiffInHours;
                $returnArr['user_jobs_id'] = $userJob->id;
                $returnArr['name'] = $userJob->jobs->name;
                $returnArr['date'] = $userJobSchedule->start_date;
                $returnArr['hours_per_day'] = $totalDiffInHours;
                $returnArr['hours'] = $totalHours . ':' . sprintf("%02d", $totalMinutes);
                $returnArr['want_to_add_overtime'] = $userJob->usersJobsInvoice->want_to_add_overtime ?? null;
                $returnArr['overtime_hours'] = $userJob->usersJobsInvoice->overtime_hours ?? null;
                $returnArr['total_worked_days'] = $totalWorkedDays;
                $returnArr['payment_type'] = $userJob->jobs->jobPayment->payment_type;
                $returnArr['rate'] = $userJob->jobs->jobPayment->amount;
                $returnArr['total_amount'] = optional($userJob->usersJobsInvoice)->total_amount ? $userJob->usersJobsInvoice->total_amount : ($userJob->jobs->jobPayment->payment_type === 'HOURLY'
                    ? (double)($totalHours . '.' . $totalMinutes) * (double)$userJob->jobs->jobPayment->amount
                    :
                    $userJob->jobs->jobPayment->amount);
                $userJob['invoice_details'] = $returnArr;
            } else {
                $userJob['invoice_details'] = null;
            }
            $this->setMeta('status', AppConstant::STATUS_OK);
            $this->setData('user_job', $userJob);
            return response()->json($this->setResponse(), AppConstant::OK);
        } catch (QueryException|Exception $e) {
            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function applyToJobs(UserJobsCreateRequest $request): JsonResponse
    {
        try {
            $job = Jobs::whereId($request->job_id)->first();
            if ($job->job_type !== Auth::user()->expertise) {
                $this->setMeta('message', __('messages.physician.jobs.cannotApplyToJobsWithDifferentExpertise'));
                $this->setMeta('status', AppConstant::STATUS_FAIL);
                return response()->json($this->setResponse(), Response::HTTP_FORBIDDEN);
            }
            if (!isset($job->description, $job->jobPayment) && empty($job->jobSchedules->toArray())) {
                $this->setMeta('message', __('messages.physician.jobs.cannotApplyToIncompleteJobs'));
                $this->setMeta('status', AppConstant::STATUS_FAIL);
                return response()->json($this->setResponse(), Response::HTTP_FORBIDDEN);
            }
            $userJobs = UserJobs::updateOrCreate(['user_id' => Auth::id(), 'job_id' => $request->job_id], $request->validated());
            $name = Auth::user()->first_name . ' ' . Auth::user()->last_name;

            Helper::sendNotificationToHospital($job->hospital_id, $name, $job->name, AppConstant::JOB_APPLIED, $userJobs);
            $this->setMeta('status', AppConstant::STATUS_OK);
            $this->setMeta('message', __('messages.physician.jobs.appliedSuccessfully'));
            $this->setData('user_jobs', $userJobs);
            return response()->json($this->setResponse(), AppConstant::OK);
        } catch (QueryException|Exception $e) {
            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function jobsFeedBack(UserJobsFeebackCreateRequest $request)
    {
        try {
            $validatedRequest = $request->validated();
            $userJobs = UserJobs::where(['user_id' => Auth::id(), 'job_id' => $request->job_id])->first();
            if (!$userJobs) {
                $this->setMeta('message', __('messages.recordNotFound'));
                $this->setMeta('status', AppConstant::STATUS_FAIL);
                return response()->json($this->setResponse(), Response::HTTP_NOT_FOUND);
            }

            if ($userJobs->application_status === UserJobs::APPLIED || $userJobs->application_status === UserJobs::REJECTED || $userJobs->application_status === UserJobs::SCREENING) {
                $this->setMeta('message', __('messages.physician.jobs.cannotGenerateInvoiceForOtherStatus'));
                $this->setMeta('status', AppConstant::STATUS_FAIL);
                return response()->json($this->setResponse(), Response::HTTP_UNPROCESSABLE_ENTITY);
            }

            $userJobs->update($validatedRequest);
//            $userJobs = UserJobs::updateOrCreate(['user_id' => Auth::id(), 'job_id' => $request->job_id], $request->validated());
            $this->setMeta('status', AppConstant::STATUS_OK);
            $this->setData('user_jobs', $userJobs);
            return response()->json($this->setResponse(), AppConstant::OK);
        } catch (QueryException|Exception $e) {
            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @param GenerateUserJobInvoiceRequest $request
     * @return JsonResponse
     */
    public function generateUserJobInvoice(GenerateUserJobInvoiceRequest $request): JsonResponse
    {
        try {
            $validated = $request->validated();
            $validated['user_id'] = Auth::id();
            $validated['invoice_status'] = 'PENDING';

            $userJobDetailsAndInvoice = UserJobsInvoice::UpdateOrCreate(
                [
                    'user_jobs_id' => $validated['user_jobs_id']
                ], $validated);

            $this->setMeta('status', AppConstant::STATUS_OK);
            $this->setMeta('message', __('messages.physician.jobs.invoiceSent'));
            $this->setData('user_job_invoice_details', $userJobDetailsAndInvoice);
            return response()->json($this->setResponse(), AppConstant::OK);
        } catch (QueryException|Exception $e) {
            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function acknowledgePaidJobInvoice(int $userJobInvoiceId): JsonResponse
    {
        try {
            $userJobDetailsAndInvoice = UserJobsInvoice::whereId($userJobInvoiceId)->first();
            if ($userJobDetailsAndInvoice->invoice_status !== UserJobsInvoice::PAID) {
                $this->setMeta('message', __('messages.physician.jobs.acknowledgePaidInvoice'));
                $this->setMeta('status', AppConstant::STATUS_FAIL);
                return response()->json($this->setResponse(), Response::HTTP_UNPROCESSABLE_ENTITY);
            }
            $userJobDetailsAndInvoice->acknowledge = true;
            $userJobDetailsAndInvoice->save();

            $this->setMeta('status', AppConstant::STATUS_OK);
            $this->setMeta('message', __('messages.physician.jobs.invoiceSent'));
            $this->setData('user_job_invoice_details', $userJobDetailsAndInvoice);
            return response()->json($this->setResponse(), AppConstant::OK);
        } catch (QueryException|Exception $e) {
            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function getJobsFeedBack($userJobId)
    {
        try {
            $userJobs = UserJobs::whereId($userJobId);
            $this->setMeta('status', AppConstant::STATUS_OK);
            $this->setData('user_jobs', $userJobs->first());
            return response()->json($this->setResponse(), AppConstant::OK);
        } catch (QueryException|Exception $e) {
            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @param int $jobId
     * @return JsonResponse
     */
    public function saveUnSaveJob(int $jobId): JsonResponse
    {
        try {
            $jobs = Jobs::whereId($jobId)->first();
            if (!$jobs) {
                $this->setMeta('message', __('messages.recordNotFound'));
                $this->setMeta('status', AppConstant::STATUS_FAIL);
                return response()->json($this->setResponse(), Response::HTTP_NOT_FOUND);
            }
            if ($jobs->job_type !== Auth::user()->expertise) {
                $this->setMeta('message', __('messages.unauthorizedAccessToRecord'));
                $this->setMeta('status', AppConstant::STATUS_FAIL);
                return response()->json($this->setResponse(), Response::HTTP_FORBIDDEN);
            }
            $user = User::whereId(Auth::id())->first();
            if (!$user->userSavedJobs->contains($jobId)) {
                $user->userSavedJobs()->attach($jobId);
                $this->setMeta('status', AppConstant::STATUS_OK);
                $this->setMeta('message', __('messages.physician.jobs.savedJobSuccessfully'));
                return response()->json($this->setResponse(), AppConstant::OK);
            }
            $user->userSavedJobs()->detach($jobId);
            $this->setMeta('status', AppConstant::STATUS_OK);
            $this->setMeta('message', __('messages.physician.jobs.unsavedJobSuccessfully'));
            return response()->json($this->setResponse(), AppConstant::OK);
        } catch (QueryException|Exception $e) {
            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function cancelJob(int $jobId): JsonResponse
    {
        try {
            $userJob = UserJobs::where(['user_id' => Auth::id(), 'job_id' => $jobId])->first();
            if (!$userJob) {
                $this->setMeta('message', __('messages.recordNotFound'));
                $this->setMeta('status', AppConstant::STATUS_FAIL);
                return response()->json($this->setResponse(), Response::HTTP_NOT_FOUND);
            }
            if ($userJob->user_id !== Auth::id()) {
                $this->setMeta('message', __('messages.unauthorizedAccessToRecord'));
                $this->setMeta('status', AppConstant::STATUS_FAIL);
                return response()->json($this->setResponse(), Response::HTTP_UNAUTHORIZED);
            }
            if ($userJob->application_status !== UserJobs::APPLIED) {
                $this->setMeta('message', __('messages.physician.jobs.canceledAppliedJobOnly'));
                $this->setMeta('status', AppConstant::STATUS_FAIL);
                return response()->json($this->setResponse(), Response::HTTP_UNPROCESSABLE_ENTITY);
            }
            $userJob->delete();
            $this->setMeta('status', AppConstant::STATUS_OK);
            $this->setMeta('message', __('messages.physician.jobs.canceledJobSuccessfully'));
            return response()->json($this->setResponse(), AppConstant::OK);
        } catch (QueryException|Exception $e) {
            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
