<?php

namespace App\Http\Controllers\API\V1\Physician\Jobs;

use App\Http\Controllers\Controller;
use App\Models\Hospital\Jobs\Jobs;
use App\Models\Hospital\Jobs\UserJobReviewAndRating;
use App\Models\Physician\Jobs\UserJobs;
use App\Models\Physician\Jobs\UserJobsSaved;
use App\Models\Physician\Onboarding\User;
use App\Traits\ApiResponse;
use App\Utils\AppConstant;
use Exception;
use Illuminate\Database\QueryException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

class JobsController extends Controller
{
    use ApiResponse;

    public function index(Request $request): JsonResponse
    {
        try {
            /*collect(json_decode($request->q))->mapWithKeys(function ($item, $key) {
                switch ($key) {
                    case 'shift':
                        dump(1);
//                        $q->whereHas('jobSchedules', function ($q) use ($item) {
//                            $q->whereIn('shift', $item);
//                        });
                        break;
                    case 'paymentType':
                        dump(2);
//                        $q->whereHas('jobPayment', function ($q) use ($item) {
//                            $q->where(['payment_type' => $item]);
//                        });
                        break;
                    default:
                        break;
                }
            });
            dd(1);*/
//            dd(json_decode(substr($request['q'], 1, -1)));
//            collect(json_decode($request->q))->mapWithKeys(function ($key, $item) {
//                dd($key, $item);
//            });
            $userExpertise = Auth::user()->expertise;
            $jobType = 'jobDetailsGeneralSurgeon';
            if ($userExpertise === Jobs::HOSPITALIST) {
                $jobType = 'jobDetailsHospitalist';
            }
            $query = Jobs::where('job_type', $userExpertise)->with([$jobType, 'hospital', 'description', 'jobPayment', 'jobSchedules'])->when(isset($request['q']), function ($q) use ($request) {
                $request = json_decode($request['q']);
                $q->when(isset($request->search), function ($q) use ($request) {
                    $q->where('name', 'like', '%' . $request->search->val . '%');
                    $q->whereHas('description', function ($q) use ($request) {
                        $q->where('description', 'like', '%' . $request->search->val . '%');
                    });
                })->when(isset($request->shift), function ($q) use ($request) {
                    $q->whereHas('jobSchedules', function ($q) use ($request) {
                        $q->whereIn('shift', $request->shift->val);
                    });
                })->when(isset($request->paymentType), function ($q) use ($request) {
                    $q->whereHas('jobPayment', function ($q) use ($request) {
                        $q->where('payment_type', '=', $request->paymentType->val);
                    });
                })->when(isset($request->amount), function ($q) use ($request) {
                    $q->whereHas('jobPayment', function ($q) use ($request) {
                        $q->where('amount', '>=', (double)$request->amount->val);
                    });
                })->when(isset($request->date), function ($q) use ($request) {
                    $q->whereHas('jobSchedules', function ($q) use ($request) {
                        $q->where(function ($q) use ($request) {
                            $q->where([
                                ['end_date', '<=', $request->date->val[1]],
                                ['start_date', '>=', $request->date->val[0]]
                            ]);
                        });
                    });
                })->when(isset($request->g_upperLowerEndoscopy), function ($q) use ($request) {
                    $q->whereHas('jobDetailsGeneralSurgeon', function (&$q) use ($request) {
                        $q->where('upper_lower_endoscopy', $request->g_upperLowerEndoscopy->val);
                    });
                })->when(isset($request->g_cSection), function ($q) use ($request) {
                    $q->whereHas('jobDetailsGeneralSurgeon', function ($q) use ($request) {
                        $q->where('c_section', $request->g_cSection->val);
                    });
                })->when(isset($request->g_dialysisAccess), function ($q) use ($request) {
                    $q->whereHas('jobDetailsGeneralSurgeon', function ($q) use ($request) {
                        $q->where('dialysis_access', $request->g_dialysisAccess->val);
                    });
                })->when(isset($request->g_outPatientClinicResponsiblities), function ($q) use ($request) {
                    $q->whereHas('jobDetailsGeneralSurgeon', function ($q) use ($request) {
                        $q->where('is_verified_clinic', $request->g_outPatientClinicResponsiblities->val);
                    });
                })->when(isset($request->h_familyPracticePhysician), function ($q) use ($request) {
                    $q->whereHas('jobDetailsHospitalist', function ($q) use ($request) {
                        $q->where('family_practice_physicians_apply', $request->h_familyPracticePhysician->val);
                    });
                })->when(isset($request->h_respiratoryTherapist), function ($q) use ($request) {
                    $q->whereHas('jobDetailsHospitalist', function ($q) use ($request) {
                        $q->where('respiratory_therapist', $request->h_respiratoryTherapist->val);
                    });
                })->when(isset($request->h_contractPeriodType), function ($q) use ($request) {
                    $q->whereHas('jobDetailsHospitalist', function ($q) use ($request) {
                        $q->where('contract_period_type', $request->h_contractPeriodType->val);
                    });
                });
            })->orderBy('id', 'DESC');

            $paginatedList = $query->paginate($request->paginate ?? 10);
            $pagination = [
                "total" => $paginatedList->total(),
                "current_page" => $paginatedList->currentPage(),
                "next_page_url" => $paginatedList->nextPageUrl(),
                "previous_page_url" => $paginatedList->previousPageUrl(),
            ];
            $this->setMeta('status', AppConstant::STATUS_OK);
            $this->setData('jobs', $paginatedList->getCollection());
            $this->setPaginate($pagination);
            return response()->json($this->setResponse(), AppConstant::OK);
        } catch (QueryException|Exception $e) {
            $this->setMeta('message', $e->getTrace());
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function show($id): JsonResponse
    {
        try {
            $jobs = Jobs::whereId($id)->with(['hospital.hospitalDetails', 'hospital.highlights', 'jobDetailsGeneralSurgeon', 'jobDetailsHospitalist', 'description', 'jobSchedules', 'jobPayment'])->withCount('userJobs')->first();

            $jobs['is_saved'] = UserJobsSaved::where(['job_id' => $id, 'user_id' => Auth::id()])->exists();
            $isGeneratedInvoice = false;

            if (!$jobs) {
                $this->setMeta('message', __('messages.hospital.job.jobNotFound'));
                $this->setMeta('status', AppConstant::STATUS_FAIL);
                return response()->json($this->setResponse(), AppConstant::UNPROCESSABLE_REQUEST);
            }
            if ($jobs->job_type !== Auth::user()->expertise) {
                $this->setMeta('message', __('messages.unauthorizedAccessToRecord'));
                $this->setMeta('status', AppConstant::STATUS_FAIL);
                return response()->json($this->setResponse(), AppConstant::FORBIDDEN);
            }
            if ($jobs->job_type === Jobs::GENERAL_SURGERY) {
                $jobs->unsetRelation('jobDetailsHospitalist');
            } else {
                $jobs->unsetRelation('jobDetailsGeneralSurgeon');
            }
            $userJob = UserJobs::where(['job_id' => $id, 'user_id' => Auth::id()])->first();
            if ($userJob && $userJob->usersJobsInvoice->exists()) {
                $isGeneratedInvoice = true;
            }
            $jobs['is_invoice_generated'] = $isGeneratedInvoice;

            $jobs['review_written'] = UserJobs::where(['user_id' => Auth::id(), 'job_id' => $jobs->id])->first();
            $jobs['my_review'] = UserJobReviewAndRating::whereUserJobsId($jobs['review_written']->id ?? null)->first();
            $this->setMeta('message', __('messages.physician.jobs.detailsFetchedSuccessfully'));
            $this->setMeta('status', AppConstant::STATUS_OK);
            $this->setData('job', $jobs);
            return response()->json($this->setResponse(), AppConstant::OK);
        } catch (QueryException|Exception $e) {

            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
