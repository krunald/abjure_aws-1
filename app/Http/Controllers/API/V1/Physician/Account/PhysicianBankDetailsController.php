<?php

namespace App\Http\Controllers\API\V1\Physician\Account;

use App\Http\Controllers\Controller;
use App\Http\Requests\API\V1\Physician\Account\UpdateBankDetailsRequest;
use App\Models\Physician\Account\PhysicianBankDetails;
use App\Traits\ApiResponse;
use App\Utils\AppConstant;
use Exception;
use Illuminate\Database\QueryException;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

class PhysicianBankDetailsController extends Controller
{
    use ApiResponse;

    /**
     * @return JsonResponse
     */
    public function fetchBankDetails(): JsonResponse
    {
        try {
            $physicianId = Auth::id();
            $data = PhysicianBankDetails::whereUserId($physicianId)->first();

            $this->setMeta('message', __('messages.physician.bankDetails.fetch'));
            $this->setMeta('status', AppConstant::STATUS_OK);
            $this->setData('bank_details', $data);
            return response()->json($this->setResponse(), AppConstant::OK);
        } catch (QueryException|Exception $e) {
            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @param UpdateBankDetailsRequest $request
     * @return JsonResponse
     */
    public function updateBankDetails(UpdateBankDetailsRequest $request): JsonResponse
    {
        try {
            $validated = $request->validated();
            $physicianId = Auth::id();
            $validated['user_id'] = $physicianId;

            $data = PhysicianBankDetails::updateOrCreate(
                [
                    'user_id' => $physicianId
                ],
                $validated
            );

            $this->setMeta('message', __('messages.physician.bankDetails.update'));
            $this->setMeta('status', AppConstant::STATUS_OK);
            $this->setData('bank_details', $data);
            return response()->json($this->setResponse(), AppConstant::OK);
        } catch (QueryException|Exception $e) {
            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
