<?php

namespace App\Http\Controllers\API\V1\Physician\Account;

use App\Http\Controllers\Controller;
use App\Http\Requests\API\V1\Physician\Account\EmploymentPreferenceRequest;
use App\Models\Physician\Account\EmploymentShiftPreference;
use App\Models\Physician\Account\EmploymentStatePreference;
use App\Models\Physician\Account\ShiftPreference;
use App\Models\Physician\Onboarding\User;
use App\Traits\ApiResponse;
use App\Utils\AppConstant;
use Exception;
use Illuminate\Database\QueryException;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

class EmploymentPreferenceController extends Controller
{
    use ApiResponse;

    /**
     * @return JsonResponse
     */
    public function fetchEmploymentPreference(): JsonResponse
    {
        try {
            $physicianId = Auth::id();
            $shiftData = EmploymentShiftPreference::where('user_id', $physicianId)->get();
            $stateData = EmploymentStatePreference::where('user_id', $physicianId)->get();

            $this->setMeta('message', __('messages.physician.notifications.update'));
            $this->setMeta('status', AppConstant::STATUS_OK);
            $this->setData('shift_preferences', $shiftData);
            $this->setData('state_preferences', $stateData);
            return response()->json($this->setResponse(), AppConstant::OK);
        } catch (QueryException|Exception $e) {
            $this->setMeta('message', $e->getMessage());
//            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @param EmploymentPreferenceRequest $request
     * @return JsonResponse
     */
    public function updateEmploymentPreference(EmploymentPreferenceRequest $request): JsonResponse
    {
        try {
            $validated = $request->validated();
            $physicianId = Auth::id();

            if(!empty($validated['shifts'])){
                User::find($physicianId)->shifts()->sync($validated['shift']);
            }

            if(!empty($validated['state_id'])){
                User::find($physicianId)->states()->sync($validated['state_id']);
            }

            $user = User::find($physicianId)->with(['shifts', 'states'])->first();
            $this->setMeta('message', __('messages.physician.notifications.update'));
            $this->setMeta('status', AppConstant::STATUS_OK);
            $this->setData('state_preferences', $user);
            return response()->json($this->setResponse(), AppConstant::OK);
        } catch (QueryException|Exception $e) {
            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
