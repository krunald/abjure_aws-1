<?php

namespace App\Http\Controllers\API\V1\Physician\Common;

use App\Http\Controllers\Controller;
use App\Http\Requests\API\V1\Hospital\Common\UpdateNotificationPreferenceRequest;
use App\Models\Hospital\common\NotificationPreferences;
use App\Models\Physician\common\PhysicianNotificationPreference;
use App\Traits\ApiResponse;
use App\Utils\AppConstant;
use Exception;
use Illuminate\Database\QueryException;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

class PhysicianNotificationPreferenceController extends Controller
{
    use ApiResponse;

    /**
     * @param UpdateNotificationPreferenceRequest $request
     * @return JsonResponse
     */
    public function updatePreferences(UpdateNotificationPreferenceRequest $request): JsonResponse
    {
        try {
            $validated = $request->validated();
            $userId = Auth::id();
            PhysicianNotificationPreference::whereUserId($userId)->update($validated);

            $this->setMeta('message', __('messages.physician.notifications.update'));
            $this->setMeta('status', AppConstant::STATUS_OK);
            $this->setData('preferences', PhysicianNotificationPreference::whereUserId($userId)->first());
            return response()->json($this->setResponse(), AppConstant::OK);
        } catch (QueryException|Exception $e) {
            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @return JsonResponse
     */
    public function fetchPreferences(): JsonResponse
    {
        try {
            $notificationPreferences = PhysicianNotificationPreference::whereUserId(Auth::id())
                ->first();

            $this->setMeta('message', __('messages.physician.notifications.fetch'));
            $this->setMeta('status', AppConstant::STATUS_OK);
            $this->setData('preferences', $notificationPreferences);
            return response()->json($this->setResponse(), AppConstant::OK);
        } catch (QueryException|Exception $e) {
            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
