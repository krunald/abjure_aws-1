<?php

namespace App\Http\Controllers\API\V1\Physician\Statistics;

use App\Http\Controllers\Controller;
use App\Models\Hospital\Jobs\UserJobsScheduleInterview;
use App\Models\Physician\Jobs\UserJobs;
use App\Models\Physician\Jobs\UserJobsInvoice;
use App\Traits\ApiResponse;
use App\Utils\AppConstant;
use Carbon\Carbon;
use Exception;
use Illuminate\Database\QueryException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

class StatisticsController extends Controller
{
    use ApiResponse;

    /**
     * @return JsonResponse
     */
    public function jobsAppliedCounts(): JsonResponse
    {
        try {
            $physicianId = Auth::id();
            $response['jobs_applied'] = UserJobs::where([
                'user_id' => $physicianId,
                'application_status' => UserJobs::APPLIED
            ])->count();

            $response['interview_schedule'] = UserJobsScheduleInterview::whereUserId($physicianId)->where('interview_date', '>=', Carbon::now()->toDateString())->count();

            $response['pending_invoice'] = UserJobsInvoice::where(
                [
                    'user_id' => $physicianId,
                    'invoice_status' => UserJobs::PENDING
                ])->count();

            $this->setMeta('status', AppConstant::STATUS_OK);
            $this->setData('dashboard_cards', $response);
            return response()->json($this->setResponse(), AppConstant::OK);
        } catch (QueryException|Exception $e) {
            $this->setMeta('message', $e->getMessage());
            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function fetchUpcomingInterviews(Request $request): JsonResponse
    {
        try {
            $interviews = UserJobsScheduleInterview::where([
                ['user_id', '=', Auth::id()],
                ['interview_date', '>=', Carbon::now()->toDateString()]
            ])->with(['userJob.jobs', 'hospital'])->orderBy('id', 'DESC')->get();

            $this->setMeta('message', __('messages.physician.fetchedInterViewListSuccessfully'));
            $this->setMeta('status', AppConstant::STATUS_OK);
            $this->setData('upcoming_interviews', $interviews);
            return response()->json($this->setResponse(), Response::HTTP_OK);
        } catch (QueryException|Exception $e) {
            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function fetchApplicationStatus(Request $request): JsonResponse
    {
        try {
            $filterType = $request->get('q');
            $filterNumber = $request->get('n');
            $physicianId = Auth::id();

            $userJobs = UserJobs::select('id', 'application_status', 'job_id')
                ->whereHas('jobs', function ($query) use ($physicianId) {
                    $query->where('user_id', $physicianId);
                });
            $userJobs = $this->getFIlter($filterType, $filterNumber, $userJobs);

            $data = $userJobs->transform(function ($jobs, $key) use (&$item) {
                return [
                    strtolower($key) => $jobs->count()
                ];
            })->values()->toArray();
            $data = array_merge(...$data);
            $data = empty($data) ? null : $data;
            $this->setMeta('status', AppConstant::STATUS_OK);
            $this->setMeta('message', __('messages.hospital.dashboard.fetchPendingApplications'));
            $this->setData('application_status', $data);
            return response()->json($this->setResponse(), AppConstant::OK);
        } catch (QueryException|Exception $e) {
            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @param $filter
     * @param $filterNumber
     * @param $userJobs
     * @return mixed
     */
    protected function getFilter($filter, $filterNumber, $userJobs)
    {
        if (strtolower($filter) === "days") {
            $userJobs = $userJobs->whereDate('created_at', '>=', Carbon::now()->subDays($filterNumber));
        } elseif (strtolower($filter) === "month") {
            $userJobs = $userJobs->whereDate('created_at', '>=', Carbon::now()->subMonth($filterNumber));
        } elseif (strtolower($filter) === "year") {
            $userJobs = $userJobs->whereDate('created_at', '>=', Carbon::now()->subYear($filterNumber));
        }

        return $userJobs->get()->groupBy(['application_status']);
    }
}
