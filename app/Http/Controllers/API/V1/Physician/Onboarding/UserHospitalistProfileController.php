<?php

namespace App\Http\Controllers\API\V1\Physician\Onboarding;

use App\Http\Controllers\Controller;
use App\Http\Requests\API\V1\Physician\OnBoarding\UserHospitalistProfileCreateRequest;
use App\Http\Requests\API\V1\Physician\OnBoarding\UserHospitalistProfileUpdateRequest;
use App\Models\Physician\Onboarding\UserDetailsStatus;
use App\Models\Physician\Onboarding\UserHospitalityProfileDetails;
use App\Traits\ApiResponse;
use App\Utils\AppConstant;
use Exception;
use Illuminate\Database\QueryException;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Response;

class UserHospitalistProfileController extends Controller
{
    use ApiResponse;

    public function store(UserHospitalistProfileCreateRequest $request): JsonResponse
    {
        try {
            DB::beginTransaction();
            $userId = Auth::id();
            $request['user_id'] = $userId;

            $requestData = $request->only(
                'user_id',
                'icu_management',
                'max_desired_patient_load_flexible',
                'max_desired_patient_load_numbers',
                'high_volume_care_part_of_practice',
                'procedures'
            );
            $userHospitality = UserHospitalityProfileDetails::updateOrCreate(['user_id' => $userId], $requestData);
            if ($userHospitality) {
                UserDetailsStatus::updateOrCreate(['user_id' => $request->user_id], [UserDetailsStatus::PROFILE_EXPERTISE_DETAILS => true]);
            }
            $this->setMeta('status', AppConstant::STATUS_OK);
            $this->setMeta('message', __('messages.physician.onBoarding.hospitalistDetailsAdded'));
            $this->setData('hospitality_profile_details', $userHospitality);
            DB::commit();
            return response()->json($this->setResponse(), Response::HTTP_OK);
        } catch (QueryException|Exception $e) {
            DB::rollBack();
            $this->setMeta('message',  __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function update(UserHospitalistProfileUpdateRequest $request): JsonResponse
    {
        try {
            $userId = Auth::id();
            $request['user_id'] = $userId;
            $userHospitality = UserHospitalityProfileDetails::whereUserId($userId)->first();
            if (!$userHospitality) {
                $this->setMeta('status', AppConstant::STATUS_FAIL);
                $this->setMeta('message', __('messages.recordNotFound'));
                return response()->json($this->setResponse(), Response::HTTP_NOT_FOUND);
            }


            $requestData = $request->only(
                'user_id',
                'icu_management',
                'max_desired_patient_load_flexible',
                'max_desired_patient_load_numbers',
                'high_volume_care_part_of_practice',
                'procedures'
            );

            $userHospitality->update($requestData);
            $this->setMeta('status', AppConstant::STATUS_OK);
            $this->setMeta('message', __('messages.physician.onBoarding.hospitalistDetailsUpdated'));
            $this->setData('hospitality_profile_details', $userHospitality);
            return response()->json($this->setResponse(), Response::HTTP_OK);
        } catch (QueryException|Exception $e) {
            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function show(): JsonResponse
    {
        try {
            $userHospitality = UserHospitalityProfileDetails::whereUserId(Auth::id())->first();

            $this->setMeta('status', AppConstant::STATUS_OK);
            $this->setMeta('message', __('messages.physician.onBoarding.hospitalistDetailsFetched'));
            $this->setData('hospitalist_profile_details', $userHospitality);
            return response()->json($this->setResponse(), Response::HTTP_OK);
        } catch (QueryException|Exception $e) {
            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
