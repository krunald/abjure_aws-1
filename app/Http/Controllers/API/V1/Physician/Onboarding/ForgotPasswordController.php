<?php

namespace App\Http\Controllers\API\V1\Physician\Onboarding;

use App\Http\Controllers\Controller;
use App\Http\Requests\API\V1\Physician\OnBoarding\ForgotPasswordRequest;
use App\Http\Requests\API\V1\Physician\OnBoarding\PhysicianUpdatePasswordRequest;
use App\Http\Requests\API\V1\Physician\OnBoarding\PhysicianVerifyPasswordRequest;
use App\Mail\User\ResetPassword;
use App\Models\PasswordReset;
use App\Models\Physician\Onboarding\PhysicianForgotPassword;
use App\Models\Physician\Onboarding\User;
use App\Models\PhysicianUserAccess;
use App\Traits\ApiResponse;
use App\Utils\AppConstant;
use Carbon\Carbon;
use Exception;
use Illuminate\Database\QueryException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Str;
use Symfony\Component\HttpFoundation\Response;

class ForgotPasswordController extends Controller
{
    use ApiResponse;

    public function forgotPassword(ForgotPasswordRequest $request): JsonResponse
    {
        try {
            $validated = $request->validated();
            $email = $validated['email'];

            $physician = User::whereEmail($email)->select('first_name', 'last_name', 'email', 'user_is_active')->first();

            if (!$physician) {
                $this->setMeta('message', __('messages.userNotFound'));
                $this->setMeta('status', AppConstant::STATUS_FAIL);
                return response()->json($this->setResponse(), Response::HTTP_UNPROCESSABLE_ENTITY);
            }

            if ($physician->user_is_active == 0) {
                $this->setMeta('message', __('messages.accountBlocked'));
                $this->setMeta('status', AppConstant::STATUS_FAIL);
                return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
            }

            $resetToken = Str::random(60);
            PhysicianForgotPassword::updateOrCreate(
                [
                    'email' => $email
                ],
                [
                    'token' => $resetToken,
                ]
            );

            $tokenData = PhysicianForgotPassword::whereEmail($email)->first();
            $this->sendResetEmail($email, $tokenData->token);

            $this->setMeta('message', __('messages.password.verifyPassword.sent'));
            $this->setMeta('status', AppConstant::STATUS_OK);
            return response()->json($this->setResponse(), Response::HTTP_OK);
        } catch (QueryException|Exception $e) {
            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function verifyToken(array $validated)
    {

        // Validate the token
        $tokenData = PhysicianForgotPassword::where(
            [
                'email' => $validated['email'],
                'token' => $validated['token']
            ]
        )->first();

        // Redirect the user back to the password reset request form if the token is invalid
        if (!$tokenData) {
            return false;
        }

        $startTime = Carbon::parse($tokenData->updated_at);
        $finishTime = Carbon::now();

        $totalDuration = $finishTime->diffInSeconds($startTime);
        $totalDuration = abs(gmdate('i', $totalDuration));

        if ($totalDuration > 5) {
            return false;
        }
        return true;
    }

    public function verifyPasswordResetToken(PhysicianVerifyPasswordRequest $request): JsonResponse
    {
        try {
            $validated = $request->validated();
            $tokenVerified = $this->verifyToken($validated);
            if (!$tokenVerified) {
                $this->setMeta('message', __('messages.token.resetTokenExpiredOrInvalidData'));
                $this->setMeta('status', AppConstant::STATUS_FAIL);
                return response()->json($this->setResponse(), Response::HTTP_UNPROCESSABLE_ENTITY);
            }
            $this->setMeta('message', __('messages.token.verified'));
            $this->setMeta('status', AppConstant::STATUS_OK);
            return response()->json($this->setResponse(), Response::HTTP_OK);
        } catch (QueryException|Exception $e) {
            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    private function sendResetEmail($email, $token): bool
    {
        //Retrieve the user from the database
        $physician = User::whereEmail($email)->select('first_name', 'last_name', 'email')->first();

        //Generate, the password reset link. The token generated is embedded in the link
        $link = config('appconstants.PHYSICIAN_URL') . '/password/reset-password?token=' . $token . '&email=' . urlencode($physician->email);
        $physician['link'] = $link;
        $physician['name'] = $physician['first_name'] . ' ' . $physician['last_name'];
        try {
            Mail::to($email)->send(new ResetPassword($physician));
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    public function showResetPasswordForm()
    {
        $data['token'] = $_GET['token'];
        $data['email'] = $_GET['email'];
        return view('Physician.Onboarding.ForgotPassword')->with(['data' => $data]);
    }

    public function updatePassword(PhysicianUpdatePasswordRequest $request)
    {
        try {
            $validated = $request->validated();
            $password = $validated['password'];
            unset($validated['password']);

            $tokenVerified = $this->verifyToken($validated);

            if (!$tokenVerified) {
                $this->setMeta('message', __('messages.token.resetTokenExpired'));
                $this->setMeta('status', AppConstant::STATUS_FAIL);
                return response()->json($this->setResponse(), Response::HTTP_UNPROCESSABLE_ENTITY);
            }

            $physician = User::whereEmail($validated['email'])->first();

            // Redirect the user back if the email is invalid
            if (!$physician) {
                $request->session()->flash('password_error_message', __('messages.userNotFound'));
                return Redirect::back();
            }

            $physician_all_token = PhysicianUserAccess::where('user_id', $physician->id)->get('access_token');

            foreach ($physician_all_token as $t) {
                PhysicianUserAccess::where('access_token', '=', $t['access_token'])->delete();
            }

            //Hash and update the new password
            $physician->password = bcrypt($password);
            $physician->update(); //or $physician->save();

            //Delete the token
            PhysicianForgotPassword::whereEmail($physician->email)
                ->delete();

            $this->setMeta('message', __('messages.password.set'));
            $this->setMeta('status', AppConstant::STATUS_OK);
            return response()->json($this->setResponse(), Response::HTTP_OK);
        } catch (QueryException|Exception $e) {
            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
