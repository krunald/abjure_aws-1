<?php

namespace App\Http\Controllers\API\V1\Physician\Onboarding;

use App\Http\Controllers\Controller;
use App\Http\Requests\API\V1\Physician\OnBoarding\UserDisciplinaryActionCreateRequest;
use App\Http\Requests\API\V1\Physician\OnBoarding\UserDisciplinaryActionUpdateRequest;
use App\Models\Physician\Onboarding\UserDetailsStatus;
use App\Models\Physician\Onboarding\UserDisciplinaryActions;
use App\Traits\ApiResponse;
use App\Utils\AppConstant;
use Exception;
use Illuminate\Database\QueryException;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Response;

class UserDisciplinaryActionController extends Controller
{
    use ApiResponse;

    public function store(UserDisciplinaryActionCreateRequest $request): JsonResponse
    {
        try {
            DB::beginTransaction();
            $userId = Auth::id();
            $request['user_id'] = $userId;
            $userDisciplinaryAction = UserDisciplinaryActions::updateOrCreate(['user_id' => Auth::id()], $request->all());
            if ($userDisciplinaryAction) {
                UserDetailsStatus::updateOrCreate(['user_id' => $userId], [UserDetailsStatus::DISCIPLINARY_ACTIONS => true]);
            }
            $this->setMeta('status', AppConstant::STATUS_OK);
            $this->setMeta('message', __('messages.physician.onBoarding.disciplinaryActionDetailsAdded'));
            $this->setData('data', $userDisciplinaryAction);
            DB::commit();
            return response()->json($this->setResponse(), Response::HTTP_OK);
        } catch (QueryException|Exception $e) {
            DB::rollBack();
            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function update(UserDisciplinaryActionUpdateRequest $request): JsonResponse
    {
        try {
            $requestData = $request->only('ever_convicted',
                'ever_convicted_explanation',
                'ever_denied_clinical_privileges',
                'ever_denied_clinical_privileges_explanation',
                'ever_denied_professional_liability_insurance',
                'ever_denied_professional_liability_insurance_explanation',
                'ever_denied_or_suspended_license_in_any_jurisdiction',
                'ever_denied_or_suspended_license_in_any_jurisdiction_explanation',
                'ever_denied_or_suspended_participation',
                'ever_denied_or_suspended_participation_explanation',
                'ever_treated_alcoholism_and_substance_abuse',
                'ever_treated_alcoholism_and_substance_abuse_explanation',
                'ever_been_advised_to_seek_treatment',
                'ever_been_advised_to_seek_treatment_explanation',
                'condition_that_can_affect_practicing_medicine',
                'condition_that_can_affect_practicing_medicine_explanation',
                'presently_involved_in_use_of_illegal_substance',
                'presently_involved_in_use_of_illegal_substance_explanation');
            $userDisciplinaryAction = UserDisciplinaryActions::whereUserId(Auth::id());
            if (!$userDisciplinaryAction) {
                $this->setMeta('status', AppConstant::STATUS_FAIL);
                $this->setMeta('message', __('messages.recordNotFound'));
                return response()->json($this->setResponse(), Response::HTTP_NOT_FOUND);
            }
            DB::beginTransaction();
            $userDisciplinaryAction->update($requestData);
            $this->setMeta('status', AppConstant::STATUS_OK);
            $this->setMeta('message', __('messages.physician.onBoarding.disciplinaryActionDetailsUpdated'));
            $this->setData('data', $userDisciplinaryAction->first());
            DB::commit();
            return response()->json($this->setResponse(), Response::HTTP_OK);
        } catch (QueryException|Exception $e) {
            DB::rollBack();
            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function show(): JsonResponse
    {
        try {
            $userDisciplinaryAction = UserDisciplinaryActions::whereUserId(Auth::id());
            if (!$userDisciplinaryAction) {
                $this->setMeta('status', AppConstant::STATUS_FAIL);
                $this->setMeta('message', __('messages.recordNotFound'));
                return response()->json($this->setResponse(), Response::HTTP_NOT_FOUND);
            }
            $this->setMeta('status', AppConstant::STATUS_OK);
            $this->setMeta('message', __('messages.physician.onBoarding.disciplinaryActionDetailsFetched'));
            $this->setData('data', $userDisciplinaryAction->first());
            return response()->json($this->setResponse(), Response::HTTP_OK);
        } catch (QueryException $qe) {
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            $this->setMeta('message', __('messages.somethingWrong'));
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        } catch (Exception $e) {
            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
