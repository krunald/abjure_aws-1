<?php

namespace App\Http\Controllers\API\V1\Physician\Onboarding;

use App\Http\Controllers\Controller;
use App\Http\Requests\API\V1\Physician\OnBoarding\UserWorkReferencesCreateRequest;
use App\Http\Requests\API\V1\Physician\OnBoarding\UserWorkReferencesUpdateRequest;
use App\Models\Physician\Onboarding\User;
use App\Models\Physician\Onboarding\UserDetailsStatus;
use App\Models\Physician\Onboarding\UserReferencesDetails;
use App\Traits\ApiResponse;
use App\Utils\AppConstant;
use Exception;
use Illuminate\Database\QueryException;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Response;

class UserWorkReferencesController extends Controller
{
    use ApiResponse;

    public function store(UserWorkReferencesCreateRequest $request): JsonResponse
    {
        try {
            DB::beginTransaction();

            $userId = Auth::id();
            $requestData = [];

            collect($request->work_references)->each(function ($item) use ($userId, &$requestData) {
                $item['user_id'] = $userId;
                $requestData[] = $item;
            });
            $userWorkReferences = [];
            collect($requestData)->map(function ($req) use (&$userWorkReferences) {
                $userWorkReferences[] = UserReferencesDetails::create($req);
            });

            if (count($userWorkReferences) > 0) {
                UserDetailsStatus::updateOrCreate(['user_id' => $request->user_id], [UserDetailsStatus::USER_REFERENCES => true]);
            }

            $userReferencesDetails = User::with('referencesDetails')->whereId($userId)->first()->referencesDetails;

            $this->setMeta('status', AppConstant::STATUS_OK);
            $this->setMeta('message', __('messages.physician.onBoarding.workReferencesDetailsAdded'));
            $this->setData('references_details', $userReferencesDetails);
            DB::commit();
            return response()->json($this->setResponse(), Response::HTTP_OK);
        } catch (QueryException $qe) {
            DB::rollBack();
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            $this->setMeta('message', $qe->getMessage());
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        } catch (Exception $e) {
            DB::rollBack();
            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function update(UserWorkReferencesUpdateRequest $request): JsonResponse
    {
        try {
            DB::beginTransaction();
            $userId = Auth::id();
            UserReferencesDetails::whereUserId($userId)->delete();
            collect($request->work_references)->map(function ($reqData) use ($userId) {
                $reqData['user_id'] = $userId;
                UserReferencesDetails::create($reqData);
            });

            $userReferencesDetails = User::with('referencesDetails')->whereId($userId)->first()->referencesDetails;
            $this->setMeta('status', AppConstant::STATUS_OK);
            $this->setMeta('message', __('messages.physician.onBoarding.workReferencesDetailsUpdated'));
            $this->setData('references_details', $userReferencesDetails);
            DB::commit();
            return response()->json($this->setResponse(), Response::HTTP_OK);
        } catch (QueryException|Exception $e) {
            DB::rollBack();
            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function destroy(int $id): JsonResponse
    {
        try {
            DB::beginTransaction();
            $userReference = UserReferencesDetails::whereId($id)->first();
            if (!$userReference) {
                $this->setMeta('status', AppConstant::STATUS_FAIL);
                $this->setMeta('message', __('messages.recordNotFound'));
                return response()->json($this->setResponse(), Response::HTTP_NOT_FOUND);
            }
            $userReference->delete();
            DB::commit();
            $this->setMeta('status', AppConstant::STATUS_OK);
            $this->setMeta('message', __('messages.physician.onBoarding.workReferencesDetailsDeleted'));
            return response()->json($this->setResponse(), Response::HTTP_OK);
        } catch (QueryException|Exception $e) {
            DB::rollBack();
            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function show(): JsonResponse
    {
        try {
            $userReference = UserReferencesDetails::whereUserId(Auth::id())->get();

            $this->setMeta('status', AppConstant::STATUS_OK);
            $this->setData('references_details', $userReference);
            $this->setMeta('message', __('messages.physician.onBoarding.workReferencesDetailsDeleted'));
            return response()->json($this->setResponse(), Response::HTTP_OK);
        } catch (QueryException $qe) {
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            $this->setMeta('message', __('messages.somethingWrong'));
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        } catch (Exception $e) {
            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
