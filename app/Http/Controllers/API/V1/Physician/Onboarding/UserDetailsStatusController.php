<?php

namespace App\Http\Controllers\API\V1\Physician\Onboarding;

use App\Http\Controllers\Controller;
use App\Models\Physician\Onboarding\UserDetailsStatus;
use App\Traits\ApiResponse;
use App\Utils\AppConstant;
use Illuminate\Http\JsonResponse;

class UserDetailsStatusController extends Controller
{

    use ApiResponse;

    public function show($userId)
    {
        $this->setMeta('status', AppConstant::STATUS_OK);
        $this->setData('data', UserDetailsStatus::whereUserId($userId)->first());
        return response()->json($this->setResponse(), JsonResponse::HTTP_OK);
    }
}
