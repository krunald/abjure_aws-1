<?php

namespace App\Http\Controllers\API\V1\Physician\Onboarding;

use App\Http\Controllers\Controller;
use App\Http\Requests\API\V1\Hospital\Onboarding\ChangePasswordRequest;
use App\Http\Requests\API\V1\Physician\OnBoarding\UploadProfilePictureRequest;
use App\Http\Requests\API\V1\Physician\OnBoarding\UserCreateRequest;
use App\Http\Requests\API\V1\Physician\OnBoarding\UserUpdateRequest;
use App\Jobs\Physician\SendVerificationMailJob;
use App\Models\Physician\common\PhysicianNotificationPreference;
use App\Http\Requests\API\V1\Physician\VerifyEmailRequest;
use App\Mail\User\VerifyEmail;
use App\Models\Physician\Onboarding\PhysicianVerifyEmail;
use App\Models\Physician\Onboarding\User;
use App\Models\PhysicianUserAccess;
use App\Traits\ApiResponse;
use App\Utils\AppConstant;
use Carbon\Carbon;
use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Database\QueryException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Str;
use Ramsey\Uuid\Uuid;
use Symfony\Component\HttpFoundation\Response;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Token;

class UserController extends Controller
{

    use ApiResponse;

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function authenticate(Request $request): JsonResponse
    {
        $credentials = $request->only('email', 'password');

        //valid credential
        $validator = Validator::make($credentials, [
            'email' => 'required|email',
            'password' => 'required'
        ]);
        $validatedRequest = $validator->validated();
        //Send failed response if request is not valid
        if ($validator->fails()) {
            return response()->json($this->setQueryExceptionResponse($validator->messages()), JsonResponse::HTTP_BAD_GATEWAY);
        }

        DB::beginTransaction();
        $physician = User::whereEmail($validatedRequest['email'])->with(['userDetailsStatus'])->first();
        if (!isset($physician)) {
            $this->setMeta('message', __('messages.invalidCredentials'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return new JsonResponse($this->setResponse(), Response::HTTP_NOT_FOUND);
        }

        $claim_credentials = [
            'user_id' => $physician['id'],
            'name' => $physician['last_name'] ? $physician['first_name'] . ' ' . $physician['last_name'] : $physician['first_name'],
            'email' => $physician['email']
        ];

        try {
            if (!$jwt_token = JWTAuth::claims($claim_credentials)->attempt($validatedRequest)) {
                $this->setMeta('message', __('messages.invalidCredentials'));
                $this->setMeta('status', AppConstant::STATUS_FAIL);
                return new JsonResponse($this->setResponse(), Response::HTTP_NOT_FOUND);
            }

            PhysicianUserAccess::Create([
                'user_id' => $physician['id'],
                'access_token' => $jwt_token
            ]);

            PhysicianNotificationPreference::create([
                'user_id' => $physician->id,
                'email' => 1,
                'text_message' => 1
            ]);

            $access_token = $this->createTokenObj($jwt_token);
            DB::commit();
            $this->setMeta('message', __('messages.login'));
            $this->setMeta('status', AppConstant::STATUS_OK);
            $this->setData('physician', $physician);
            $this->setData('physician_token', $access_token);
            return new JsonResponse($this->setResponse(), Response::HTTP_OK);
        } catch (QueryException|Exception $e) {
            DB::rollBack();
            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return new JsonResponse($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function logout(Request $request): JsonResponse
    {
        try {
            $token = str_replace('Bearer ', '', $request->header('Authorization'));
            PhysicianUserAccess::where('access_token', '=', $token)->delete();
            JWTAuth::invalidate(JWTAuth::parseToken());
            auth()->logout();

            $this->setMeta('message', __('messages.logout'));
            $this->setMeta('status', AppConstant::STATUS_OK);
            return new JsonResponse($this->setResponse(), AppConstant::OK);
        } catch (QueryException|\Exception $e) {
            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return new JsonResponse($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @param UserCreateRequest $request
     * @return JsonResponse
     */
    public function store(UserCreateRequest $request): JsonResponse
    {
        try {
            DB::beginTransaction();
            $validated = $request->validated();
            $data = User::create(array_merge(
                $validated,
                ['password' => bcrypt($request->password), 'uuid' => Uuid::uuid4()->toString()]
            ));
            DB::commit();
            $this->setMeta('message', __('messages.registered'));
            $this->setMeta('status', AppConstant::STATUS_OK);
            $this->setData('physician', $data);
            return new JsonResponse($this->setResponse(), Response::HTTP_OK);
        } catch (QueryException|Exception $e) {
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            $this->setMeta('message', __('messages.somethingWrong'));
            return new JsonResponse($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @param $token
     * @return array
     */
    protected function createTokenObj($token): array
    {
        return [
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth('api')->factory()->getTTL() * 3600,
        ];
    }

    /**
     * @param $id
     * @return JsonResponse
     */
    public function show($id): JsonResponse
    {
        try {
            $this->setMeta('status', AppConstant::STATUS_OK);
            $this->setData('user', User::whereId(Auth::id())->first());
            return new JsonResponse($this->setResponse(), Response::HTTP_OK);
        } catch (QueryException $qe) {
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            $this->setMeta('message', __('messages.somethingWrong'));
            return new JsonResponse($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        } catch (Exception $e) {
            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return new JsonResponse($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @return JsonResponse
     */
    public function currentUser(): JsonResponse
    {
        try {
            $user = User::whereId(Auth::id())->first();
            $this->setMeta('status', AppConstant::STATUS_OK);
            $this->setData('user', $user);
            return new JsonResponse($this->setResponse(), Response::HTTP_OK);
        } catch (QueryException $qe) {
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            $this->setMeta('message', __('messages.somethingWrong'));
            return new JsonResponse($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        } catch (Exception $e) {
            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return new JsonResponse($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @param UserUpdateRequest $request
     * @return JsonResponse
     */
    public function update(UserUpdateRequest $request): JsonResponse
    {
        try {
            DB::beginTransaction();
            $userId = Auth::id();
            $user = User::with('personInfo')->findOrFail($userId);

            if (!$user) {
                $this->setMeta('message', __('messages.userNotFound'));
                $this->setMeta('status', AppConstant::STATUS_FAIL);
                return new JsonResponse($this->setResponse(), Response::HTTP_UNPROCESSABLE_ENTITY);
            }

            if (!$user->personInfo) {
                $this->setMeta('message', __('messages.dataNotFoundWithUser'));
                $this->setMeta('status', AppConstant::STATUS_FAIL);
                return new JsonResponse($this->setResponse(), Response::HTTP_UNPROCESSABLE_ENTITY);
            }

            $user->update($request->only(
                'suffix',
                'title',
                'first_name',
                'middle_name',
                'last_name',
                'expertise',
            ));
            $user->personInfo->update($request->only(
                'address',
                'citizen',
                'dob',
                'npi',
                'contact_no',
                'experience_month',
                'experience_year',
            ));

            $this->setMeta('message', __('messages.physician.onBoarding.userUpdated'));
            $this->setMeta('status', AppConstant::STATUS_OK);
            $this->setData('user', $user->with('personInfo')->first());
            DB::commit();
            return new JsonResponse($this->setResponse(), Response::HTTP_OK);
        } catch (QueryException|Exception $e) {
            DB::rollBack();
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            $this->setMeta('message', __('messages.somethingWrong'));
            return new JsonResponse($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }

    }

    /**
     * @param $email
     * @param $token
     * @return bool
     */
    private function sendVerificationEmail($email, $token): bool
    {
        //Retrieve the user from the database
        $physician = User::whereEmail($email)->select('first_name', 'last_name', 'email')->first();

        //Generate, the email verification link. The token generated is embedded in the link
        $link = config('appconstants.APP_URL') . '/api/user/verify/email?token=' . $token . '&email=' . urlencode($physician->email);
        $physician['link'] = $link;
        $physician['name'] = $physician['first_name'] . ' ' . $physician['last_name'];
        try {
            Mail::to($email)->send(new VerifyEmail($physician));
//            SendVerificationMailJob::dispatchSync($physician)->onQueue('emailVerification');
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * @param VerifyEmailRequest $request
     * @return Application|Factory|View|JsonResponse|RedirectResponse|Redirector
     */
    public function verifyEmailLink(VerifyEmailRequest $request)
    {
        try {
            $validated = $request->validated();
            $tokenVerified = $this->verifyToken($validated);
            if (!$tokenVerified) {
                $this->setMeta('message', __('messages.token.verifyTokenExpiredOrInvalidData'));
                $this->setMeta('status', AppConstant::STATUS_FAIL);
                return redirect(config('appconstants.PHYSICIAN_URL'));
            }
            $physician = User::whereEmail($validated['email'])->first();
            $physician->is_verified = 1;
            $physician->update(); //or $physician->save();
            PhysicianVerifyEmail::whereEmail($physician->email)
                ->delete();
            $this->setMeta('message', __('messages.emailSuccessfullyVerified'));
            $this->setMeta('status', AppConstant::STATUS_OK);
            return view('email.verify.emailVerifiedSuccessfully')->with(['type' => 'PHYSICIAN']);
        } catch (QueryException|Exception $e) {
            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return new JsonResponse($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function verifyToken(array $validated): bool
    {

        // Validate the token
        $tokenData = PhysicianVerifyEmail::where(
            [
                'email' => $validated['email'],
                'token' => $validated['token']
            ]
        )->first();

        // Return false if the token is invalid
        if (!$tokenData) {
            return false;
        }

        $startTime = Carbon::parse($tokenData->updated_at);
        $finishTime = Carbon::now();

        $totalDuration = $finishTime->diffInSeconds($startTime);
        $totalDuration = abs(gmdate('i', $totalDuration));

        if ($totalDuration > 5) {
            return false;
        }
        return true;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function reSendEmailVerificationLink(Request $request): JsonResponse
    {
        try {
            $resetToken = Str::random(60);
            $email = Auth::user()->email;
            $user = User::whereEmail($email)->first();
            if ($user->is_verified) {
                $this->setMeta('message', __('messages.physician.emailAlreadyVerified'));
                $this->setMeta('status', AppConstant::STATUS_FAIL);
                return new JsonResponse($this->setResponse(), Response::HTTP_UNPROCESSABLE_ENTITY);
            }
            DB::beginTransaction();
            PhysicianVerifyEmail::updateOrCreate(
                [
                    'email' => $email
                ],
                [
                    'token' => $resetToken,
                ]
            );
            $this->sendVerificationEmail($email, $resetToken);
            $token = str_replace('Bearer ', '', $request->header('Authorization'));
            PhysicianUserAccess::where('access_token', '=', $token)->delete();
            JWTAuth::invalidate(JWTAuth::parseToken());
            auth()->logout();
            DB::commit();
            $this->setMeta('message', __('messages.physician.resendEmailVerification'));
            $this->setMeta('status', AppConstant::STATUS_OK);
            return new JsonResponse($this->setResponse(), Response::HTTP_OK);
        } catch (QueryException|Exception $e) {
            DB::rollBack();
            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return new JsonResponse($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @param ChangePasswordRequest $request
     * @return JsonResponse
     */
    public function changePassword(ChangePasswordRequest $request): JsonResponse
    {
        try {
            $validated = $request->validated();
            $physician = Auth::user();

            //match current password in db
            if (!(Hash::check($validated['current_password'], $physician->password))) {
                $this->setMeta('message', __('passwords.password.currentPassword'));
                $this->setMeta('status', AppConstant::STATUS_FAIL);
                return new JsonResponse($this->setResponse(), Response::HTTP_UNPROCESSABLE_ENTITY);
            }

            //new password match with current password
            if (strcmp($validated['current_password'], $validated['new_password']) == 0) {
                $this->setMeta('message', __('passwords.password.samePassword'));
                $this->setMeta('status', AppConstant::STATUS_FAIL);
                return new JsonResponse($this->setResponse(), Response::HTTP_UNPROCESSABLE_ENTITY);
            }

            //Change Password
            $physicianAllToken = PhysicianUserAccess::whereUserId($physician['id'])->get('access_token');
            $currentToken = str_replace('Bearer ', '', $request->header('Authorization'));
            try {
                foreach ($physicianAllToken as $t) {
                    if ($currentToken !== $t['access_token']) {
                        JWTAuth::manager()->invalidate(new Token($t['access_token']));
                        PhysicianUserAccess::whereAccessToken($t['access_token'])->delete();
                    }
                }
            } catch (Exception $e) {
                PhysicianUserAccess::whereAccessToken($t['access_token'])->delete();
            }

            $physician->password = bcrypt($validated['new_password']);
            $physician->save();

            $validated['password'] = $validated['new_password'];
            $validated['email'] = $physician['email'];
            unset($validated['current_password']);
            unset($validated['new_password']);

            $claim_credentials = [
                'hospital_id' => $physician['id'],
                'name' => $physician['name'],
                'email' => $physician['email']
            ];

            $jwt_token = JWTAuth::claims($claim_credentials)->attempt($validated);
            $access_token = $this->createTokenObj($jwt_token);

            $this->setMeta('message', __('messages.password.set'));
            $this->setMeta('status', AppConstant::STATUS_OK);
            $this->setData('token', $access_token);
            return response()->json($this->setResponse(), AppConstant::OK);
        } catch (QueryException|Exception $e) {
            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function uploadProfilePicture(UploadProfilePictureRequest $request)
    {
        try {
            $validated = $request->validated();
            $user = auth()->user();
            if (isset($validated['profile_picture'])) {
                if (Storage::exists($user->personInfo->profile_pic_path)) {
                    Storage::delete($user->personInfo->profile_pic_path);
                }
                $filePath = 'user/' . $user->id . '/profile';
                $path = $validated['profile_picture']->storePublicly($filePath, 'public');
                $user->personInfo->profile_pic_path = $path;
                $user->personInfo->save();
            }

            $this->setMeta('message', __('messages.user.picture_updated'));
            $this->setMeta('status', AppConstant::STATUS_OK);
            $this->setData('user', $user->personInfo);
            return response()->json($this->setResponse(), AppConstant::OK);
        } catch (QueryException $e) {
            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        } catch (\Exception $e) {
            $this->setMeta('message', $e->getMessage());
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

}
