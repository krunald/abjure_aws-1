<?php

namespace App\Http\Controllers\API\V1\Physician\Onboarding;

use App\Http\Controllers\Controller;
use App\Http\Requests\API\V1\Physician\OnBoarding\UserBoardCertificationCreateRequest;
use App\Http\Requests\API\V1\Physician\OnBoarding\UserBoardCertificationUpdateRequest;
use App\Http\Requests\API\V1\Physician\OnBoarding\UserCertificationAndLicensureRequest;
use App\Http\Requests\API\V1\Physician\OnBoarding\UserCertificationAndLicensurUpdateRequest;
use App\Http\Requests\API\V1\Physician\OnBoarding\UserClinicalCertificationCreateRequest;
use App\Http\Requests\API\V1\Physician\OnBoarding\UserClinicalCertificationUpdateRequest;
use App\Http\Requests\API\V1\Physician\OnBoarding\UserLicenseIssuerStateCreateRequest;
use App\Http\Requests\API\V1\Physician\OnBoarding\UserLicenseIssuerStateUpdateRequest;
use App\Models\Physician\Onboarding\User;
use App\Models\Physician\Onboarding\UserBoardCertificationDetails;
use App\Models\Physician\Onboarding\UserClinicalCertificationDetails;
use App\Models\Physician\Onboarding\UserDetailsStatus;
use App\Models\Physician\Onboarding\UserLicenseIssuerStates;
use App\Traits\ApiResponse;
use App\Utils\AppConstant;
use Exception;
use Illuminate\Database\QueryException;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Response;

class UserCertificationAndLicenseController extends Controller
{
    use ApiResponse;


    public function storeBoardCertificationDetail(UserBoardCertificationCreateRequest $request)
    {
        try {
            DB::beginTransaction();
            $request['user_id'] = Auth::id();
            if ($request->are_you_board_certified) {

                $userBoardCertificationDetails = UserBoardCertificationDetails::updateOrCreate(['user_id' => Auth::id()], $request->only(
                    'user_id',
                    'are_you_board_certified',
                    'issuer_name',
                    'speciality',
                    'date_certified',
                    'date_recertified',
                    'expiration_date'
                ));
            } else {
                $request['issuer_name'] = null;
                $request['speciality'] = null;
                $request['date_certified'] = null;
                $request['date_recertified'] = null;
                $request['expiration_date'] = null;
                $userBoardCertificationDetails = UserBoardCertificationDetails::updateOrCreate(['user_id' => Auth::id()], $request->only(
                    'user_id',
                    'are_you_board_certified',
                    'issuer_name',
                    'speciality',
                    'date_certified',
                    'date_recertified',
                    'expiration_date'
                ));
            }
            $user = User::with('clinicalCertification', 'licenseIssuerDetails')->whereId(Auth::id())->first();

            if ($user->clinicalCertification && count($user->licenseIssuerDetails) > 0) {
                UserDetailsStatus::updateOrCreate(
                    ['user_id' => $request->user_id],
                    [UserDetailsStatus::CERTIFICATION_AND_LICENSE => true]
                );
            }

            $this->setMeta('message', __('messages.physician.onBoarding.boardCertificationAdded'));
            $this->setMeta('status', Response::HTTP_OK);
            $this->setData('board_certification_details', $userBoardCertificationDetails);
            DB::commit();
            return response()->json($this->setResponse(), Response::HTTP_OK);
        } catch (QueryException|Exception $qe) {
            DB::rollBack();
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            $this->setMeta('message', __('messages.somethingWrong'));
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function updateBoardCertificationDetail(UserBoardCertificationUpdateRequest $request)
    {
        try {
            $userId = Auth::id();
            $request['user_id'] = $userId;
            if ($request->are_you_board_certified) {
                UserBoardCertificationDetails::whereUserId($request['user_id'])->update($request->only(
                    'user_id',
                    'are_you_board_certified',
                    'issuer_name',
                    'speciality',
                    'date_certified',
                    'date_recertified',
                    'expiration_date'
                ));
            } else {
                $request['issuer_name'] = null;
                $request['speciality'] = null;
                $request['date_certified'] = null;
                $request['date_recertified'] = null;
                $request['expiration_date'] = null;

                UserBoardCertificationDetails::whereUserId($userId)->first()->update($request->only(
                    'user_id',
                    'are_you_board_certified',
                    'issuer_name',
                    'speciality',
                    'date_certified',
                    'date_recertified',
                    'expiration_date'
                ));
            }
            $userBoardCertificationDetails = UserBoardCertificationDetails::whereUserId($userId)->first();
            $user = User::with('clinicalCertification', 'licenseIssuerDetails')->whereId(Auth::id())->first();
            if ($user->clinicalCertification && !empty($user->licenseIssuerDetails->toArray()) && $userBoardCertificationDetails) {
                UserDetailsStatus::updateOrCreate(
                    ['user_id' => $request->user_id],
                    [UserDetailsStatus::CERTIFICATION_AND_LICENSE => true]
                );
            }
            $this->setMeta('message', __('messages.physician.onBoarding.boardCertificationUpdated'));
            $this->setMeta('status', Response::HTTP_OK);
            $this->setData('board_certification_details', $userBoardCertificationDetails);
            DB::commit();
            return response()->json($this->setResponse(), Response::HTTP_OK);
        } catch (QueryException|Exception $qe) {
            DB::rollBack();
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            $this->setMeta('message', __('messages.somethingWrong'));
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function showBoardCertificationDetail()
    {
        try {
            $userBoardCertificationDetails = UserBoardCertificationDetails::whereUserId(Auth::id())->first();
            $this->setMeta('message', __('messages.physician.onBoarding.boardCertificationFetched'));
            $this->setMeta('status', Response::HTTP_OK);
            $this->setData('board_certification_details', $userBoardCertificationDetails);
            return response()->json($this->setResponse(), Response::HTTP_OK);
        } catch (QueryException|Exception $qe) {
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            $this->setMeta('message', __('messages.somethingWrong'));
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function storeClinicalCertificationDetail(UserClinicalCertificationCreateRequest $request)
    {
        try {
            DB::beginTransaction();
            $validatedRequest = $request->validated();
            $userId = Auth::id();
            $clinicalCertificationDetail = UserClinicalCertificationDetails::updateOrCreate(['user_id' => $userId], $validatedRequest);

            $user = User::with('boardCertification', 'licenseIssuerDetails')->whereId($userId)->first();

            if (count($user->licenseIssuerDetails) > 0 && $user->boardCertification) {
                UserDetailsStatus::updateOrCreate(
                    ['user_id' => $request->user_id],
                    [UserDetailsStatus::CERTIFICATION_AND_LICENSE => true]
                );
            }
            $this->setMeta('status', Response::HTTP_OK);
            $this->setMeta('message', __('messages.physician.onBoarding.clinicalCertificationAdded'));
            $this->setData('clinical_certification_details', $clinicalCertificationDetail);
            DB::commit();
            return response()->json($this->setResponse(), Response::HTTP_OK);
        } catch (QueryException|Exception $qe) {
            DB::rollBack();
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            $this->setMeta('message', __('messages.somethingWrong'));
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function updateClinicalCertificationDetail($userId, UserClinicalCertificationUpdateRequest $request)
    {
        try {
            DB::beginTransaction();
            $userClinicalCertificationDetails = UserClinicalCertificationDetails::whereUserId($userId)->first();
            if (!$userClinicalCertificationDetails) {
                $this->setMeta('status', AppConstant::STATUS_FAIL);
                $this->setMeta('message', __('messages.recordNotFound'));
                return response()->json($this->setResponse(), Response::HTTP_NOT_FOUND);
            }
            $userClinicalCertificationDetails->update($request->all());
            DB::commit();
            $this->setMeta('message', __('messages.physician.onBoarding.clinicalCertificationUpdated'));
            $this->setMeta('status', AppConstant::STATUS_OK);
            $this->setData('license_issuer_details', $userClinicalCertificationDetails);
            return response()->json($this->setResponse(), AppConstant::OK);
        } catch (QueryException|Exception $qe) {
            DB::rollBack();
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            $this->setMeta('message', __('messages.somethingWrong'));
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function showClinicalCertificationDetail()
    {
        try {
            $userClinicalCertificationDetails = UserClinicalCertificationDetails::whereUserId(Auth::id())->first();
//            if (!$userClinicalCertificationDetails) {
//                $this->setMeta('status', AppConstant::STATUS_FAIL);
//                $this->setMeta('message', __('messages.recordNotFound'));
//                return response()->json($this->setResponse(), Response::HTTP_NOT_FOUND);
//            }
            DB::commit();
            $this->setMeta('message', __('messages.physician.onBoarding.clinicalCertificationFetched'));
            $this->setMeta('status', AppConstant::STATUS_OK);
            $this->setData('license_issuer_details', $userClinicalCertificationDetails);
            return response()->json($this->setResponse(), AppConstant::OK);
        } catch (QueryException|Exception $qe) {
            DB::rollBack();
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            $this->setMeta('message', __('messages.somethingWrong'));
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function storeLicenseIssuerDetail(UserLicenseIssuerStateCreateRequest $request)
    {
        try {
            $userId = Auth::id();
            collect($request->licensure)->each(function ($requestItem) use ($userId) {
                $requestItem['user_id'] = $userId;
                UserLicenseIssuerStates::create($requestItem);
            });

            $user = User::with('boardCertification', 'clinicalCertification')->whereId($userId)->first();

            if ($user->boardCertification && $user->clinicalCertification) {
                UserDetailsStatus::updateOrCreate(
                    ['user_id' => $userId],
                    [UserDetailsStatus::CERTIFICATION_AND_LICENSE => true]
                );
            }
            $licenseDetails = User::with('licensure')->whereId($userId)->first()->licensure;
            $this->setMeta('message', __('messages.physician.onBoarding.licenseIssuerStateAdded'));
            $this->setMeta('status', AppConstant::STATUS_OK);
            $this->setData('license_issuer_details', $licenseDetails);
            return response()->json($this->setResponse(), AppConstant::OK);
        } catch (QueryException|Exception $e) {
            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function updateLicenseIssuerDetail($id, UserLicenseIssuerStateUpdateRequest $request)
    {
        try {
            $userId = Auth::id();
            UserLicenseIssuerStates::whereUserId($userId)->delete();
            collect($request->licensure)->map(function ($req) use ($userId) {
                $req['user_id'] = $userId;
                UserLicenseIssuerStates::create($req);
            });
            $userLicensureDetails = User::with('licensure')->whereId($userId)->first()->licensure;
            $this->setMeta('message', __('messages.physician.onBoarding.licenseIssuerStateUpdated'));
            $this->setMeta('status', AppConstant::STATUS_OK);
            $this->setData('license_issuer_details', $userLicensureDetails);
            return response()->json($this->setResponse(), AppConstant::OK);
        } catch (QueryException|Exception $e) {
            DB::rollBack();
            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function showLicenseIssuerDetail()
    {
        try {
            $licensureDetails = UserLicenseIssuerStates::whereUserId(Auth::id())->get();
//            if (count($licensureDetails) <= 0) {
//                $this->setMeta('status', AppConstant::STATUS_FAIL);
//                $this->setMeta('message', __('messages.recordNotFound'));
//                return response()->json($this->setResponse(), Response::HTTP_NOT_FOUND);
//            }
            $this->setMeta('message', __('messages.physician.onBoarding.licenseIssuerStateFetched'));
            $this->setMeta('status', AppConstant::STATUS_OK);
            $this->setData('license_issuer_details', $licensureDetails);
            return response()->json($this->setResponse(), AppConstant::OK);
        } catch (QueryException $qe) {
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            $this->setMeta('message', __('messages.somethingWrong'));
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        } catch (Exception $e) {
            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);

        }
    }
}
