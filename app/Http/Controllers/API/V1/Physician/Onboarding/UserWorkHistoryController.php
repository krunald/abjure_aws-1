<?php

namespace App\Http\Controllers\API\V1\Physician\Onboarding;

use App\Http\Controllers\Controller;
use App\Http\Requests\API\V1\Physician\OnBoarding\UserWorkHistoryCreateRequest;
use App\Http\Requests\API\V1\Physician\OnBoarding\UserWorkHistoryUpdateRequest;
use App\Models\Physician\Onboarding\User;
use App\Models\Physician\Onboarding\UserDetailsStatus;
use App\Models\Physician\Onboarding\UserWorkExperience;
use App\Traits\ApiResponse;
use App\Utils\AppConstant;
use Exception;
use Illuminate\Database\QueryException;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Response;

class UserWorkHistoryController extends Controller
{

    use ApiResponse;

    public function store(UserWorkHistoryCreateRequest $request): JsonResponse
    {
        try {
            DB::beginTransaction();
            $userId = Auth::id();
            collect($request->work_history)->each(function ($reqWorkHistory) use ($userId) {
                $reqWorkHistory['user_id'] = $userId;
                UserWorkExperience::create($reqWorkHistory);
            });
            $userWorkHistory = User::whereId($userId)->with('workHistory')->first()->workHistory;
            if (!empty($userWorkHistory)) {
                UserDetailsStatus::updateOrCreate(['user_id' => $request->user_id], [UserDetailsStatus::WORK_HISTORY => true]);
            }
            $this->setMeta('status', AppConstant::STATUS_OK);
            $this->setMeta('message', __('messages.physician.onBoarding.workHistoryDetailsAdded'));
            $this->setData('work_history_details', $userWorkHistory);
            DB::commit();
            return response()->json($this->setResponse(), Response::HTTP_OK);
        } catch (QueryException|Exception $e) {
            DB::rollBack();
            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function update(UserWorkHistoryUpdateRequest $request): JsonResponse
    {
        try {
            $userId = Auth::id();
            UserWorkExperience::whereUserId($userId)->delete();
            collect($request->work_history)->each(function ($req) use ($userId) {
                $req['user_id'] = $userId;
                UserWorkExperience::create($req);
            });

            $userWorkHistory = User::whereId($userId)->with(['workHistory'])->first()->workHistory;
            if (!empty($userWorkHistory)) {
                UserDetailsStatus::updateOrCreate(['user_id' => $request->user_id], [UserDetailsStatus::WORK_HISTORY => true]);
            }
            $this->setMeta('status', AppConstant::STATUS_OK);
            $this->setMeta('message', __('messages.physician.onBoarding.workHistoryDetailsUpdated'));
            $this->setData('work_history_details', $userWorkHistory);
            return response()->json($this->setResponse(), Response::HTTP_OK);
        } catch (QueryException|Exception $e) {
            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function show(): JsonResponse
    {
        try {
            $userWorkHistory = User::whereId(Auth::id())->first()->workHistory;

            $this->setMeta('status', AppConstant::STATUS_OK);
            $this->setMeta('message', __('messages.physician.onBoarding.workHistoryDetailsFetched'));
            $this->setData('work_history_details', $userWorkHistory);
            return response()->json($this->setResponse(), Response::HTTP_OK);
        } catch (QueryException|Exception $e) {
            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function destroy(int $id): JsonResponse
    {
        try {
            $userWorkExperience = UserWorkExperience::whereId($id)->first();
            if (!$userWorkExperience) {
                return response()->json($this->setQueryExceptionResponse(__('messages.recordNotFound')), Response::HTTP_NOT_FOUND);
            }
            $userWorkExperience->delete();
            $this->setMeta('status', AppConstant::STATUS_OK);
            $this->setMeta('message', __('messages.physician.onBoarding.workHistoryDetailsDeleted'));
            return response()->json($this->setResponse(), Response::HTTP_OK);
        } catch (QueryException|Exception $e) {
            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
