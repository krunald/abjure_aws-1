<?php

namespace App\Http\Controllers\API\V1\Physician\Onboarding;

use App\Http\Controllers\Controller;
use App\Http\Requests\API\V1\Physician\OnBoarding\UserGeneralSuergeryCreateRequest;
use App\Http\Requests\API\V1\Physician\OnBoarding\UserGeneralSuergeryUpdateRequest;
use App\Models\Physician\Onboarding\UserDetailsStatus;
use App\Models\Physician\Onboarding\UserGeneralSurgeryDetails;
use App\Traits\ApiResponse;
use App\Utils\AppConstant;
use Exception;
use Illuminate\Database\QueryException;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Response;

class UserGeneralSurgeryController extends Controller
{
    use ApiResponse;

    public function store(UserGeneralSuergeryCreateRequest $request): JsonResponse
    {
        try {
            DB::beginTransaction();
            $userId = Auth::id();
            $request['user_id'] = $userId;
            $requestData = $request->only(
                'user_id',
                'cholecstectomies',
                'robotic_surgery',
                'cesarean_section',
                'egd_and_colonoscopy',
                'vascular_access',
                'vascular_access_fistula_and_grafts',
                'trauma_center_level',
            );
            $generalSurgeryDetails = UserGeneralSurgeryDetails::updateOrCreate(['user_id' => $userId], $requestData);
            if ($generalSurgeryDetails) {
                UserDetailsStatus::updateOrCreate(['user_id' => $userId], [UserDetailsStatus::PROFILE_EXPERTISE_DETAILS => true]);
            }

            $this->setMeta('status', AppConstant::STATUS_OK);
            $this->setMeta('message', __('messages.physician.onBoarding.generalSurgeryDetailsAdded'));
            $this->setData('general_surgery', $generalSurgeryDetails);
            DB::commit();
            return response()->json($this->setResponse(), Response::HTTP_OK);
        } catch (QueryException|Exception $e) {
            DB::rollBack();
            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function update(UserGeneralSuergeryUpdateRequest $request): JsonResponse
    {
        try {
            DB::beginTransaction();
            $userId = Auth::id();
            $request['user_id'] = $userId;
            $generalSurgeryDetails = UserGeneralSurgeryDetails::whereUserId($userId)->first();
            if (!$generalSurgeryDetails) {
                $this->setMeta('status', AppConstant::STATUS_FAIL);
                $this->setMeta('message', __('messages.recordNotFound'));
                return response()->json($this->setResponse(), Response::HTTP_NOT_FOUND);
            }
            $requestData = $request->only(
                'user_id',
                'cholecstectomies',
                'robotic_surgery',
                'cesarean_section',
                'egd_and_colonoscopy',
                'vascular_access_fistula_and_grafts',
                'vascular_access',
                'trauma_center_level'
            );

            $generalSurgeryDetails->update($requestData);
            $this->setMeta('status', AppConstant::STATUS_OK);
            $this->setMeta('message', __('messages.physician.onBoarding.generalSurgeryDetailsUpdated'));
            $this->setData('general_surgery', $generalSurgeryDetails);
            DB::commit();
            return response()->json($this->setResponse(), Response::HTTP_OK);
        } catch (QueryException|Exception $e) {
            DB::rollBack();
            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function show(): JsonResponse
    {
        try {
            $generalSurgeryDetails = UserGeneralSurgeryDetails::whereUserId(Auth::id())->first();
            if (!$generalSurgeryDetails) {
                $this->setMeta('status', AppConstant::STATUS_FAIL);
                $this->setMeta('message', __('messages.recordNotFound'));
                return response()->json($this->setResponse(), Response::HTTP_NOT_FOUND);
            }
            $this->setMeta('status', JsonResponse::HTTP_OK);
            $this->setData('general_surgery_profile_details', $generalSurgeryDetails);
            return response()->json($this->setResponse(), JsonResponse::HTTP_OK);
        } catch (QueryException|Exception $e) {
            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
