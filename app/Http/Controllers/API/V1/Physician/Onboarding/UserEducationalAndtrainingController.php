<?php

namespace App\Http\Controllers\API\V1\Physician\Onboarding;

use App\Http\Controllers\Controller;
use App\Http\Requests\API\V1\Physician\OnBoarding\UserEducationalAndTrainingRequest;
use App\Http\Requests\API\V1\Physician\OnBoarding\UserEducationalAndTrainingUpdateRequest;
use App\Http\Requests\API\V1\Physician\OnBoarding\UserTrainingCreateRequest;
use App\Http\Requests\API\V1\Physician\OnBoarding\UserTrainingUpdateRequest;
use App\Models\Physician\Onboarding\User;
use App\Models\Physician\Onboarding\UserDetailsStatus;
use App\Models\Physician\Onboarding\UserEducationalAndTrainingDetails;
use App\Models\Physician\Onboarding\UserEducationRelatedSubDetail;
use App\Traits\ApiResponse;
use App\Utils\AppConstant;
use Exception;
use Illuminate\Database\QueryException;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Response;

class UserEducationalAndtrainingController extends Controller
{
    use ApiResponse;

    public function store(UserEducationalAndTrainingRequest $request): JsonResponse
    {
        try {
            $requestCompleteData = [];
            $userId = Auth::id();
            $subDetails = $request->only(
                'user_id',
                'attended_fifth_way_program',
                'foreign_medical_graduate',
                'ecfmg_certificate',
                'ecfmg_certificate_number'
            );

            collect($request->university_details)->each(function ($universityKey) use (&$requestCompleteData, $userId) {
                $universityKey['user_id'] = $userId;
                $requestCompleteData[] = $universityKey;
            });

            collect($request->medical_school)->each(function ($medicalSchoolKey) use ($userId, &$requestCompleteData) {
                $medicalSchoolKey['user_id'] = $userId;
                $requestCompleteData[] = $medicalSchoolKey;
            });

            collect($request->fifth_way_program)->each(function ($fifthWayProgramKey) use ($userId, &$requestCompleteData) {
                $fifthWayProgramKey['user_id'] = $userId;
                $requestCompleteData[] = $fifthWayProgramKey;
            });

            DB::beginTransaction();
            collect($requestCompleteData)->map(function ($reqData) {
                UserEducationalAndTrainingDetails::create($reqData);
            });

            UserEducationRelatedSubDetail::updateOrCreate(['user_id' => $userId], $subDetails);
            $userData = User::with(['educationalAndTraingDetails', 'educationaRelatedSubDetails'])->whereId($userId)->first();
            $training = User::whereId($userId)->with(['educationalAndTraingDetails' => function ($q) {
                return $q->where('type', 'TRAINING');
            }])->first();
            $educationDetails = $userData->educationalAndTraingDetails->groupBy('type')->forget('TRAINING');

            if ($userData->educationalAndTraingDetails && $userData->educationaRelatedSubDetails && $training->educationalAndTraingDetails && count($training->educationalAndTraingDetails) > 0) {
                UserDetailsStatus::updateOrCreate(['user_id' => $userId], [UserDetailsStatus::EDUCATION_AND_TRAINING => true]);
            }
            $this->setMeta('status', AppConstant::STATUS_OK);
            $this->setMeta('message', __('messages.physician.onBoarding.educationAndTrainingDetailsAdded'));

            $this->setData('education_andtraining_details', $educationDetails);
            $this->setData('education_and_training_sub_details', $userData->educationaRelatedSubDetails);
            DB::commit();
            return response()->json($this->setResponse(), Response::HTTP_OK);
        } catch (QueryException|Exception $e) {
            DB::rollBack();
            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function update(UserEducationalAndTrainingUpdateRequest $request): JsonResponse
    {
        try {
            $subDetails = $request->only(
                'attended_fifth_way_program',
                'foreign_medical_graduate',
                'ecfmg_certificate',
                'ecfmg_certificate_number'
            );
            DB::beginTransaction();
            $userId = Auth::id();
            UserEducationalAndTrainingDetails::whereUserId($userId)->where('type', '!=', 'TRAINING')->delete();

            collect($request->university_details)->each(function ($universityKey) use ($userId) {
                $universityKey['user_id'] = $userId;
                UserEducationalAndTrainingDetails::create($universityKey);
            });

            collect($request->medical_school)->each(function ($medicalSchoolKey) use ($userId) {
                $medicalSchoolKey['user_id'] = $userId;
                UserEducationalAndTrainingDetails::create($medicalSchoolKey);
            });

            collect($request->fifth_way_program)->each(function ($fifthWayProgramKey) use ($userId) {
                $fifthWayProgramKey['user_id'] = $userId;
                UserEducationalAndTrainingDetails::create($fifthWayProgramKey);
            });


            //updating sub details
            $subDetails['user_id'] = $userId;
            UserEducationRelatedSubDetail::updateOrCreate(['user_id' => $userId], $subDetails);

            $user = User::with(['educationalAndTraingDetails', 'educationaRelatedSubDetails'])->whereId($userId)->first();
            $responseData = $user->educationalAndTraingDetails->groupBy('type');
            $responseData->forget('TRAINING');
            $this->setMeta('status', Response::HTTP_OK);
            $this->setMeta('message', __('messages.physician.onBoarding.educationAndTrainingDetailsUpdated'));
            $this->setData('education_and_training', $responseData);
            $this->setData('education_and_training_sub_details', $user->educationaRelatedSubDetails);
            DB::commit();
            return response()->json($this->setResponse(), Response::HTTP_OK);
        } catch (QueryException|Exception $e) {
            DB::rollBack();
            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function showEducationDetails(): JsonResponse
    {
        try {
            $user = User::whereId(Auth::id())->with(['educationalAndTraingDetails', 'educationaRelatedSubDetails'])->first();
            $educationDetails = $user->educationalAndTraingDetails->groupBy('type')->forget('TRAINING');
            $this->setMeta('status', Response::HTTP_OK);
            $this->setMeta('message', __('messages.physician.onBoarding.educationAndTrainingDetailsFetched'));
            $this->setData('education_and_training', $educationDetails);
            $this->setData('education_and_training_sub_details', $user->educationaRelatedSubDetails);
            return response()->json($this->setResponse(), Response::HTTP_OK);
        } catch (QueryException|Exception $e) {
            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function storeTrainingDetails(UserTrainingCreateRequest $request): JsonResponse
    {
        try {
            DB::beginTransaction();
            $userid = Auth::id();
            collect($request->training)->each(function ($trainingKey) use ($userid) {
                $trainingKey['user_id'] = $userid;
                UserEducationalAndTrainingDetails::create($trainingKey);
            });

            $user = User::whereId($userid)->with(['educationalAndTraingDetails' => function ($q) {
                return $q->where('type', 'TRAINING');
            }])->first();

            $userData = User::with(['educationalAndTraingDetails', 'educationaRelatedSubDetails'])->whereId(Auth::id())->first();

            if ($userData->educationalAndTraingDetails && $userData->educationaRelatedSubDetails && $user->educationalAndTraingDetails && count($user->educationalAndTraingDetails) > 0) {
                UserDetailsStatus::updateOrCreate(['user_id' => $request->user_id], [UserDetailsStatus::EDUCATION_AND_TRAINING => true]);
            }
            $this->setMeta('status', Response::HTTP_OK);
            $this->setMeta('message', __('messages.physician.onBoarding.trainingDetailsAdded'));
            $this->setData('education_and_training', $user->educationalAndTraingDetails);
            DB::commit();
            return response()->json($this->setResponse(), Response::HTTP_OK);

        } catch (QueryException|Exception $e) {
            DB::rollBack();
            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function updateTrainingDetails(UserTrainingUpdateRequest $request)
    {
        try {
            DB::beginTransaction();
            $userId = Auth::id();
            UserEducationalAndTrainingDetails::where(['user_id' => $userId, 'type' => 'TRAINING'])->delete();

            collect($request->training)->each(function ($trainingKey) use ($userId) {
                $trainingKey['user_id'] = $userId;
                UserEducationalAndTrainingDetails::create($trainingKey);
            });
            $user = User::with(['educationalAndTraingDetails' => function ($q) {
                return $q->where('type', 'TRAINING');
            }])->whereId($userId)->first();
            $this->setMeta('status', Response::HTTP_OK);
            $this->setMeta('message', __('messages.physician.onBoarding.trainingDetailsUpdated'));
            $this->setData('training_details', $user->educationalAndTraingDetails);
            DB::commit();
            return response()->json($this->setResponse(), Response::HTTP_OK);
        } catch (QueryException|Exception $e) {
            DB::rollBack();
            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function showTrainingDetails(): JsonResponse
    {
        try {
            $user = User::with(['educationalAndTraingDetails' => function ($q) {
                return $q->where('type', 'TRAINING');
            }])->whereId(Auth::id())->first();
            $this->setMeta('status', Response::HTTP_OK);
            $this->setMeta('message', __('messages.physician.onBoarding.trainingDetailsFetched'));
            $this->setData('training_details', $user->educationalAndTraingDetails);
            DB::commit();
            return response()->json($this->setResponse(), Response::HTTP_OK);
        } catch (QueryException|Exception $e) {
            DB::rollBack();
            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }

    }

    public function upsertUserEducationData(array $dataObject): void
    {
        UserEducationalAndTrainingDetails::updateOrCreate(['id' => $dataObject['id'], 'user_id' => $dataObject['user_id']], $dataObject);
    }
}
