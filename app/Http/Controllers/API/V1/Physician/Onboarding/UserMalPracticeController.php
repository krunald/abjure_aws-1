<?php

namespace App\Http\Controllers\API\V1\Physician\Onboarding;

use App\Http\Controllers\Controller;
use App\Http\Requests\API\V1\Physician\OnBoarding\UserMalPracticeCreateRequest;
use App\Http\Requests\API\V1\Physician\OnBoarding\UserMalPracticeUpdateRequest;
use App\Models\Physician\Onboarding\User;
use App\Models\Physician\Onboarding\UserDetailsStatus;
use App\Models\Physician\Onboarding\UserMalpracticeHistory;
use App\Traits\ApiResponse;
use App\Utils\AppConstant;
use Exception;
use Illuminate\Database\QueryException;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Response;

class UserMalPracticeController extends Controller
{

    use ApiResponse;

    public function store(UserMalPracticeCreateRequest $request): JsonResponse
    {
        try {
            DB::beginTransaction();

            $userId = Auth::id();
            $requestData = [];
            collect($request->mal_practice)->each(function ($requestItem) use ($userId, &$requestData) {
                $requestItem['user_id'] = $userId;
                $requestData[] = UserMalpracticeHistory::create($requestItem);
            });
            $userMalPracticeData = User::with('malPractice')->whereId($userId)->first()->malPractice;
            if (!empty($requestData)) {
                UserDetailsStatus::updateOrCreate(['user_id' => $userId], [UserDetailsStatus::MALPRACTICE_HISTORY_DETAILS => true]);
            }
            $this->setMeta('status', AppConstant::STATUS_OK);
            $this->setMeta('message', __('messages.physician.onBoarding.malPracticeDetailsAdded'));

            $this->setData('mal_practice_details', $userMalPracticeData);
            DB::commit();
            return response()->json($this->setResponse(), Response::HTTP_OK);
        } catch (QueryException|Exception $e) {
            DB::rollBack();
            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function update(UserMalPracticeUpdateRequest $request): JsonResponse
    {
        try {
            DB::beginTransaction();
            $userId = Auth::id();
            UserMalpracticeHistory::whereUserId($userId)->delete();
            collect($request->mal_practice)->map(function ($reqData) use ($userId) {
                $reqData['user_id'] = $userId;
                UserMalpracticeHistory::create($reqData);
            });

            $userMalPracticeData = User::with('malPractice')->whereId($userId)->first()->malPractice;
            if (!empty($userMalPracticeData->toArray())) {
                UserDetailsStatus::updateOrCreate(['user_id' => $userId], [UserDetailsStatus::MALPRACTICE_HISTORY_DETAILS => true]);
            }
            $this->setMeta('status', AppConstant::STATUS_OK);
            $this->setMeta('message', __('messages.physician.onBoarding.malPracticeDetailsUpdated'));
            $this->setData('mal_practice_details', $userMalPracticeData);
            DB::commit();
            return response()->json($this->setResponse(), Response::HTTP_OK);
        } catch (QueryException|Exception $e) {
            DB::rollBack();
            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function destroy(int $id): JsonResponse
    {
        try {
            DB::beginTransaction();
            $userMalPractice = UserMalpracticeHistory::whereId($id)->first();
            if (!$userMalPractice) {
                $this->setMeta('status', AppConstant::STATUS_FAIL);
                $this->setMeta('message', __('messages.recordNotFound'));
                return response()->json($this->setResponse(), Response::HTTP_NOT_FOUND);
            }
            $userMalPractice->delete();
            DB::commit();
            $this->setMeta('status', AppConstant::STATUS_OK);
            $this->setMeta('message', __('messages.physician.onBoarding.malPracticeDetailsDeleted'));
            return response()->json($this->setResponse(), Response::HTTP_OK);
        } catch (QueryException|Exception $e) {
            DB::rollBack();
            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function show(): JsonResponse
    {
        try {
            $userMalPractice = UserMalpracticeHistory::whereUserId(Auth::id())->get();

            $this->setMeta('status', AppConstant::STATUS_OK);
            $this->setMeta('message', __('messages.physician.onBoarding.malPracticeDetailsFetched'));
            $this->setData('mal_practice_details', $userMalPractice);
            return response()->json($this->setResponse(), Response::HTTP_OK);
        } catch (QueryException|Exception $qe) {
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            $this->setMeta('message', __('messages.somethingWrong'));
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
