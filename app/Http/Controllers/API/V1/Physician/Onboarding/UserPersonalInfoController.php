<?php

namespace App\Http\Controllers\API\V1\Physician\Onboarding;

use App\Http\Controllers\Controller;
use App\Http\Requests\API\V1\Physician\OnBoarding\UserPersonalInfoRequest;
use App\Http\Requests\API\V1\Physician\OnBoarding\UserPersonalInfoUpdateRequest;
use App\Models\Physician\Onboarding\User;
use App\Models\Physician\Onboarding\UserDetailsStatus;
use App\Models\Physician\Onboarding\UserPersonalInfos;
use App\Traits\ApiResponse;
use App\Utils\AppConstant;
use Exception;
use Illuminate\Database\QueryException;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Response;

class UserPersonalInfoController extends Controller
{
    use ApiResponse;

    public function store(UserPersonalInfoRequest $request): JsonResponse
    {
        try {
            DB::beginTransaction();
            $userId = Auth::id();
            $request['user_id'] = $userId;
            $userPersonalInfo = UserPersonalInfos::updateOrCreate(['user_id' => $userId], $request->validated());
            if ($userPersonalInfo) {
                UserDetailsStatus::updateOrCreate(['user_id' => $userId], [UserDetailsStatus::PERSONAL_DETAILS => true]);
            }
            $this->setMeta('message', __('messages.physician.onBoarding.personInfoAdded'));
            $this->setMeta('status', AppConstant::STATUS_OK);
            $this->setData('personal_info', $userPersonalInfo);
            DB::commit();
            return response()->json($this->setResponse(), Response::HTTP_OK);
        } catch (QueryException|Exception $e) {
            DB::rollBack();
            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function update(UserPersonalInfoUpdateRequest $request): JsonResponse
    {
        try {
            DB::beginTransaction();
            $userId = Auth::id();
            $request['user_id'] = $userId;
            $user = User::findOrFail($userId);

            if (!$user) {
                $this->setMeta('message', __('messages.userNotFound'));
                $this->setMeta('status', AppConstant::STATUS_FAIL);
                return response()->json($this->setResponse(), Response::HTTP_NOT_FOUND);
            }

            $userPersonalInfo = UserPersonalInfos::whereUserId($userId)->first();

            if (!$userPersonalInfo) {
                $this->setMeta('message', __('messages.hospital.job.jobNotFound'));
                $this->setMeta('status', AppConstant::STATUS_FAIL);
                return response()->json($this->setResponse(), AppConstant::NOT_FOUND);
            }
            if ($userPersonalInfo) {
                UserDetailsStatus::updateOrCreate(['user_id' => $userId], [UserDetailsStatus::PERSONAL_DETAILS => true]);
            }
            $userPersonalInfo->update($request->all());
            $this->setMeta('message', __('messages.physician.onBoarding.personInfoUpdated'));
            $this->setMeta('status', AppConstant::STATUS_OK);
            $this->setData('personal_info', $userPersonalInfo);
            DB::commit();
            return response()->json($this->setResponse(), Response::HTTP_OK);
        } catch (QueryException|Exception $e) {
            DB::rollBack();
            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function show()
    {
        try {
            $userPersonalInfo = UserPersonalInfos::whereUserId(Auth::id())->first();

            $this->setMeta('message', __('messages.physician.onBoarding.personInfoFetched'));
            $this->setMeta('status', AppConstant::STATUS_OK);
            $this->setData('personal_info', $userPersonalInfo);
            return response()->json($this->setResponse(), Response::HTTP_OK);
        } catch (QueryException|Exception $e) {
            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
