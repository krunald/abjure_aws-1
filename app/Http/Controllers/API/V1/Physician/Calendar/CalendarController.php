<?php

namespace App\Http\Controllers\API\V1\Physician\Calendar;

use App\Helper\Helper;
use App\Http\Controllers\Controller;
use App\Http\Requests\API\V1\Physician\Calendar\SetPhysicianAvailabilityRequest;
use App\Http\Requests\API\V1\Physician\Calendar\UpdatePhysicianAvailabilityRequest;
use App\Models\Hospital\Jobs\UserJobsScheduleInterview;
use App\Models\Physician\Calendar\PhysicianAvailability;
use App\Models\Physician\Onboarding\User;
use App\Traits\ApiResponse;
use App\Utils\AppConstant;
use Exception;
use Illuminate\Database\QueryException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

class CalendarController extends Controller
{
    use ApiResponse;

    public function physicianCalendarList(): JsonResponse
    {
        try {
            $userJobScheduleInterview = UserJobsScheduleInterview::with(['userJob' => function ($q) {
                $q->where('user_id', Auth::id());
            }])->get();
            $resultArr = [];
            $userJobScheduleInterview->each(function ($item, $index) use (&$resultArr) {
                if (isset($item->userJob)) {
                    $resultArr[$index]['id'] = $item->id;
                    $resultArr[$index]['title'] = $item->userJob->jobs->name;
                    $resultArr[$index]['date'] = $item->interview_date;
                    $resultArr[$index]['time'] = $item->interview_time;
                    $resultArr[$index]['interview_type'] = $item->interview_type;
                    if ($item->interview_type === 'OTHER') {
                        $resultArr[$index]['other_interview_type'] = $item->other_interview_type;
                        if ($item->other_interview_type === 'VIDEO_CALL') {
                            $resultArr[$index]['interview_link'] = $item->video_call_link;
                        }
                    }
                }
            });
            PhysicianAvailability::whereUserId(Auth::id())->orderBy('id', 'desc')
                ->each(function ($item, $index) use (&$resultArr) {
                    $dataObject = [];
                    $dataObject['id'] = $item->id;
                    $dataObject['title'] = "Available";
                    $dataObject['shift'] = $item->shift;
                    $dataObject['date'] = $item->availability_date;
                    array_push($resultArr, $dataObject);
                });

            $this->setMeta('status', AppConstant::STATUS_OK);
            $this->setMeta('message', __('messages.physician.calendarDetailsFetched'));
            $this->setData('physicians_jobs_scheduled_interviews', $resultArr);
            return response()->json($this->setResponse(), AppConstant::OK);
        } catch (QueryException|Exception $e) {
            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function setPhysicianAvailability(SetPhysicianAvailabilityRequest $request): JsonResponse
    {
        try {
            $validatedRequest = $request->validated();
            $utcDate = Helper::setUTCDate($validatedRequest['availability_date']);
            $validatedRequest['user_id'] = Auth::id();
            $validatedRequest['availability_date'] = $utcDate->toDateString();
            $containsAvailability = PhysicianAvailability::whereUserId(Auth::id())->get()
                ->contains('availability_date', $validatedRequest['availability_date']);
            $containsAvailabilityShift = PhysicianAvailability::where([
                ['user_id', '=', Auth::id()],
                ['availability_date', '=', $validatedRequest['availability_date']]
            ])->get()
                ->contains('shift', $validatedRequest['shift']);
            if ($containsAvailability && $containsAvailabilityShift) {
                $this->setMeta('message', __('messages.physician.availability.conflict'));
                $this->setMeta('status', AppConstant::STATUS_FAIL);
                return response()->json($this->setResponse(), Response::HTTP_CONFLICT);
            }
            $availability = PhysicianAvailability::create($validatedRequest);
            $this->setMeta('status', AppConstant::STATUS_OK);
            $this->setMeta('message', __('messages.physician.availability.created'));
            $this->setData('physicians_jobs_scheduled_interviews', $availability);
            return response()->json($this->setResponse(), AppConstant::OK);
        } catch (QueryException|Exception $e) {
            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function updatePhysicianAvailability(int $availabilityId, UpdatePhysicianAvailabilityRequest $request): JsonResponse
    {
        try {
            $availability = PhysicianAvailability::whereId($availabilityId)->first();
            if (!$availability) {
                $this->setMeta('status', AppConstant::STATUS_FAIL);
                $this->setMeta('message', __('messages.recordNotFound'));
                return response()->json($this->setResponse(), Response::HTTP_NOT_FOUND);
            }
            $validatedRequest = $request->validated();
            $utcDate = Helper::setUTCDate($validatedRequest['availability_date']);
            $validatedRequest['availability_date'] = $utcDate->toDateString();
            $availability->update($validatedRequest);
            $this->setMeta('status', AppConstant::STATUS_OK);
            $this->setMeta('message', __('messages.physician.availability.fetched'));
            $this->setData('physicians_jobs_scheduled_interviews', $availability);
            return response()->json($this->setResponse(), AppConstant::OK);
        } catch (QueryException|Exception $e) {
//            $this->setMeta('message', $e->getMessage());
            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function getPhysicianAvailability()
    {
        try {
            $availability = PhysicianAvailability::whereUserId(Auth::id())->orderBy('id', 'desc')->get();
            $this->setMeta('status', AppConstant::STATUS_OK);
            $this->setMeta('message', __('messages.physician.availability.fetched'));
            $this->setData('physicians_jobs_scheduled_interviews', $availability);
            return response()->json($this->setResponse(), AppConstant::OK);
        } catch (QueryException|Exception $e) {
            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
