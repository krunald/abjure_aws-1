<?php

namespace App\Http\Controllers\API\V1\Physician;

use App\Http\Controllers\Controller;
use App\Models\Speciality;
use App\Traits\ApiResponse;
use Symfony\Component\HttpFoundation\Response;

class SpecialitiesController extends Controller
{
    use ApiResponse;

    public function all(){
        $specialities=Speciality::all();
        $this->setMeta('status', Response::HTTP_OK);
        $this->setData('specialities', $specialities);
        return response()->json($this->setResponse(), Response::HTTP_OK);

    }
}
