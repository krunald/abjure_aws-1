<?php

namespace App\Http\Controllers\API\V1\Payment;

use App\Http\Controllers\Controller;
use App\Http\Requests\API\V1\Hospital\Subscription\CreateSubscriptionRequest;
use App\Models\Hospital\Onboarding\Hospital;
use App\Models\Hospital\Subscription\Subscription;
use App\Models\StripePlans;
use App\Traits\ApiResponse;
use App\Utils\AppConstant;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Laravel\Cashier\Billable;
use Laravel\Cashier\Cashier;
use Symfony\Component\HttpFoundation\Response;

class StripeSubscriptionController extends Controller
{
    use ApiResponse;
    use Billable;

    /**
     * @return JsonResponse
     */
    public function getPlans(): JsonResponse
    {
        try {
            $plans = StripePlans::all()->groupBy('name');

            $plans->transform(function ($item, $key) {
                $item = $item->first();
                $item[$item->interval] = [
                    'price' => $item->unit_amount,
                    'decimal_price' => $item->unit_amount_decimal
                ];
                unset($item['interval'], $item['unit_amount'], $item['unit_amount_decimal']);
                return $item;
            });

            $this->setMeta('message', __('messages.hospital.subscription.fetchStripePlan'));
            $this->setMeta('status', AppConstant::STATUS_OK);
            $this->setData('products', $plans);
            return response()->json($this->setResponse(), AppConstant::OK);
        } catch (Exception $e) {
            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @return JsonResponse
     */
    public function getCustomerPlans(): JsonResponse
    {
        try {
            $hospital = Auth::user();
            $subscriptionItems = Hospital::with(['subscriptionItems'])->find($hospital->id);

            if (!is_null($hospital->stripe_id)) {
                $plans = StripePlans::where('product_id', $subscriptionItems->subscriptionItems->stripe_product)->first();

                /*$productId = $subscriptionItems->subscriptionItems->stripe_product;
                $stripeId = $subscriptionItems->subscriptionDetails->first()->subscriptionItemDetails->stripe_id;
                $stripe = Cashier::stripe();
                $subscriptionItems = $stripe->subscriptionItems->retrieve(
                    $stripeId,
                    []
                );

                $productName = optional($stripe->products->retrieve($productId))->name;

                $items = [];
                $items['id'] = $subscriptionItems->id;

                $items['quantity'] = $subscriptionItems->quantity;
                $items['interval'] = $subscriptionItems->plan->interval;
                $items['interval_count'] = $subscriptionItems->plan->interval_count;
                $items['currency'] = $subscriptionItems->plan->currency;
                $items['name'] = $productName;
                $items['price'] = $subscriptionItems->price->unit_amount / 100;
                $items['decimal_price'] = number_format((float)($subscriptionItems->price->unit_amount / 100), 2, '.', '');
                $items['purchased_start_date'] = Carbon::createFromTimestamp($subscriptionItems->created)->toDateString();
                $items['purchased_end_date'] = Carbon::createFromTimestamp($subscriptionItems->created)->addMonth()->toDateString();
                $items['metakey'] = $subscriptionItems->metadata;*/

                //fetch invoices from stripe of logged in hospital
                $invoices = $hospital->invoices();

                $invoice_item = [];
                $invoices->transform(function ($item, $key) use (&$invoice_item) {
                    $data = collect($item->lines->data)->where('proration', false);
                    $data->transform(function ($item) use ($key, &$invoice_item) {
                        $invoice_item[$key]['id'] = $item->id;
                        $invoice_item[$key]['currency'] = $item->currency;
                        $invoice_item[$key]['price'] = $item->amount / 100;
                        $invoice_item[$key]['decimal_price'] = number_format((float)($item->amount / 100), 2, '.', '');
                        $invoice_item[$key]['period_start'] = Carbon::createFromTimestamp($item->period->start)->toDateString();
                        $invoice_item[$key]['period_end'] = Carbon::createFromTimestamp($item->period->end)->toDateString();
                        $invoice_item[$key]['quantity'] = $item->quantity;
                        $invoice_item[$key]['description'] = $item->description;
                        $invoice_item[$key]['metadata'] = $item->metadata;
                    });
                });

                $this->setMeta('message', __('messages.hospital.subscription.fetchStripePlan'));
                $this->setMeta('status', AppConstant::STATUS_OK);
                $this->setData('billing_details', $plans);
                $this->setData('invoices', $invoice_item);
            } else {
                $this->setMeta('message', __('messages.hospital.subscription.fetchStripePlan'));
                $this->setMeta('status', AppConstant::STATUS_OK);
                $this->setData('billing_details', $hospital->only('id', 'email', 'is_free_user', 'free_trial_starts_at', 'free_trial_ends_at'));
            }
            return response()->json($this->setResponse(), AppConstant::OK);
        } catch (Exception $e) {
            //$this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('message', $e->getMessage());
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @param CreateSubscriptionRequest $request
     * @return JsonResponse
     */
    public function createSubscription(CreateSubscriptionRequest $request): JsonResponse
    {
        try {
            $validated = $request->validated();
            $hospitalId = Auth::id();
            $hospital = Hospital::find($hospitalId);

            if (strtolower($validated['planType']) === "free") {
                if (!is_null($hospital->free_trial_starts_at) && !is_null($hospital->free_trial_ends_at)) {
                    $this->setMeta('message', __('messages.hospital.subscription.alreadySubscribedToFreePlan'));
                    $this->setMeta('status', AppConstant::STATUS_OK);
                    return response()->json($this->setResponse(), AppConstant::OK);
                }

                $hospital->update([
                    'is_free_user' => 1,
                    'free_trial_starts_at' => Carbon::now(),
                    'free_trial_ends_at' => Carbon::now()->addMonths(1)
                ]);

                $this->setMeta('message', __('messages.hospital.subscription.subscribedToFree'));
                $this->setMeta('status', AppConstant::STATUS_OK);
                return response()->json($this->setResponse(), AppConstant::OK);
            }

            $hospital->createOrGetStripeCustomer([
                "source" => $validated['stripeToken']
            ]);

            $paymentMethods = $hospital->paymentMethods()->map(function ($paymentMethod) {
                return $paymentMethod->asStripePaymentMethod();
            })->first();

            if (!$hospital->subscribed('Daily')) {
                $hospital->newSubscription('Daily', 'price_1LHn27KrvmJx6ecLk9HzaEhO')->add();
                $this->setMeta('message', __('messages.hospital.subscription.subscribed'));
            } else {
                $this->setMeta('message', __('messages.hospital.subscription.alreadySubscribed'));
            }

            $subscription = Subscription::whereHospitalId($hospital['id'])->first();
            Hospital::whereId($hospital['id'])->update([
                'is_free_user' => 0,
                'stripe_subscription_id' => optional($subscription)->id,
                'pm_type' => $paymentMethods->type,
                'pm_last_four' => $paymentMethods->card->last4
            ]);

            $this->setMeta('status', AppConstant::STATUS_OK);
            return response()->json($this->setResponse(), AppConstant::OK);

        } catch (Exception $e) {
            $this->setMeta('message', $e->getMessage());
            //$this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @return JsonResponse
     */
    public function cancelSubscription(): JsonResponse
    {
        try {
            $hospital = Auth::user();

            if ($hospital->subscribed('Premium')) {
                $hospital->subscription('Premium')->cancel();
            }

            $this->setMeta('message', __('messages.hospital.subscription.subscriptionCanceled'));
            $this->setMeta('status', AppConstant::STATUS_OK);
            return response()->json($this->setResponse(), AppConstant::OK);
        } catch (Exception $e) {
            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
