<?php

namespace App\Http\Controllers\API\V1\Hospital;

use App\Http\Controllers\Controller;
use App\Models\Physician\HospitalNotification;
use App\Traits\ApiResponse;
use App\Utils\AppConstant;
use Exception;
use Illuminate\Database\QueryException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

class NotificationController extends Controller
{
    use ApiResponse;

    public function index(Request $request): JsonResponse
    {
        try {
            $notificationList = HospitalNotification::whereReceiverId(Auth::id())->orderBy('id', 'DESC');
            $paginate = $request->paginate ?? 10;
            $paginatedList = $notificationList->paginate($paginate);

            $pagination = [
                "total" => $paginatedList->total(),
                "total_pages" => $paginatedList->lastPage(),
                "current_page" => $paginatedList->currentPage(),
                "next_page_url" => $paginatedList->nextPageUrl(),
                "previous_page_url" => $paginatedList->previousPageUrl(),
            ];
            $this->setMeta('status', AppConstant::STATUS_OK);
            $this->setPaginate($pagination);
            $this->setData('notification_list', $notificationList->get());
            return response()->json($this->setResponse(), AppConstant::OK);
        } catch (QueryException|Exception $e) {
            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
    public function updateStatus(int $notificationId): JsonResponse
    {
        try {
            $notificationList = HospitalNotification::whereId($notificationId)->first();

            if (!$notificationList) {
                $this->setMeta('message', __('messages.recordNotFound'));
                $this->setMeta('status', AppConstant::STATUS_FAIL);
                return response()->json($this->setResponse(), Response::HTTP_NOT_FOUND);
            }
            $notificationList->is_read = true;
            $notificationList->save();

            $this->setMeta('status', AppConstant::STATUS_OK);
            $this->setData('notification', $notificationList);
            return response()->json($this->setResponse(), AppConstant::OK);
        } catch (QueryException|Exception $e) {
            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
