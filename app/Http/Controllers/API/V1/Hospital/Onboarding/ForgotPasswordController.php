<?php

namespace App\Http\Controllers\API\V1\Hospital\Onboarding;

use App\Http\Controllers\Controller;
use App\Http\Requests\API\V1\Hospital\Onboarding\ForgotPasswordRequest;
use App\Http\Requests\API\V1\Hospital\Onboarding\HospitalUpdatePasswordRequest;
use App\Http\Requests\API\V1\Hospital\Onboarding\HospitalVerifyPasswordRequest;
use App\Mail\HospitalForgotPasswordMail;
use App\Models\Hospital\Onboarding\Hospital;
use App\Models\Hospital\Onboarding\HospitalAccess;
use App\Models\Hospital\Onboarding\HospitalForgotPassword;
use App\Traits\ApiResponse;
use App\Utils\AppConstant;
use Carbon\Carbon;
use Exception;
use Hash;
use Illuminate\Database\QueryException;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Str;
use Symfony\Component\HttpFoundation\Response;

class ForgotPasswordController extends Controller
{
    use ApiResponse;

    public function ForgotPassword(ForgotPasswordRequest $request): JsonResponse
    {
        try {
            $validated = $request->validated();
            $email = $validated['email'];

            $hospital = Hospital::whereEmail($email)->select('name', 'email', 'status')->first();

            if (!$hospital) {
                $this->setMeta('message', __('messages.hospital.hospitalNotFound'));
                $this->setMeta('status', AppConstant::STATUS_FAIL);
                return response()->json($this->setResponse(), Response::HTTP_UNPROCESSABLE_ENTITY);
            }

            if ($hospital->status == 0) {
                $this->setMeta('message', __('messages.accountBlocked'));
                $this->setMeta('status', AppConstant::STATUS_FAIL);
                return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
            }

            $resetToken = Str::random(60);
            HospitalForgotPassword::updateOrCreate(
                [
                    'email' => $email
                ],
                [
                    'token' => $resetToken,
                ]
            );

            $tokenData = HospitalForgotPassword::whereEmail($email)->first();
            $this->sendResetEmail($email, $tokenData->token);

            $this->setMeta('message', __('messages.password.verifyPassword.sent'));
            $this->setMeta('status', AppConstant::STATUS_OK);
            return response()->json($this->setResponse(), Response::HTTP_OK);
        } catch (QueryException|Exception $e) {
            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function verifyToken(array $validated)
    {

        // Validate the token
        $tokenData = HospitalForgotPassword::where(
            [
                'email' => $validated['email'],
                'token' => $validated['token']
            ]
        )->first();

        // Redirect the user back to the password reset request form if the token is invalid
        if (!$tokenData) {
            return false;
        }

        $startTime = Carbon::parse($tokenData->updated_at);
        $finishTime = Carbon::now();

        $totalDuration = $finishTime->diffInSeconds($startTime);
        $totalDuration = abs(gmdate('i', $totalDuration));

        if ($totalDuration > 5) {
            return false;
        }
        return true;
    }

    public function verifyPasswordResetToken(HospitalVerifyPasswordRequest $request): JsonResponse
    {
        try {
            $validated = $request->validated();

            $tokenVerified = $this->verifyToken($validated);
            if (!$tokenVerified) {
                $this->setMeta('message', __('messages.token.resetTokenExpiredOrInvalidData'));
                $this->setMeta('status', AppConstant::STATUS_FAIL);
                return response()->json($this->setResponse(), Response::HTTP_UNPROCESSABLE_ENTITY);
            }

            $this->setMeta('message', __('messages.token.verified'));
            $this->setMeta('status', AppConstant::STATUS_OK);
            return response()->json($this->setResponse(), Response::HTTP_OK);
        } catch (QueryException|Exception $e) {
            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    private function sendResetEmail($email, $token): bool
    {
        //Retrieve the user from the database
        $hospital = Hospital::whereEmail($email)->select('name', 'email')->first();

        //Generate, the password reset link. The token generated is embedded in the link
        $link = config('appconstants.HOSPITAL_URL') . '/password/reset-password?token=' . $token . '&email=' . urlencode($hospital->email);
        $hospital['link'] = $link;

        try {
            Mail::to($email)->send(new HospitalForgotPasswordMail($hospital));
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    public function showResetPasswordForm()
    {
        $data['token'] = $_GET['token'];
        $data['email'] = $_GET['email'];
        return view('Hospital.Onboarding.ForgotPassword')->with(['data' => $data]);
    }

    public function updatePassword(HospitalUpdatePasswordRequest $request)
    {
        try {
            $validated = $request->validated();
            $password = $validated['password'];
            unset($validated['password']);

            $tokenVerified = $this->verifyToken($validated);

            if (!$tokenVerified) {
                $this->setMeta('message', __('messages.token.resetTokenExpiredOrInvalidData'));
                $this->setMeta('status', AppConstant::STATUS_FAIL);
                return response()->json($this->setResponse(), Response::HTTP_UNPROCESSABLE_ENTITY);
            }

            $hospital = Hospital::whereEmail($validated['email'])->first();

            // Redirect the user back if the email is invalid
            if (!$hospital) {
                $request->session()->flash('password_error_message', __('messages.hospital.hospitalNotFound'));
                return Redirect::back();
            }

            $hospitals_all_token = HospitalAccess::where('hospital_id', $hospital->id)->get('access_token');

            foreach ($hospitals_all_token as $t) {
                HospitalAccess::where('access_token', '=', $t['access_token'])->delete();
            }

            //Hash and update the new password
            $hospital->password = bcrypt($password);
            $hospital->update(); //or $hospital->save();

            //Delete the token
            HospitalForgotPassword::whereEmail($hospital->email)
                ->delete();

            $this->setMeta('message', __('messages.password.set'));
            $this->setMeta('status', AppConstant::STATUS_OK);
            return response()->json($this->setResponse(), Response::HTTP_OK);
        } catch (QueryException|Exception $e) {
            $this->setMeta('message', $e->getMessage());
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
