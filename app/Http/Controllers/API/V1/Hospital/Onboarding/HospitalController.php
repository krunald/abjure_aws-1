<?php

namespace App\Http\Controllers\API\V1\Hospital\Onboarding;

use App\Http\Controllers\Controller;
use App\Http\Requests\API\V1\Hospital\Onboarding\ChangePasswordRequest;
use App\Http\Requests\API\V1\Hospital\Onboarding\HospitalHighlightsRequest;
use App\Http\Requests\API\V1\Hospital\Onboarding\HospitalOnboardingRequest;
use App\Http\Requests\API\V1\Hospital\Onboarding\LoginRulesRequest;
use App\Http\Requests\API\V1\Hospital\Onboarding\UpdateHighlightsRequest;
use App\Http\Requests\API\V1\Hospital\Onboarding\UpdateHospitalRequest;
use App\Http\Requests\API\V1\Physician\VerifyEmailRequest;
use App\Mail\Hospital\VerifyEmail;
use App\Models\Hospital\common\NotificationPreferences;
use App\Models\Hospital\Onboarding\Hospital;
use App\Models\Hospital\Onboarding\HospitalAccess;
use App\Models\Hospital\Onboarding\HospitalHighlights;
use App\Models\Hospital\Onboarding\HospitalVerifyEmail;
use App\Traits\ApiResponse;
use App\Utils\AppConstant;
use Carbon\Carbon;
use Exception;
use Illuminate\Database\QueryException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Ramsey\Uuid\Uuid;
use Stripe\StripeClient;
use Symfony\Component\HttpFoundation\Response;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Token;

class HospitalController extends Controller
{
    use ApiResponse;

    public $token = true;
    public $hospitalNotFound = 'messages.hospital.hospitalNotFound';
    public $somethingWentWrong = 'messages.somethingWrong';
    public $hospitalProfileUpdated = 'messages.hospital.profile_updated';
    public $hospitalHighlightAdded = 'messages.hospital.highlights_added';
    public $hospitalHighlightUpdated = 'messages.hospital.highlights_updated';
    public $hospitalHighlightCreated = 'messages.hospital.highlights_created';

    /**
     * @param HospitalOnboardingRequest $request
     * @return JsonResponse
     */
    public function register(HospitalOnboardingRequest $request): JsonResponse
    {
        try {
            $validated = $request->validated();
            DB::beginTransaction();
            $hospital = Hospital::create(
                array_merge(
                    $validated,
                    [
                        'password' => bcrypt($request->password),
                        'uuid' => Uuid::uuid4()->toString()
                    ]
                )
            );
            DB::commit();

            $this->setMeta('message', __('messages.registered'));
            $this->setMeta('status', AppConstant::STATUS_OK);
            $this->setData('hospital', $hospital);
            return response()->json($this->setResponse(), AppConstant::OK);
        } catch (QueryException|Exception $e) {
            DB::rollBack();
            $this->setMeta('message', __($this->somethingWentWrong));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @param LoginRulesRequest $request
     * @return JsonResponse
     */
    public function login(LoginRulesRequest $request): JsonResponse
    {
        try {
            $validated = $request->validated();

            $hospital = Hospital::where('email', $validated['email'])->where('status', 1)->first();
            if (!$hospital) {
                $this->setMeta('message', __($this->hospitalNotFound));
                $this->setMeta('status', AppConstant::STATUS_FAIL);
                return response()->json($this->setResponse(), AppConstant::UNAUTHORIZED);
            }
            $hospitalHighlights = HospitalHighlights::where('hospital_id', $hospital['id'])->first();


            $claim_credentials = [
                'hospital_id' => $hospital['id'],
                'name' => $hospital['name'],
                'email' => $hospital['email']
            ];

            if (!$jwt_token = JWTAuth::claims($claim_credentials)->attempt($validated)) {
                $this->setMeta('message', __($this->hospitalNotFound));
                $this->setMeta('status', AppConstant::STATUS_FAIL);
                return response()->json($this->setResponse(), AppConstant::UNAUTHORIZED);
            }

            $hospital = auth()->user();
            $access_token = $this->createTokenObj($jwt_token);
            HospitalAccess::Create([
                'hospital_id' => $hospital['id'],
                'access_token' => $jwt_token
            ]);
            $hospital['highlights'] = $hospitalHighlights;
            $this->setMeta('message', __('messages.hospital.login'));
            $this->setMeta('status', AppConstant::STATUS_OK);
            $this->setData('hospital', $hospital);
            $this->setData('hospital_token', $access_token);
            return response()->json($this->setResponse(), AppConstant::OK);
        } catch (QueryException|Exception $e) {
            $this->setMeta('message', $e->getMessage());
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function logout(Request $request): JsonResponse
    {
        try {
            $token = str_replace('Bearer ', '', $request->header('Authorization'));
            HospitalAccess::where('access_token', '=', $token)->delete();
            JWTAuth::invalidate(JWTAuth::parseToken());
            auth()->logout();

            $this->setMeta('message', __('messages.hospital.logout'));
            $this->setMeta('status', AppConstant::STATUS_OK);
            return response()->json($this->setResponse(), AppConstant::OK);
        } catch (QueryException|Exception $e) {
            $this->setMeta('message', __($this->somethingWentWrong));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @param UpdateHospitalRequest $request
     * @return JsonResponse
     */
    public function updateHospital(UpdateHospitalRequest $request): JsonResponse
    {
        try {
            $hospitalId = Auth::id();
            $validated = $request->validated();
            Hospital::whereId($hospitalId)->update($validated);

            $this->setMeta('message', __($this->hospitalProfileUpdated));
            $this->setMeta('status', AppConstant::STATUS_OK);
            $this->setData('hospital', Hospital::find($hospitalId));
            return response()->json($this->setResponse(), AppConstant::OK);
        } catch (QueryException|Exception $e) {
            $this->setMeta('message', __($this->somethingWentWrong));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @param HospitalHighlightsRequest $request
     * @return JsonResponse
     */
    public function createHighlights(HospitalHighlightsRequest $request): JsonResponse
    {
        try {
            $validated = $request->validated();
            $validated['hospital_id'] = Auth::id();
            $specialistAvailable = $validated['specialist_available'];
            unset($validated['specialist_available']);

            $hospitalHighlights = HospitalHighlights::create($validated);
            Hospital::find($validated['hospital_id'])->speciality()->sync($specialistAvailable);

            $this->setMeta('message', __($this->hospitalHighlightAdded));
            $this->setMeta('status', AppConstant::STATUS_OK);
            $this->setData('highlights', $hospitalHighlights);
            return response()->json($this->setResponse(), AppConstant::OK);
        } catch (QueryException|Exception $e) {
            $this->setMeta('message', __($this->somethingWentWrong));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @param UpdateHighlightsRequest $request
     * @param $highlights_id
     * @return JsonResponse
     */
    public function updateHighlights(UpdateHighlightsRequest $request, $highlights_id): JsonResponse
    {
        try {
            $validated = $request->validated();
            $validated['hospital_id'] = Auth::id();
            if (isset($validated['specialist_available'])) {
                $specialistAvailable = $validated['specialist_available'];
                unset($validated['specialist_available']);
                Hospital::find($validated['hospital_id'])->speciality()->sync($specialistAvailable);
            }

            HospitalHighlights::whereId($highlights_id)->update($validated);

            $this->setMeta('message', __($this->hospitalHighlightUpdated));
            $this->setMeta('status', AppConstant::STATUS_OK);
            $this->setData('highlights', HospitalHighlights::with('speciality.specialityName')->find($highlights_id));
            return response()->json($this->setResponse(), AppConstant::OK);

        } catch (QueryException|Exception $e) {
            $this->setMeta('message', $e->getMessage());
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function fetchLoggedInHospital(Request $request): JsonResponse
    {
        try {
            $token = str_replace('Bearer ', '', $request->header('Authorization'));
            $hospital = JWTAuth::toUser($token);

            $this->setMeta('message', __('messages.hospital.profile_fetched'));
            $this->setMeta('status', AppConstant::STATUS_OK);
            $this->setData('hospital', $hospital);
            return response()->json($this->setResponse(), AppConstant::OK);
        } catch (QueryException|Exception $e) {
            $this->setMeta('message', __($this->somethingWentWrong));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @return JsonResponse
     */
    public function fetchHighlights(): JsonResponse
    {
        try {
            $hospitalId = Auth::id();
            $data = Hospital::with(['highlights', 'speciality'])->whereId($hospitalId)->first();

            if (!$data) {
                $this->setMeta('message', __('messages.hospital.hospitalHighlightsNotFound'));
                $this->setMeta('status', AppConstant::STATUS_FAIL);
                return response()->json($this->setResponse(), AppConstant::OK);
            }

            $this->setMeta('message', __('messages.hospital.profile_fetched'));
            $this->setMeta('status', AppConstant::STATUS_OK);
            $this->setData('hospital', $data);
            return response()->json($this->setResponse(), AppConstant::OK);
        } catch (QueryException|Exception $e) {
            $this->setMeta('message', __($this->somethingWentWrong));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @param ChangePasswordRequest $request
     * @return JsonResponse
     */
    public function changePassword(ChangePasswordRequest $request): JsonResponse
    {
        try {
            $validated = $request->validated();
            $hospital = Auth::user();

            //match current password in db
            if (!(Hash::check($validated['current_password'], $hospital->password))) {
                $this->setMeta('message', __('passwords.password.currentPassword'));
                $this->setMeta('status', AppConstant::STATUS_FAIL);
                return new JsonResponse($this->setResponse(), Response::HTTP_UNPROCESSABLE_ENTITY);
            }

            //new password match with current password
            if (strcmp($validated['current_password'], $validated['new_password']) == 0) {
                $this->setMeta('message', __('passwords.password.samePassword'));
                $this->setMeta('status', AppConstant::STATUS_FAIL);
                return new JsonResponse($this->setResponse(), Response::HTTP_UNPROCESSABLE_ENTITY);
            }

            //Change Password
            $hospitalAllToken = HospitalAccess::whereHospitalId($hospital['id'])->get('access_token');
            $currentToken = str_replace('Bearer ', '', $request->header('Authorization'));
            try {
                foreach ($hospitalAllToken as $t) {
                    if ($currentToken !== $t['access_token']) {
                        JWTAuth::manager()->invalidate(new Token($t['access_token']));
                        HospitalAccess::whereAccessToken($t['access_token'])->delete();
                    }
                }
            } catch (Exception $e) {
                HospitalAccess::whereAccessToken($t['access_token'])->delete();
            }

            $hospital->password = bcrypt($validated['new_password']);
            $hospital->save();

            $validated['password'] = $validated['new_password'];
            $validated['email'] = $hospital['email'];
            unset($validated['current_password']);
            unset($validated['new_password']);

            $claim_credentials = [
                'hospital_id' => $hospital['id'],
                'name' => $hospital['name'],
                'email' => $hospital['email']
            ];

            $jwt_token = JWTAuth::claims($claim_credentials)->attempt($validated);
            $access_token = $this->createTokenObj($jwt_token);
            $this->setMeta('message', __('messages.password.set'));
            $this->setMeta('status', AppConstant::STATUS_OK);
            $this->setData('token', $access_token);
            return response()->json($this->setResponse(), AppConstant::OK);
        } catch (QueryException|Exception $e) {
            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @param String $token
     * @return array
     */
    protected function createTokenObj(string $token): array
    {
        return [
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth('api')->factory()->getTTL() * 60,
        ];
    }


    private function sendVerificationEmail($email, $token): bool
    {
        //Retrieve the user from the database
        $hospital = Hospital::whereEmail($email)->select('name', 'email')->first();

        //Generate, the email verification link. The token generated is embedded in the link
        $link = config('appconstants.APP_URL') . '/api/hospital/verify/email?token=' . $token . '&email=' . urlencode($hospital->email);
        $hospital['link'] = $link;
        $hospital['name'] = $hospital->name;
        try {
            Mail::to($email)->send(new VerifyEmail($hospital));
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    public function verifyEmailLink(VerifyEmailRequest $request)
    {
        try {
            $validated = $request->validated();
            $tokenVerified = $this->verifyToken($validated);
            if (!$tokenVerified) {
                $this->setMeta('message', __('messages.token.verifyTokenExpiredOrInvalidData'));
                $this->setMeta('status', AppConstant::STATUS_FAIL);
                return redirect(config('appconstants.HOSPITAL_URL'));
            }
            $hospital = Hospital::whereEmail($validated['email'])->first();
            $hospital->is_verified = 1;
            $hospital->update(); //or $hospital->save();
            HospitalVerifyEmail::whereEmail($hospital->email)
                ->delete();
            $this->setMeta('message', __('messages.emailSuccessfullyVerified'));
            $this->setMeta('status', AppConstant::STATUS_OK);
            return view('email.verify.emailVerifiedSuccessfully')->with(['type' => 'HOSPITAL']);
        } catch (QueryException|Exception $e) {
            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @param array $validated
     * @return bool
     */
    public function verifyToken(array $validated): bool
    {

        // Validate the token
        $tokenData = HospitalVerifyEmail::where(
            [
                'email' => $validated['email'],
                'token' => $validated['token']
            ]
        )->first();

        // Return false if the token is invalid
        if (!$tokenData) {
            return false;
        }

        $startTime = Carbon::parse($tokenData->updated_at);
        $finishTime = Carbon::now();

        $totalDuration = $finishTime->diffInSeconds($startTime);
        $totalDuration = abs(gmdate('i', $totalDuration));

        if ($totalDuration > 5) {
            return false;
        }
        return true;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function reSendEmailVerificationLink(Request $request): JsonResponse
    {
        try {
            $resetToken = Str::random(60);
            $email = Auth::user()->email;
            $hospital = Hospital::whereEmail($email)->first();
            if ($hospital->is_verified) {
                $this->setMeta('message', __('messages.physician.emailAlreadyVerified'));
                $this->setMeta('status', AppConstant::STATUS_FAIL);
                return response()->json($this->setResponse(), Response::HTTP_UNPROCESSABLE_ENTITY);
            }
            HospitalVerifyEmail::updateOrCreate(
                [
                    'email' => $email
                ],
                [
                    'token' => $resetToken,
                ]
            );
            $this->sendVerificationEmail($email, $resetToken);
            $token = str_replace('Bearer ', '', $request->header('Authorization'));
            HospitalAccess::where('access_token', '=', $token)->delete();
            JWTAuth::invalidate(JWTAuth::parseToken());
            auth()->logout();
            $this->setMeta('message', __('messages.physician.resendEmailVerification'));
            $this->setMeta('status', AppConstant::STATUS_OK);
            return response()->json($this->setResponse(), Response::HTTP_OK);
        } catch (QueryException|Exception $e) {
            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

}
