<?php

namespace App\Http\Controllers\API\V1\Hospital\common;

use App\Http\Controllers\Controller;
use App\Http\Requests\API\V1\Hospital\Common\UpdateNotificationPreferenceRequest;
use App\Models\Hospital\common\NotificationPreferences;
use App\Traits\ApiResponse;
use App\Utils\AppConstant;
use Illuminate\Database\QueryException;
use Illuminate\Http\Client\Request;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Exception;

class NotificationPreferencesController extends Controller
{
    use ApiResponse;
    public function updatePreferences(UpdateNotificationPreferenceRequest $request): JsonResponse
    {
        try {
            $validated = $request->validated();
            NotificationPreferences::whereHospitalId(auth()->user()->id)
                ->update($validated);

            $this->setMeta('message', __('messages.hospital.notifications.update'));
            $this->setMeta('status', AppConstant::STATUS_OK);
            return response()->json($this->setResponse(), AppConstant::OK);
        } catch (QueryException|Exception $e) {
            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function fetchPreferences()
    {
        try {
            $notificationPreferences = NotificationPreferences::where('hospital_id', auth()->user()->id)
                ->first();

            $this->setMeta('message', __('messages.hospital.notifications.fetch'));
            $this->setMeta('status', AppConstant::STATUS_OK);
            $this->setData('preferences', $notificationPreferences);
            return response()->json($this->setResponse(), AppConstant::OK);
        } catch (QueryException|Exception $e) {
            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
