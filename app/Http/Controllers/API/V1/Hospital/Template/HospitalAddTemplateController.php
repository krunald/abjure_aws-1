<?php

namespace App\Http\Controllers\API\V1\Hospital\Template;

use App\Http\Controllers\Controller;
use App\Http\Requests\API\V1\Hospital\Template\AddTemplateRequest;
use App\Http\Requests\API\V1\Hospital\Template\UpdateTemplateRequest;
use App\Models\Hospital\Template\GeneralSurgeryTemplate;
use App\Models\Hospital\Template\HospitalAddTemplate;
use App\Models\Hospital\Template\HospitalistTemplate;
use App\Traits\ApiResponse;
use App\Utils\AppConstant;
use Exception;
use Illuminate\Database\QueryException;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Response;

class HospitalAddTemplateController extends Controller
{
    use ApiResponse;

    /**
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        try {
            $templateList = HospitalAddTemplate::with(['jobDetailsGeneralSurgeon', 'jobDetailsHospitalist'])->whereHospitalId(auth()->user()->id);

            if (!$templateList) {
                $this->setMeta('message', __('messages.hospital.template.list_fetch'));
                $this->setMeta('status', AppConstant::STATUS_FAIL);
                return response()->json($this->setResponse(), AppConstant::OK);
            }

            $paginate = $request->paginate ?? 10;
            $paginatedList = $templateList->paginate($paginate);

            $pagination = [
                "total" => $paginatedList->total(),
                "current_page" => $paginatedList->currentPage(),
                "next_page_url" => $paginatedList->nextPageUrl(),
                "previous_page_url" => $paginatedList->previousPageUrl(),
            ];

            $templateList = $templateList->get()->sortBy([['id', 'desc']]);

            $this->setMeta('message', __('messages.hospital.template.fetch'));
            $this->setMeta('status', AppConstant::STATUS_OK);
            $this->setPaginate($pagination);
            $this->setData('list', $templateList);
            return response()->json($this->setResponse(), AppConstant::OK);
        } catch (QueryException $e) {
            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        } catch (\Exception $e) {
            $this->setMeta('message', $e->getMessage());
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function create(AddTemplateRequest $request)
    {
        //
    }

    /**
     * @param AddTemplateRequest $request
     * @return JsonResponse
     */
    public function store(AddTemplateRequest $request): JsonResponse
    {
        try {
            $validated = $request->validated();

            $basicJobDetails = [
                'hospital_id' => auth()->user()->id,
                'template_name' => $validated['template_name'],
                'job_title' => $validated['job_title'],
                'job_type' => $validated['job_type'],
                'slots' => $validated['slots'],
                'min_experience' => $validated['min_experience'],
                'max_experience' => $validated['max_experience'],
                'description' => $validated['description'],
            ];

            DB::beginTransaction();
            $hospitalTemplate = HospitalAddTemplate::create($basicJobDetails);

            collect($basicJobDetails)->each(function ($item, $key) use (&$validated) {
                unset($validated[$key]);
            });

            $validated['template_id'] = $hospitalTemplate->id;

            $templateWith = "jobDetailsGeneralSurgeon";
            if ($hospitalTemplate['job_type'] === "GENERAL_SURGERY") {
                GeneralSurgeryTemplate::create($validated);
            } elseif ($hospitalTemplate['job_type'] === "HOSPITALIST") {
                HospitalistTemplate::create($validated);
                $templateWith = "jobDetailsHospitalist";
            }

            $jobTemplate = HospitalAddTemplate::with($templateWith)->whereId($hospitalTemplate->id)->first();
            DB::commit();

            $this->setMeta('message', __('messages.hospital.template.add'));
            $this->setMeta('status', AppConstant::STATUS_OK);
            $this->setData('template', $jobTemplate);
            return response()->json($this->setResponse(), AppConstant::OK);
        } catch (QueryException|Exception $e) {
            DB::rollback();
            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', false);
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @param $jobID
     * @return JsonResponse
     */
    public function show($jobID): JsonResponse
    {
        try {
            $jobTemplate = HospitalAddTemplate::with(['jobDetailsGeneralSurgeon', 'jobDetailsHospitalist'])->find($jobID);

            if (!$jobTemplate) {
                $this->setMeta('message', __('messages.hospital.template.templateNotFound'));
                $this->setMeta('status', AppConstant::STATUS_FAIL);
                return response()->json($this->setResponse(), AppConstant::OK);
            }

            if ($jobTemplate->job_type === "GENERAL_SURGERY") {
                $jobTemplate->unsetRelation('jobDetailsHospitalist');
            } else {
                $jobTemplate->unsetRelation('jobDetailsGeneralSurgeon');
            }

            $this->setMeta('message', __('messages.hospital.template.fetch'));
            $this->setMeta('status', AppConstant::STATUS_OK);
            $this->setData('template', $jobTemplate);
            return response()->json($this->setResponse(), AppConstant::OK);
        } catch (QueryException|\Exception $e) {
            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function edit()
    {
        //
    }

    /**
     * @param UpdateTemplateRequest $request
     * @param $templateID
     * @return JsonResponse
     */
    public function update(UpdateTemplateRequest $request, $templateID): JsonResponse
    {
        try {
            $validated = $request->validated();

            $basicJobDetails = [
                'template_name' => $validated['template_name'],
                'job_title' => $validated['job_title'],
                'job_type' => $validated['job_type'],
                'slots' => $validated['slots'],
                'min_experience' => $validated['min_experience'],
                'max_experience' => $validated['max_experience'],
                'description' => $validated['description'],
            ];

            DB::beginTransaction();
            $hospitalTemplate = HospitalAddTemplate::whereId($templateID)->first();
            $hospitalTemplate->update($basicJobDetails);
            collect($basicJobDetails)->each(function ($item, $key) use (&$validated) {
                unset($validated[$key]);
            });

            $templateWith = "jobDetailsGeneralSurgeon";
            if ($hospitalTemplate['job_type'] === "GENERAL_SURGERY") {
                GeneralSurgeryTemplate::where('template_id', $templateID)->first()->update($validated);
            } elseif ($hospitalTemplate['job_type'] === "HOSPITALIST") {
                HospitalistTemplate::where('template_id', $templateID)->first()->update($validated);
                $templateWith = "jobDetailsHospitalist";
            }
            $jobTemplate = HospitalAddTemplate::with($templateWith)->whereId($hospitalTemplate->id)->first();
            DB::commit();

            $this->setMeta('message', __('messages.hospital.template.update'));
            $this->setMeta('status', AppConstant::STATUS_OK);
            $this->setData('template', $jobTemplate);
            return response()->json($this->setResponse(), AppConstant::OK);
        } catch (QueryException|\Exception $e) {
            DB::rollBack();
            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function destroy()
    {
        //
    }

    /**
     * @return JsonResponse
     */
    public function fetchList(): JsonResponse
    {
        try {
            $templateList = HospitalAddTemplate::select('id', 'template_name as name')->whereHospitalId(auth()->user()->id)->get();

            $this->setMeta('message', __('messages.hospital.template.fetch'));
            $this->setMeta('status', AppConstant::STATUS_OK);
            $this->setData('list', $templateList);
            return response()->json($this->setResponse(), AppConstant::OK);
        } catch (QueryException|\Exception $e) {
            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
