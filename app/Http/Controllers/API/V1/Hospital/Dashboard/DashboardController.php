<?php

namespace App\Http\Controllers\API\V1\Hospital\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Hospital\Jobs\Jobs;
use App\Models\Hospital\Jobs\JobSchedule;
use App\Models\Hospital\Jobs\UserJobsScheduleInterview;
use App\Models\Physician\Jobs\UserJobs;
use App\Traits\ApiResponse;
use App\Utils\AppConstant;
use Carbon\Carbon;
use Exception;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

class DashboardController extends Controller
{
    use ApiResponse;

    /**
     * @return JsonResponse
     */
    public function displayCounts(): JsonResponse
    {
        try {
            $hospitalId = Auth::id();
            $userJobs = UserJobs::whereHas('jobs', function ($query) use ($hospitalId) {
                $query->where('hospital_id', $hospitalId);
            })->with(['jobs' => function ($q) use ($hospitalId) {
                return $q->where('hospital_id', $hospitalId);
            }, 'users', 'usersJobsInvoice' => function ($q) {
                return $q->where('invoice_status', 'PENDING');
            }]);
            $usersJobsInvoice = collect();
            $userJobs->each(function ($item) use (&$usersJobsInvoice) {
                if ($item->usersJobsInvoice) {
                    $usersJobsInvoice->push($item);
                }
            });
            $userJobs->whereBetween('created_at', [Carbon::now()->subDays(7)->toDateTimeString(), Carbon::now()->toDateTimeString()]
            )->orderBy('id', 'DESC')->get();

            $response['recent_applies'] = collect($userJobs->get())->count();
            $response['interview_schedule'] = UserJobsScheduleInterview::whereHospitalId($hospitalId)->where('interview_date', '>=', Carbon::now()->toDateString())->count();
            $response['pending_invoice'] = $usersJobsInvoice->count();
            $this->setMeta('status', AppConstant::STATUS_OK);
            $this->setData('dashboard_cards', $response);
            return response()->json($this->setResponse(), AppConstant::OK);
        } catch (QueryException|Exception $e) {
            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @return JsonResponse
     */
    public function recentApplies(): JsonResponse
    {
        try {
            $hospitalId = Auth::id();
            $userJobs = UserJobs::whereHas('jobs', function ($query) use ($hospitalId) {
                $query->where('hospital_id', $hospitalId);
            })->with(['jobs' => function ($q) use ($hospitalId) {
                $q->where('hospital_id', $hospitalId);
            }, 'users'])->whereBetween('created_at', [Carbon::now()->subDays(7)->toDateTimeString(), Carbon::now()->toDateTimeString()]
            )->orderBy('id', 'DESC')->take(5)->get();

            $this->setMeta('status', AppConstant::STATUS_OK);
            $this->setData('recent_applies', $userJobs);
            return response()->json($this->setResponse(), AppConstant::OK);
        } catch (QueryException|Exception $e) {
            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @return JsonResponse
     */
    public function jobCountByStatus(Request $request): JsonResponse
    {
        try {
            $hospitalId = Auth::id();
            $userJobs = Jobs::where('hospital_id', $hospitalId);
            if (strtolower($request['q']) === "week") {
                $userJobs->where('created_at', '>=', Carbon::now()->subWeek($request['n'])->toDateString());
            }
            if (strtolower($request['q']) === "month") {
                $userJobs->where('created_at', '>=', Carbon::now()->subMonth($request['n'])->toDateString());
            }
            if (strtolower($request['q']) === "year") {
                $userJobs->where('created_at', '>=', Carbon::now()->subYear($request['n'])->toDateString());
            }
            if (strtolower($request['q']) === "days") {
                $userJobs->where('created_at', '>=', Carbon::now()->subDays($request['n'])->toDateString());
            }

            $response = $userJobs->get()->groupBy('job_post_status')
                ->map(function ($item) {
                    return $item->count();
                });

            /*            $jobs = Jobs::whereHospitalId($hospitalId)->with(['jobSchedules' => function ($query) {
                            $query->get()->groupBy('shift');
                        }])->get();
                        $jobs = Jobs::whereHospitalId($hospitalId)->with(['jobSchedules'=> function ($query) {
                            return $query->get()->groupBy('job_schedules.shift');
                        }])->get();
            */

            $this->setMeta('status', AppConstant::STATUS_OK);
            $this->setData('job_count_by_status', $response);
            return response()->json($this->setResponse(), AppConstant::OK);
        } catch (QueryException|Exception $e) {
            $this->setMeta('message', $e->getMessage());
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function fetchRecruitmentDetails(Request $request): JsonResponse
    {
        try {
            $filterType = $request->get('q');
            $filterNumber = $request->get('n');
            $hospitalId = Auth::id();

            $userJobs = UserJobs::select('id', 'application_status', 'job_id')
                ->whereHas('jobs', function ($query) use ($hospitalId) {
                    $query->where('hospital_id', $hospitalId);
                })
                ->with(['jobs' => function ($query) use ($hospitalId) {
                    $query->select('id', 'job_type')->where('hospital_id', $hospitalId);
                }]);
            $userJobs = $this->getFilter($filterType, $filterNumber, $userJobs);

            $data = $userJobs->transform(function ($jobs, $key) {
                return $jobs->mapWithKeys(function ($job, $k) use ($key) {
                    return [
                        strtolower($key . "_" . $k) => $job->count()
                    ];
                });
            })->values()->toArray();
            $data = array_merge(...$data);

            $this->setMeta('status', AppConstant::STATUS_OK);
            $this->setMeta('message', __('messages.hospital.dashboard.fetchRecruitmentStats'));
            $this->setData('recruitment_stats', $data);
            return response()->json($this->setResponse(), AppConstant::OK);
        } catch (QueryException|Exception $e) {
            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function fetchPendingApplicationDetails(Request $request): JsonResponse
    {
        try {
            $filterType = $request->get('q');
            $filterNumber = $request->get('n');
            $hospitalId = Auth::id();

            $userJobs = UserJobs::select('id', 'application_status', 'job_id')
                ->whereHas('jobs', function ($query) use ($hospitalId) {
                    $query->where('hospital_id', $hospitalId);
                })
                ->with(['jobs' => function ($query) use ($hospitalId) {
                    $query->select('id', 'job_type')->where([
                        'hospital_id' => $hospitalId
                    ]);
                }])->where('application_status', UserJobs::APPLICATION_STATUS['APPLIED']);
            $userJobs = $this->getFIlter($filterType, $filterNumber, $userJobs);

            $data = $userJobs->transform(function ($jobs, $key) {
                return $jobs->mapWithKeys(function ($job, $k) use ($key) {
                    return [
                        strtolower($k) => $job->count()
                    ];
                });
            })->values()->toArray();
            $data = array_merge(...$data);

            $this->setMeta('status', AppConstant::STATUS_OK);
            $this->setMeta('message', __('messages.hospital.dashboard.fetchPendingApplications'));
            $this->setData('pending_application', $data);
            return response()->json($this->setResponse(), AppConstant::OK);
        } catch (QueryException|Exception $e) {
            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @param $filter
     * @param $filterNumber
     * @param $userJobs
     * @return mixed
     */
    protected function getFilter($filter, $filterNumber, $userJobs)
    {
        if (strtolower($filter) === "days") {
            $userJobs = $userJobs->whereDate('created_at', '>=', Carbon::now()->subDays($filterNumber));
        } elseif (strtolower($filter) === "month") {
            $userJobs = $userJobs->whereDate('created_at', '>=', Carbon::now()->subMonth($filterNumber));
        } elseif (strtolower($filter) === "year") {
            $userJobs = $userJobs->whereDate('created_at', '>=', Carbon::now()->subYear($filterNumber));
        }

        return $userJobs->get()->groupBy(['application_status', 'jobs.job_type']);
    }
}
