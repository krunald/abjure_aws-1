<?php

namespace App\Http\Controllers\API\V1\Hospital\Jobs;

use App\Http\Controllers\Controller;
use App\Models\Hospital\Jobs\Jobs;
use App\Models\Hospital\Onboarding\Hospital;
use App\Models\Physician\Jobs\UserJobsInvoice;
use App\Traits\ApiResponse;
use App\Utils\AppConstant;
use Carbon\Carbon;
use Exception;
use Illuminate\Database\QueryException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Auth;
use Stripe\StripeClient;
use Symfony\Component\HttpFoundation\Response;

class UserJobsController extends Controller
{
    use ApiResponse;

    public function getAllInvoice(Request $request)
    {
        try {
            $userJobInvoiceArr = UserJobsInvoice::when(isset($request['invoice_status']), function ($q) use ($request) {
                return $q->where('invoice_status', '=', $request['invoice_status']);
            })->with(['userJob.jobs.description', 'userJob.jobs.jobSchedules', 'userJob.jobs.jobPayment', 'userJob.jobs.hospital', 'userJob.users', 'userJob.users.personInfo'])
                ->whereHas('userJob.jobs', function ($q) {
                    return $q->where('hospital_id', Auth::id());
                })->orderBy('id', 'DESC');

            $paginatedList = $userJobInvoiceArr->paginate($request->paginate ?? 10);
            $pagination = [
                "total" => $paginatedList->total(),
                "current_page" => $paginatedList->currentPage(),
                "next_page_url" => $paginatedList->nextPageUrl(),
                "previous_page_url" => $paginatedList->previousPageUrl(),
            ];
            $this->setMeta('status', AppConstant::STATUS_OK);
            $this->setMeta('message', __('messages.hospital.job.invoice.invoiceFetched'));
            $this->setData('user_job_invoice', $paginatedList->getCollection());
            $this->setPaginate($pagination);
            return response()->json($this->setResponse(), AppConstant::OK);
        } catch (QueryException|Exception $e) {
            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function getUserInvoice(int $invoiceId)
    {
        try {
            $userJobInvoice = UserJobsInvoice::whereId($invoiceId)->with(['userJob.jobs.description', 'userJob.jobs.jobSchedules', 'userJob.jobs.hospital', 'userJob.users', 'userJob.users.personInfo'])->first();
            if (!$userJobInvoice) {
                $this->setMeta('status', AppConstant::STATUS_FAIL);
                $this->setMeta('message', __('messages.recordNotFound'));
                return response()->json($this->setResponse(), Response::HTTP_NOT_FOUND);
            }
            $userJobInvoice['jobs'] = $userJobInvoice->userJob->jobs ?? null;
            $userJobInvoice['user'] = $userJobInvoice->userJob->users ?? null;
            $userJobInvoice['hours'] = Carbon::parse($userJobInvoice->hours)->format('H:i') ?? null;
            $userJobInvoice['hours1'] = $userJobInvoice->hours?? null;
            $userJobInvoice->unsetRelation('userJob');
            $this->setMeta('status', AppConstant::STATUS_OK);
            $this->setMeta('message', __('messages.hospital.job.invoice.invoiceFetched'));
            $this->setData('user_job_invoice', $userJobInvoice);
            return response()->json($this->setResponse(), AppConstant::OK);
        } catch (QueryException|Exception $e) {
            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function payUserInvoice(int $userJobId): JsonResponse
    {
        try {
            $userJobInvoice = UserJobsInvoice::whereUserJobsId($userJobId)->first();
            if (!$userJobInvoice) {
                $this->setMeta('status', AppConstant::STATUS_FAIL);
                $this->setMeta('message', __('messages.recordNotFound'));
                return response()->json($this->setResponse(), Response::HTTP_NOT_FOUND);
            }

            $userJobInvoice->invoice_status = "PAID";
            $userJobInvoice->save();

            $this->setMeta('status', AppConstant::STATUS_OK);
            $this->setMeta('message', __('messages.hospital.job.invoice.invoicePaid'));
            $this->setData('user_job_invoice', $userJobInvoice);
            return response()->json($this->setResponse(), AppConstant::OK);
        } catch (QueryException|Exception $e) {
            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
