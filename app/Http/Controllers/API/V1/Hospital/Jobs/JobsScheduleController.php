<?php

namespace App\Http\Controllers\API\V1\Hospital\Jobs;

use App\Helper\Helper;
use App\Http\Controllers\Controller;
use App\Http\Requests\API\V1\Hospital\Jobs\AddJobsSchedule;
use App\Models\Hospital\Jobs\JobSchedule;
use App\Traits\ApiResponse;
use App\Utils\AppConstant;
use Exception;
use Illuminate\Database\QueryException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Response;

class JobsScheduleController extends Controller
{
    use ApiResponse;

    public function store(AddJobsSchedule $request): JsonResponse
    {
        try {
            DB::beginTransaction();
            $jobId = $request->job_id;
            JobSchedule::whereJobId($request['job_id'])->delete();

            collect($request->job_schedule)->map(function ($req) use ($jobId) {
                $req['job_id'] = $jobId;
                $req['start_date'] = Helper::setUTCDate($req['start_date']);
                $req['end_date'] = Helper::setUTCDate($req['end_date']);
                $req['start_time'] = isset($req['start_time']) ? Helper::setUTCDateTime($req['start_date'], $req['start_time']) : null;
                $req['end_time'] = isset($req['end_time']) ? Helper::setUTCDateTime($req['end_date'], $req['end_time']) : null;
                $newReq = new Request($req);
                JobSchedule::create(
                    [
                        'start_date' => $newReq->start_date,
                        'end_date' => $newReq->end_date,
                        'job_id' => $newReq->job_id,
                        'start_time' => $newReq->start_time,
                        'end_time' => $newReq->end_time,
                        'shift' => $newReq->shift,
                    ]);
            });

            $jobSchedule = JobSchedule::whereJobId($jobId)->get();

            $this->setMeta('message', __('messages.hospital.job.schedule.added'));
            $this->setMeta('status', AppConstant::STATUS_OK);
            $this->setData('job_schedule', $jobSchedule);
            DB::commit();
            return response()->json($this->setResponse(), AppConstant::OK);
        } catch (QueryException|Exception $e) {
            DB::rollBack();
            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
