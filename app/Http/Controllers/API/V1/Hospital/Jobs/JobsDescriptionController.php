<?php

namespace App\Http\Controllers\API\V1\Hospital\Jobs;

use App\Http\Controllers\Controller;
use App\Http\Requests\API\V1\Hospital\Jobs\AddJobsDescription;
use App\Models\Hospital\Jobs\JobDescription;
use App\Traits\ApiResponse;
use App\Utils\AppConstant;
use Exception;
use Illuminate\Database\QueryException;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class JobsDescriptionController extends Controller
{
    use ApiResponse;

    public function store(AddJobsDescription $request): JsonResponse
    {
        try {
            $jobDescription = JobDescription::updateOrCreate(['job_id' => $request->only('job_id')], $request->only('job_id', 'description'));
            $this->setMeta('message', __('messages.hospital.job.description.added'));
            $this->setMeta('status', AppConstant::STATUS_OK);
            $this->setData('job_description', $jobDescription);
            return response()->json($this->setResponse(), AppConstant::OK);
        } catch (QueryException|Exception $e) {
            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
