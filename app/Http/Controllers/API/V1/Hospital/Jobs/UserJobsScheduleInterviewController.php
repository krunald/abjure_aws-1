<?php

namespace App\Http\Controllers\API\V1\Hospital\Jobs;

use App\Helper\Helper;
use App\Http\Controllers\Controller;
use App\Http\Requests\API\V1\Hospital\Jobs\UserJobsScheduleInterviewCreateRequest;
use App\Http\Requests\API\V1\Hospital\Jobs\UserJobsScheduleInterviewUpdateRequest;
use App\Models\Hospital\Jobs\UserJobsScheduleInterview;
use App\Models\Physician\Jobs\UserJobs;
use App\Traits\ApiResponse;
use App\Utils\AppConstant;
use Exception;
use Illuminate\Database\QueryException;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Response;

class UserJobsScheduleInterviewController extends Controller
{
    use ApiResponse;

    public function index(): JsonResponse
    {
        try {
            $userJobScheduleInterview = UserJobsScheduleInterview::where(['hospital_id' => Auth::id()])->get();
            $this->setMeta('status', AppConstant::STATUS_OK);
            $this->setMeta('message', __('messages.hospital.userJobs.applicantInterviewScheduleList'));
            $this->setData('applicant_job_scheduled_interviews', $userJobScheduleInterview);
            return response()->json($this->setResponse(), AppConstant::OK);
        } catch (QueryException|Exception $e) {
            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function hospitalCalendarList(): JsonResponse
    {
        try {
            $userJobScheduleInterview = UserJobsScheduleInterview::whereHospitalId(Auth::id())->with(['userJob', 'hospital'])->get();
            $resultArr = [];
            $userJobScheduleInterview->each(function ($item, $index) use (&$resultArr) {
                if (isset($item->userJob)) {
                    $resultArr[$index]['id'] = $item->id;
                    $resultArr[$index]['user_job_id'] = $item->userJob->id;
                    $resultArr[$index]['user_id'] = $item->userJob->user_id;
                    $resultArr[$index]['job_status'] = $item->userJob->jobs->job_post_status;
                    $resultArr[$index]['title'] = $item->userJob->jobs->name;
                    $resultArr[$index]['date'] = Helper::setUTCDate($item->interview_date);
                    $resultArr[$index]['time'] = $item->interview_time;
                    $resultArr[$index]['interview_type'] = $item->interview_type;
                    if ($item->interview_type === 'OTHER') {
                        $resultArr[$index]['other_interview_type'] = $item->other_interview_type;
                        if ($item->other_interview_type === 'VIDEO_CALL') {
                            $resultArr[$index]['interview_link'] = $item->video_call_link;
                        }
                    }
                }
            });

            $this->setMeta('status', AppConstant::STATUS_OK);
            $this->setMeta('message', __('messages.hospital.calendarDetailsFetched'));
            $this->setData('applicant_job_scheduled_interviews', $resultArr);
            return response()->json($this->setResponse(), AppConstant::OK);
        } catch (QueryException|Exception $e) {
            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function show(int $userJobId): JsonResponse
    {
        try {
            $userJobScheduleInterview = UserJobsScheduleInterview::where(['user_job_id' => $userJobId])->first();
            if (!$userJobScheduleInterview) {
                $this->setMeta('status', AppConstant::STATUS_FAIL);
                $this->setMeta('message', __('messages.recordNotFound'));
                return response()->json($this->setResponse(), Response::HTTP_NOT_FOUND);
            }
            $this->setMeta('status', AppConstant::STATUS_OK);
            $this->setMeta('message', __('messages.hospital.userJobs.applicantInterviewScheduleList'));
            $this->setData('applicant_job_scheduled_interviews', $userJobScheduleInterview);
            return response()->json($this->setResponse(), AppConstant::OK);
        } catch (QueryException|Exception $e) {
            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function store(UserJobsScheduleInterviewCreateRequest $request): JsonResponse
    {
        try {
            DB::beginTransaction();
            $validate = $request->validated();
            $validate['hospital_id'] = Auth::id();
            //Below is condition code need to check when needed.
            /*$userData = UserJobsScheduleInterview::where(function ($q) use ($validate) {
                $q->where(['user_id' => $validate['user_id'], 'interview_date' => $validate['interview_date']]);
                $q->whereBetween('interview_time', [Carbon::parse($validate['interview_time'])->subMinute(), Carbon::parse($validate['interview_time'])->addHour()]);
            })->get();
            if (!empty($userData->toArray())) {
                $this->setMeta('status', AppConstant::STATUS_FAIL);
                $this->setMeta('message', __('messages.hospital.calendar.meetingAlreadyExists'));
                return response()->json($this->setResponse(), Response::HTTP_CONFLICT);
            }*/
            $userJobScheduleInterview = UserJobsScheduleInterview::create($validate);
            $this->setMeta('status', AppConstant::STATUS_OK);
            $this->setMeta('message', __('messages.hospital.userJobs.applicantInterviewScheduled'));
            $this->setData('applicant_job_scheduled_interview', $userJobScheduleInterview);
            DB::commit();
            return response()->json($this->setResponse(), AppConstant::OK);
        } catch (QueryException|Exception $e) {
            DB::rollBack();
            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function update(int $jobScheduleInterviewId, UserJobsScheduleInterviewUpdateRequest $request): JsonResponse
    {
        try {
            DB::beginTransaction();
            $validate = $request->validated();
            $validate['hospital_id'] = Auth::id();
            $userJobScheduleInterview = UserJobsScheduleInterview::whereId($jobScheduleInterviewId)->first();

            if (!$userJobScheduleInterview) {
                $this->setMeta('status', AppConstant::STATUS_FAIL);
                $this->setMeta('message', __('messages.recordNotFound'));
                return response()->json($this->setResponse(), Response::HTTP_NOT_FOUND);
            }
            if ($userJobScheduleInterview->userJob->application_status === UserJobs::APPLICATION_STATUS['COMPLETED']) {
                $this->setMeta('status', AppConstant::STATUS_FAIL);
                $this->setMeta('message', __('messages.hospital.job.schedule.cannotUpdateCompletedJobSchedule'));
                return response()->json($this->setResponse(), Response::HTTP_NOT_FOUND);
            }
            $userJobScheduleInterview->update($validate);
            $this->setMeta('status', AppConstant::STATUS_OK);
            $this->setMeta('message', __('messages.hospital.userJobs.applicantInterviewScheduledUpdated'));
            $this->setData('applicant_job_scheduled_interview', $userJobScheduleInterview);
            DB::commit();
            return response()->json($this->setResponse(), AppConstant::OK);
        } catch (QueryException|Exception $e) {
            DB::rollBack();
            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
