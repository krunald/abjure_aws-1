<?php

namespace App\Http\Controllers\API\V1\Hospital\Jobs;

use App\Http\Controllers\Controller;
use App\Http\Requests\API\V1\Hospital\Jobs\UserJobsReviewAndRatingCreateRequest;
use App\Http\Requests\API\V1\Hospital\Jobs\UserJobsReviewAndRatingUpdateRequest;
use App\Models\Hospital\Jobs\UserJobReviewAndRating;
use App\Traits\ApiResponse;
use App\Utils\AppConstant;
use Exception;
use Illuminate\Database\QueryException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class UserJobReviewAndRatingController extends Controller
{
    use ApiResponse;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param UserJobsReviewAndRatingCreateRequest $request
     * @return JsonResponse
     */
    public function store(UserJobsReviewAndRatingCreateRequest $request): JsonResponse
    {
        try {
            $validated = $request->validated();
            $userJobReviewAndRating = UserJobReviewAndRating::create($validated);
            $this->setMeta('message', __('messages.hospital.userJobs.reviewAndRating.added'));
            $this->setMeta('status', AppConstant::STATUS_OK);
            $this->setData('user_job_review_and_rating', $userJobReviewAndRating);
            return response()->json($this->setResponse(), AppConstant::OK);
        } catch (QueryException|Exception $e) {
            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $userJobId
     * @return JsonResponse
     */
    public function show(int $userJobId): JsonResponse
    {
        try {
            $userJobReviewAndRating = UserJobReviewAndRating::whereUserJobsId($userJobId)->first();
            if (!$userJobReviewAndRating) {
                $this->setMeta('status', AppConstant::STATUS_FAIL);
                $this->setMeta('message', __('messages.recordNotFound'));
                return response()->json($this->setResponse(), Response::HTTP_NOT_FOUND);
            }
            $this->setMeta('message', __('messages.hospital.userJobs.reviewAndRating.fetched'));
            $this->setMeta('status', AppConstant::STATUS_OK);
            $this->setData('user_job_review_and_rating', $userJobReviewAndRating);
            return response()->json($this->setResponse(), AppConstant::OK);
        } catch (QueryException|Exception $e) {
            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Hospital\Jobs\UserJobReviewAndRating $userJobReviewAndRating
     * @return \Illuminate\Http\Response
     */
    public function edit(UserJobReviewAndRating $userJobReviewAndRating)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param int $userJobReviewAndRatingId
     * @param UserJobsReviewAndRatingUpdateRequest $request
     * @return JsonResponse
     */
    public function update(int $userJobReviewAndRatingId, UserJobsReviewAndRatingUpdateRequest $request): JsonResponse
    {
        try {
            $userJobReviewAndRating = UserJobReviewAndRating::find($userJobReviewAndRatingId);
            if (!$userJobReviewAndRating) {
                $this->setMeta('status', AppConstant::STATUS_FAIL);
                $this->setMeta('message', __('messages.recordNotFound'));
                return response()->json($this->setResponse(), Response::HTTP_NOT_FOUND);
            }
            $userJobReviewAndRating->update($request->validated());
            $this->setMeta('message', __('messages.hospital.userJobs.reviewAndRating.fetched'));
            $this->setMeta('status', AppConstant::STATUS_OK);
            $this->setData('user_job_review_and_rating', $userJobReviewAndRating);
            return response()->json($this->setResponse(), AppConstant::OK);
        } catch (QueryException|Exception $e) {
            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Hospital\Jobs\UserJobReviewAndRating $userJobReviewAndRating
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserJobReviewAndRating $userJobReviewAndRating)
    {
        //
    }
}
