<?php

namespace App\Http\Controllers\API\V1\Hospital\Jobs;

use App\Http\Controllers\Controller;
use App\Http\Requests\API\V1\Hospital\Jobs\AddJobsPayement;
use App\Models\Hospital\Jobs\JobPayment;
use App\Traits\ApiResponse;
use App\Utils\AppConstant;
use Exception;
use Illuminate\Database\QueryException;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class JobsPaymentController extends Controller
{
    use ApiResponse;

    public function store(AddJobsPayement $request): JsonResponse
    {
        try {
            $jobPayment = JobPayment::updateOrCreate(['job_id' => $request->only('job_id')], $request->only(
                'job_id',
                'payment_type',
                'amount'
            ));
            $this->setMeta('message', __('messages.hospital.job.payment.added'));
            $this->setMeta('status', AppConstant::STATUS_OK);
            $this->setData('job_payment_details', $jobPayment);
            return response()->json($this->setResponse(), AppConstant::OK);
        } catch (QueryException|Exception $e) {
            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
