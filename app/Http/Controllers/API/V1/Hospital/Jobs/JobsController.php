<?php /** @noinspection ForgottenDebugOutputInspection */

namespace App\Http\Controllers\API\V1\Hospital\Jobs;

use App\Helper\Helper;
use App\Http\Controllers\Controller;
use App\Http\Requests\API\V1\Hospital\Jobs\AddJobsRequest;
use App\Http\Requests\API\V1\Hospital\Jobs\JobsStatusUpdateRequest;
use App\Http\Requests\API\V1\Hospital\Jobs\UpdateJobsRequest;
use App\Http\Requests\API\V1\Hospital\Jobs\UserJobsStatusUpdateRequest;
use App\Models\Hospital\Jobs\JobDetailsGeneralSurgeon;
use App\Models\Hospital\Jobs\JobDetailsHospitalist;
use App\Models\Hospital\Jobs\Jobs;
use App\Models\Hospital\Jobs\JobSchedule;
use App\Models\Physician\Jobs\UserJobs;
use App\Models\Physician\Onboarding\User;
use App\Traits\ApiResponse;
use App\Utils\AppConstant;
use Carbon\Carbon;
use DateTime;
use Exception;
use Illuminate\Database\QueryException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Ramsey\Uuid\Uuid;
use Symfony\Component\HttpFoundation\Response;


class JobsController extends Controller
{
    use ApiResponse;

    public function index(Request $request): JsonResponse
    {
        try {
            $jobs = Jobs::whereHospitalId(Auth::id())->with(['jobDetailsGeneralSurgeon', 'jobDetailsHospitalist', 'description', 'jobSchedules', 'jobPayment', 'userJobs.users']);

            $jobs->when(isset($request['q'], $request['q']['job_status']), function ($q) use ($request) {
                $q->where(function ($q) use ($request) {
                    $q->whereJobPostStatus($request['q']['job_status']);
                });
            });

            $paginate = $request->paginate ?? 10;
            $paginatedList = $jobs->orderBy('id', 'DESC')->paginate($paginate);
            $pagination = [
                "total" => $paginatedList->total(),
                "total_pages" => $paginatedList->lastPage(),
                "current_page" => $paginatedList->currentPage(),
                "next_page_url" => $paginatedList->nextPageUrl(),
                "previous_page_url" => $paginatedList->previousPageUrl(),
            ];


            $this->setMeta('message', __('messages.hospital.job.list_fetch'));
            $this->setMeta('status', AppConstant::STATUS_OK);
            $this->setPaginate($pagination);
            $this->setData('job', $paginatedList->getCollection());
            return response()->json($this->setResponse(), AppConstant::OK);
        } catch (QueryException|Exception $e) {
            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /*
        public function store(AddJobsRequest $request): JsonResponse
        {
            try {
                $hospital = auth()->user();
                dd($hospital->id);
                $validated = $request->validated();
                $basicJobDetails = [
                    'hospital_id' => $validated['hospital_id'],
                    'name' => $validated['name'],
                    'job_type' => $validated['job_type'],
                    'slots' => $validated['slots'],
                    'min_experience' => $validated['min_experience'],
                    'max_experience' => $validated['max_experience'],
                    'uuid' => Uuid::uuid4()->toString(),
                ];

                DB::beginTransaction();

                $jobs = Jobs::create($basicJobDetails);

                if (empty($jobs)) {
                    $this->setMeta('message', __('messages.jobNotFound'));
                    $this->setMeta('status', AppConstant::STATUS_FAIL);
                    return response()->json($this->setResponse(), Response::HTTP_UNPROCESSABLE_ENTITY);
                }

                collect($basicJobDetails)->each(function ($item, $key) use (&$validated) {
                    unset($validated[$key]);
                });

                $validated['job_id'] = $jobs->id;
                if ($request['job_type'] === Jobs::GENERAL_SURGERY) {
                    $jobsDetail = JobDetailsGeneralSurgeon::updateOrCreate(['job_id' => $jobs->id], $validated);
                } else {
                    $jobsDetail = JobDetailsHospitalist::updateOrCreate(['job_id' => $jobs->id], $validated);
                }
                DB::commit();

                $this->setMeta('message', __('messages.hospital.job.added'));
                $this->setMeta('status', AppConstant::STATUS_OK);
                $this->setData('job', $jobs);
                $this->setData('job_details', $jobsDetail);
                return response()->json($this->setResponse(), AppConstant::OK);
            } catch (QueryException|Exception $e) {
                DB::rollBack();
                $this->setMeta('message', $e->getMessage());
                $this->setMeta('status', AppConstant::STATUS_FAIL);
                return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
            }
        }*/

    public function JobUpdateOrCreate(AddJobsRequest $request): JsonResponse
    {
        try {
            $hospital = auth()->user();
            $jobsCount = Jobs::whereHospitalId($hospital->id)->count();

            $jobRequest = $request->only(
                'id',
                'hospital_id',
                'name',
                'job_type',
                'slots',
                'min_experience',
                'max_experience'
            );
            $jobId = $jobRequest['id'] ?? null;

            if ($hospital->is_free_user === 1 && $jobsCount >= Jobs::FREE_JOB_COUNT && is_null($jobId)) {
                $this->setMeta('message', __('messages.hospital.job.jobCanNotCreate'));
                $this->setMeta('status', AppConstant::STATUS_FAIL);
                return response()->json($this->setResponse(), Response::HTTP_UNPROCESSABLE_ENTITY);
            }

            DB::beginTransaction();
            $jobRequest['uuid'] = Uuid::uuid4()->toString();
            $jobs = Jobs::updateOrCreate(['id' => $jobId], $jobRequest);
            if (!$jobs) {
                $this->setMeta('message', __('messages.jobNotFound'));
                $this->setMeta('status', AppConstant::STATUS_FAIL);
                return response()->json($this->setResponse(), Response::HTTP_UNPROCESSABLE_ENTITY);
            }
            if ($request['job_type'] === Jobs::GENERAL_SURGERY) {
                $jobsDetail = JobDetailsGeneralSurgeon::updateOrCreate(['job_id' => $jobs->id], $request->only(
                    'job_id',
                    'is_verified_clinic',
                    'clinic_days_per_week',
                    'avg_no_of_patients_per_day',
                    'avg_no_of_surgeries_per_or_day',
                    'anesthesia_care',
                    'emr_used_in_clinic',
                    'upper_lower_endoscopy',
                    'trauma_center_level',
                    'avg_no_of_trauma_patients_per_month',
                    'c_section',
                    'dialysis_access',
                    'dialysis_access_value',
                    'state'
                ));
            } else {
                $jobsDetail = JobDetailsHospitalist::updateOrCreate(['job_id' => $jobs->id], $request->only(
                    'job_id',
                    'family_practice_physicians_apply',
                    'hospital_census',
                    'avg_admissions_per_provider_by_day',
                    'avg_er_admissions_per_provider_by_night',
                    'expected_patient_load',
                    'hospitalist_in_day',
                    'hospitalist_in_night',
                    'mid_levels_in_day',
                    'mid_levels_in_night',
                    'swing_shift_available',
                    'backup_plan',
                    'mid_level_available',
                    'mid_level_available_for_shift',
                    'runs_code',
                    'respiratory_therapist',
                    'respiratory_therapist_allowed_intubate',
                    'procedures_required',
                    'contract_period_type',
                    'flexible_shift_scheduling',
                    'state'
                ));
            }
            $this->setMeta('message', __('messages.hospital.job.added'));
            $this->setMeta('status', AppConstant::STATUS_OK);
            $this->setData('job', $jobs);
            $this->setData('job_details', $jobsDetail);
            DB::commit();
            return response()->json($this->setResponse(), AppConstant::OK);
        } catch (QueryException|Exception $e) {
            DB::rollBack();
            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function show($id): JsonResponse
    {
        try {
            $jobs = Jobs::with(['jobDetailsGeneralSurgeon', 'jobDetailsHospitalist', 'description', 'jobSchedules', 'jobPayment', 'userJobs.users'])->whereId($id)->first();
            if (!$jobs) {
                $this->setMeta('message', __('messages.hospital.job.jobNotFound'));
                $this->setMeta('status', AppConstant::STATUS_FAIL);
                return response()->json($this->setResponse(), AppConstant::UNPROCESSABLE_REQUEST);
            }
            if ($jobs->job_type === Jobs::GENERAL_SURGERY) {
                $jobs->unsetRelation('jobDetailsHospitalist');
            } else {
                $jobs->unsetRelation('jobDetailsGeneralSurgeon');
            }
            $this->setMeta('status', AppConstant::STATUS_OK);
            $this->setData('job', $jobs);
            return response()->json($this->setResponse(), AppConstant::OK);
        } catch (QueryException|Exception $e) {
            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @param UpdateJobsRequest $request
     * @param int $id
     * @return JsonResponse
     */
    /*    public function update(UpdateJobsRequest $request, int $id): JsonResponse
        {
            try {
                $validated = $request->validated();
                $needToSendNotification = $validated['sendNotification'] ?? null;
                unset($validated['sendNotification']);

                DB::beginTransaction();
                $jobs = Jobs::with(['jobDetailsGeneralSurgeon', 'jobDetailsHospitalist', 'description', 'jobSchedules', 'jobPayment'])->whereId($id)->first();
                if (empty($jobs)) {
                    $this->setMeta('message', __('messages.hospital.job.jobNotFound'));
                    $this->setMeta('status', AppConstant::STATUS_FAIL);
                    return response()->json($this->setResponse(), AppConstant::NOT_FOUND);
                }
                $basicJobDetails = [
                    'hospital_id' => $validated['hospital_id'],
                    'name' => $validated['name'],
                    'job_type' => $validated['job_type'],
                    'slots' => $validated['slots'],
                    'min_experience' => $validated['min_experience'],
                    'max_experience' => $validated['max_experience'],
                ];

                $jobs->update($basicJobDetails);

                collect($basicJobDetails)->each(function ($item, $key) use (&$validated) {
                    unset($validated[$key]);
                });

                if ($request->job_type === Jobs::GENERAL_SURGERY) {
                    if (!isset($jobs->jobDetailsGeneralSurgeon)) {
                        $this->setMeta('message', __('messages.hospital.job.generalSurgeryRecordNotFound'));
                        $this->setMeta('status', AppConstant::STATUS_FAIL);
                        return response()->json($this->setResponse(), AppConstant::NOT_FOUND);
                    }
                    $jobs->jobDetailsGeneralSurgeon->update($validated);
                } else {
                    if (!isset($jobs->jobDetailsHospitalist)) {
                        $this->setMeta('message', __('messages.hospital.job.hospitalistRecordNotFound'));
                        $this->setMeta('status', AppConstant::STATUS_FAIL);
                        return response()->json($this->setResponse(), AppConstant::NOT_FOUND);
                    }
                    $jobs->jobDetailsHospitalist->update($validated);
                }

                $updatedJobs = Jobs::with(['jobDetailsGeneralSurgeon', 'jobDetailsHospitalist', 'description', 'jobSchedules', 'jobPayment', 'userJobs.users'])->whereId($id);
                if ($needToSendNotification) {
                    $updatedJobs->first()->userJobs->map(function ($item) use ($updatedJobs) {
                        $name = $item->users->first_name . ' ' . $item->users->last_name;
                        Helper::sendNotificationToPhysician($item->users->id, $name, $item->jobs->name, AppConstant::JOB_UPDATED, $updatedJobs->first());
                    });
                }

                DB::commit();
                $updatedJobs = $updatedJobs->first();
                $this->setMeta('message', __('messages.hospital.job.updated'));
                $this->setMeta('status', AppConstant::STATUS_OK);
                $this->setData('job', $updatedJobs);
                return response()->json($this->setResponse(), AppConstant::OK);
            } catch (QueryException|Exception $e) {
                DB::rollBack();
                $this->setMeta('message', __('messages.somethingWrong'));
                $this->setMeta('status', AppConstant::STATUS_FAIL);
                return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
            }
        }*/

    public function getJobApplicants($jobId, Request $request): JsonResponse
    {
        try {
            $query = UserJobs::whereJobId($jobId)->with(['users', 'userScheduledInterviewDetail'])
                ->where('application_status', '!=', UserJobs::COMPLETED);
            $query->when(isset($request['q']['status']), function ($q) use ($request) {
                $q->where(function ($q) use ($request) {
                    $q->where('application_status', '=', $request['q']['status']);
                });
            });
            $paginatedList = $query->orderBy('id', 'DESC')->paginate($request->paginate ?? 10);
            $pagination = [
                "total" => $paginatedList->total(),
                "current_page" => $paginatedList->currentPage(),
                "next_page_url" => $paginatedList->nextPageUrl(),
                "previous_page_url" => $paginatedList->previousPageUrl(),
            ];
            $this->setMeta('status', AppConstant::STATUS_OK);
            $this->setData('user_jobs', $paginatedList->getCollection());
            $this->setPaginate($pagination);
            return response()->json($this->setResponse(), AppConstant::OK);
        } catch (QueryException|Exception $e) {
            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @param int $userId
     * @return JsonResponse
     */
    public function getApplicantsWorkHistory(int $userId): JsonResponse
    {
        try {
            $user = User::find($userId);
            if (!$user) {
                $this->setMeta('message', __('messages.userNotFound'));
                $this->setMeta('status', AppConstant::STATUS_FAIL);
                return response()->json($this->setResponse(), Response::HTTP_NOT_FOUND);
            }
            $jobs = Jobs::whereHospitalId(Auth::id())->with('userJobs.jobs')->get();
            $jobsArr = [];
            $jobs->each(function ($job) use ($userId, &$jobsArr) {
                $job->userJobs->map(function ($userJob) use ($userId, &$jobsArr) {
                    if ($userJob->user_id === $userId && $userJob->application_status === 'COMPLETED') {
                        $overTimeHours = 0;
                        $userJobSchedule = $userJob->jobs->jobSchedules->sortBy('start_date')->first();
                        $userJobScheduleEndDate = $userJob->jobs->jobSchedules->sortBy('end_date')->last();
                        $startDate = Carbon::parse($userJobSchedule->start_date);
                        $endDate = Carbon::parse($userJobScheduleEndDate->end_date);
                        $startTime = Carbon::parse($userJobSchedule->start_time);
                        $endTime = Carbon::parse($userJobScheduleEndDate->end_time);
                        $totalWorkedDays = $startDate->diffInDays($endDate);
                        $totalDiffInHours = (int)gmdate('H:i', $startTime->diffInSeconds($endTime));

                        if ($userJob->usersJobsDetails && $userJob->usersJobsDetails->want_to_add_overtime) {
                            $overTimeHours = $userJob->usersJobsDetails->overtime_hours;
                        }
                        $totalOverTimeDays = $totalDiffInHours > 0 ? round((int)$overTimeHours / $totalDiffInHours) : 0;
                        $returnArr['name'] = $userJob->jobs->name;
                        $returnArr['joining_date'] = $startDate;
                        $returnArr['ending_date'] = $endDate;
                        $returnArr['rating'] = $userJob->user_rating;
                        $returnArr['shift'] = $userJobSchedule->shift;
                        $jobsArr[] = $returnArr;
                    }
                });
            });

            $this->setMeta('status', AppConstant::STATUS_OK);
            $this->setData('applicant_work_history', $jobsArr);
            return response()->json($this->setResponse(), AppConstant::OK);
        } catch (QueryException|Exception $e) {
            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function getApplicantsProfile(int $userId)
    {
        try {
            $query = User::with([
                'personInfo',
                'educationalAndTraingDetails',
                'educationaRelatedSubDetails',
                'licensure',
                'malPractice',
                'disciplinaryAction',
                'referencesDetails',
                'boardCertification',
                'clinicalCertification',
                'licenseIssuerDetails',
                'generalSuregery',
                'hospitalist',
            ])->whereId($userId)->first();

            $this->setMeta('status', AppConstant::STATUS_OK);
            $this->setData('user_jobs', $query);
            return response()->json($this->setResponse(), AppConstant::OK);
        } catch (QueryException|Exception $e) {
            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function updateUserJobStatus(int $userJobId, UserJobsStatusUpdateRequest $request): JsonResponse
    {
        try {
            $validatedRequest = $request->validated();
            $userJob = UserJobs::whereId($userJobId)->first();
            if (!$userJob) {
                $this->setMeta('message', __('messages.hospital.job.jobNotFound'));
                $this->setMeta('status', AppConstant::STATUS_FAIL);
                return response()->json($this->setResponse(), AppConstant::NOT_FOUND);
            }
            $userJob->update($validatedRequest);
            $this->setMeta('status', AppConstant::STATUS_OK);
            $this->setMeta('message', __('messages.hospital.userJobs.statusUpdated'));
            $this->setData('user_job', $userJob);
            return response()->json($this->setResponse(), AppConstant::OK);
        } catch (QueryException|Exception $e) {
            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @param int $jobId
     * @param JobsStatusUpdateRequest $request
     * @return JsonResponse
     */
    public function updateJobCurrentStatus(int $jobId, JobsStatusUpdateRequest $request): JsonResponse
    {
        try {
            $validatedRequest = $request->validated();
            $job = Jobs::whereId($jobId)->first();
            if (!$job) {
                $this->setMeta('message', __('messages.hospital.job.jobNotFound'));
                $this->setMeta('status', AppConstant::STATUS_FAIL);
                return response()->json($this->setResponse(), AppConstant::NOT_FOUND);
            }
            if ($job->hospital_id !== Auth::id()) {
                $this->setMeta('message', __('messages.unauthorizedAccessToRecord'));
                $this->setMeta('status', AppConstant::STATUS_FAIL);
                return response()->json($this->setResponse(), AppConstant::UNPROCESSABLE_REQUEST);
            }
            if ($job->job_post_status === Jobs::COMPLETED) {
                $this->setMeta('message', __('messages.hospital.job.jobStatusUpdateAttemptedForCompleteJobs'));
                $this->setMeta('status', AppConstant::STATUS_FAIL);
                return response()->json($this->setResponse(), AppConstant::UNPROCESSABLE_REQUEST);
            }
            if ($validatedRequest['job_post_status'] === Jobs::CLOSED
                ||
                $validatedRequest['job_post_status'] === Jobs::COMPLETED) {
                if (!isset($job->description, $job->jobPayment) && empty($job->jobSchedules->toArray())) {
                    $this->setMeta('message', __('messages.hospital.jobs.recordNotFound'));
                    $this->setMeta('status', AppConstant::STATUS_FAIL);
                    return response()->json($this->setResponse(), Response::HTTP_FORBIDDEN);
                }
            }

            /*$isPaymentPending = false;
            if ($validatedRequest['job_post_status'] === Jobs::CLOSED) {
                $job->userJobs->each(function ($userJob) use (&$isPaymentPending) {
                    if ($userJob->usersJobsInvoice->invoice_status === "PENDING") {
                        $isPaymentPending = true;
                    }
                });
            }
            if($isPaymentPending){
                $this->setMeta('message', __('messages.hospital.job.jobStatusUpdateAttemptedForCompleteJobs'));
                $this->setMeta('status', AppConstant::STATUS_FAIL);
                return response()->json($this->setResponse(), AppConstant::UNPROCESSABLE_REQUEST);
            }*/
            $job->job_post_status = $validatedRequest['job_post_status'];
            $job->save();
            $this->setMeta('status', AppConstant::STATUS_OK);
            $this->setMeta('message', __('messages.hospital.job.jobStatusUpdated'));
            $this->setData('user_job', $job);
            return response()->json($this->setResponse(), AppConstant::OK);
        } catch (QueryException|Exception $e) {
            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function getTotalJobHours(int $jobId): JsonResponse
    {
        try {
            $job = Jobs::whereId($jobId)->first();
            $dateArr = [];
            $shiftArr = [];
            $totalHours = 0;
            $totalMinutes = 0;
            if (!$job) {
                $this->setMeta('message', __('messages.hospital.job.jobNotFound'));
                $this->setMeta('status', AppConstant::STATUS_FAIL);
                return response()->json($this->setResponse(), AppConstant::NOT_FOUND);
            }
            if ($job->hospital_id !== Auth::id()) {
                $this->setMeta('message', __('messages.unauthorizedAccessToRecord'));
                $this->setMeta('status', AppConstant::STATUS_FAIL);
                return response()->json($this->setResponse(), AppConstant::UNAUTHORIZED);
            }
            $jobScheduleFirstDate = $job->jobSchedules->sortBy('start_date');
            collect($jobScheduleFirstDate)->each(function ($item) use (&$dateArr, &$totalHours, &$shiftArr, &$totalMinutes) {
                if ($item->shift !== 'FLEXIBLE') {
                    if (!empty($dateArr)) {
                        $shouldCheck = false;
                        collect($dateArr)->each(function ($dateItem, $index) use ($item, &$shouldCheck, &$shiftArr) {
                            if ($dateItem[0] <= $item->start_date && $dateItem[1] >= $item->end_date && $shiftArr[$index] === $item->shift) {
                                $shouldCheck = false;
                            } else {
                                $shouldCheck = true;
                            }
                        });
                        if ($shouldCheck) {
                            $datetime1 = new DateTime($item->start_date . ' ' . $item->start_time);
                            $datetime2 = new DateTime($item->end_date . ' ' . $item->end_time);
                            $totalDiffDays = Carbon::createFromFormat('Y-m-d', $item->start_date)->diffInDays(Carbon::createFromFormat('Y-m-d', $item->end_date));

                            $interval = $datetime1->diff($datetime2);
//                            $totalDays = $interval->days + 1;
                            $totalDays = $totalDiffDays + 1;

                            $totalHours += $totalDays * $interval->h;
                            $totalMinutes += $totalDays * $interval->i;
                            if ($item->start_time === $item->end_time) {
                                $totalHours += $totalDays * 24;
                            }
                            $dateArr[] = [$item->start_date, $item->end_date];
                            $shiftArr[] = $item->shift;
                        }
                    }

                    if (empty($dateArr)) {
                        $datetime1 = new DateTime($item->start_date . ' ' . $item->start_time);
                        $datetime2 = new DateTime($item->end_date . ' ' . $item->end_time);
                        $totalDiffDays = Carbon::createFromFormat('Y-m-d', $item->start_date)->diffInDays(Carbon::createFromFormat('Y-m-d', $item->end_date));

                        $interval = $datetime1->diff($datetime2);
//                        $totalDays = $interval->days + 1;
                        $totalDays = $totalDiffDays + 1;

                        $totalHours += $totalDays * $interval->h;
                        $totalMinutes += $totalDays * $interval->i;
                        if ($item->start_time === $item->end_time) {
                            $totalHours += $totalDays * 24;
                        }

                        $dateArr[] = [$item->start_date, $item->end_date];
                        $shiftArr[] = $item->shift;
                    }
                } else {
                    $totalHours += 0;
                    $totalMinutes += 0;
                }
            });
            if ($totalMinutes > 59) {
                $convertedMinutes = Carbon::parse(date('H:i', mktime($totalMinutes / 60)));
                $totalHours += $convertedMinutes->hour;
                $totalMinutes = $convertedMinutes->minute;
            }
            $convertedMinutes = Carbon::parse(date('H:i', mktime($totalMinutes / 60)));
            $returnArr['hours'] = sprintf("%02d", $totalHours) . ':' . sprintf("%02d", $totalMinutes);
            $this->setMeta('status', AppConstant::STATUS_OK);
            $this->setMeta('message', __('messages.hospital.job.payment.scheduleHoursFetched'));
            $this->setData('total_hours', $returnArr);
            return response()->json($this->setResponse(), AppConstant::OK);
        } catch (QueryException|Exception $e) {
            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function activeInactive(int $jobId): JsonResponse
    {
        try {
            $job = Jobs::whereId($jobId)->first();

            if (!$job) {
                $this->setMeta('message', __('messages.hospital.job.jobNotFound'));
                $this->setMeta('status', AppConstant::STATUS_FAIL);
                return response()->json($this->setResponse(), AppConstant::NOT_FOUND);
            }
            if ($job->hospital_id !== Auth::id()) {
                $this->setMeta('message', __('messages.unauthorizedAccessToRecord'));
                $this->setMeta('status', AppConstant::STATUS_FAIL);
                return response()->json($this->setResponse(), AppConstant::UNAUTHORIZED);
            }

            if ($job->job_post_status === Jobs::INACTIVE) {
                $job->job_post_status = Jobs::ACTIVE;
            } else {
                $job->job_post_status = Jobs::INACTIVE;
            }
            $job->save();
            $this->setMeta('status', AppConstant::STATUS_OK);
            $this->setMeta('message', __('messages.hospital.job.jobStatusUpdated'));
            $this->setData('user_job', $job);
            return response()->json($this->setResponse(), AppConstant::OK);
        } catch (QueryException|Exception $e) {
            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function reOpenJob(int $jobId): JsonResponse
    {
        try {
            DB::beginTransaction();
            $job = Jobs::whereId($jobId)->first();

            if (!$job) {
                $this->setMeta('message', __('messages.hospital.job.jobNotFound'));
                $this->setMeta('status', AppConstant::STATUS_FAIL);
                return response()->json($this->setResponse(), AppConstant::NOT_FOUND);
            }
            if ($job->hospital_id !== Auth::id()) {
                $this->setMeta('message', __('messages.unauthorizedAccessToRecord'));
                $this->setMeta('status', AppConstant::STATUS_FAIL);
                return response()->json($this->setResponse(), AppConstant::UNAUTHORIZED);
            }
            if ($job->job_post_status !== Jobs::CLOSED && $job->job_post_status !== Jobs::EXPIRED) {
                $this->setMeta('message', "You cannot reopen jobs which is not filled/expired yet.");
                $this->setMeta('status', AppConstant::STATUS_FAIL);
                return response()->json($this->setResponse(), AppConstant::UNAUTHORIZED);
            }
            JobSchedule::whereJobId($jobId)->delete();
            $job->job_post_status = Jobs::ACTIVE;
            $job->save();
            DB::commit();
            $this->setMeta('status', AppConstant::STATUS_OK);
            $this->setMeta('message', __('messages.hospital.job.jobActive'));
            $this->setData('user_job', $job);
            return response()->json($this->setResponse(), AppConstant::OK);
        } catch (QueryException|Exception $e) {
            DB::rollBack();
            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
