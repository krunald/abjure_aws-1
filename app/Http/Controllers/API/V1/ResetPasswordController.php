<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use App\Models\PasswordReset;
use App\Models\Physician\Onboarding\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ResetPasswordController extends Controller
{
    public function index($token = null)
    {
        // Validate the token
        $tokenData = PasswordReset::where('token', $token)
            ->where('created_at','>',Carbon::now()->subMinutes(30))
            ->first();

        if(!$tokenData){
            $error['link_expire'] = 'Your reset password link is no longer available!';
            return view('errors.404')->with('error', $error);
        }

        $data['token'] = $token;
        $data['email'] = $_GET['email'];
        $data['id'] = $_GET['id'];

        return view('auth.passwords.reset')->with('data', $data);
    }
    public function resetPassword(Request $request){

        //Validate input
        $rules = [
            'email' => 'required|exists:users,email',
            'password' => 'required|confirmed'
        ];
        $messages = [
            'required' => 'The :attribute field is required.',
            'regex' => 'Passord må inneholde en kombinasjon av tall, små og store bokstaver og et spesialtegn.'
        ];

        $this->validate($request, $rules, $messages);
        $password = $request->password;

        // Validate the token
        $tokenData = PasswordReset::where('token', $request->token)->first();
        // Redirect the user back to the password reset request form if the token is invalid
        if (!$tokenData) return view('errors.404');
        $where = ['email' => $tokenData->email];
        $user = User::where($where)->first();

        // Redirect the user back if the email is invalid
        if (!$user) return redirect()->back()->withErrors(['email' => 'Email not found']);

        //Hash and update the new password
        $user->password = bcrypt($password);
        $user->update(); //or $user->save();
        //Delete the token
        PasswordReset::where('email', $user->email)->delete();
        return view('email.reset_password_success');
    }
    public function success(){
        return view('home');
    }
}
