<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use App\Traits\ApiResponse;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class HealthApiController extends Controller
{
    use ApiResponse;
    public function index(){
        $this->setMeta('status', JsonResponse::HTTP_OK);
        return response()->json($this->setResponse(), JsonResponse::HTTP_OK);
    }
}
