<?php

namespace App\Http\Requests\API\V1\Hospital\Template;

use App\Traits\ApiResponse;
use App\Utils\AppConstant;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\Response;

class UpdateTemplateRequest extends FormRequest
{
    use ApiResponse;

    protected const JOB_TYPE_ENUM = 'in:HOSPITALIST,GENERAL_SURGERY';

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        $arrayRule = [
            'template_name' => ['bail', 'required'],
            'job_title' => ['bail', 'required'],
            'job_type' => ['bail', 'required', 'in:GENERAL_SURGERY,HOSPITALIST'],
            'slots' => ['bail', 'required', 'numeric'],
            'min_experience' => ['bail', 'required', 'numeric', 'gte:0'],
            'max_experience' => ['bail', 'required', 'gte:min_experience'],
            'description' => ['bail', 'required'],
            'status' => ['boolean'],
        ];
        return array_merge($arrayRule, $this->jobTypeValidation());
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function jobTypeValidation(): array
    {
        switch ($this->request->get('job_type')) {
            case 'GENERAL_SURGERY':
                return [
                    'is_verified_clinic' => ['bail', 'boolean'],
                    'clinic_days_per_week' => ['bail', 'required_if:verified_clinic,1,true', 'numeric', 'between:1,7'],
                    'avg_no_of_patients_per_day' => ['bail', 'required_if:verified_clinic,1,true', 'numeric'],
                    'emr_used_in_clinic' => ['bail', 'required_if:verified_clinic,1,true'],
                    'avg_no_of_surgeries_per_or_day' => ['bail', 'numeric'],
                    'upper_lower_endoscopy' => ['bail', 'boolean'],
                    'anesthesia_care' => ['bail', 'in:ANESTHESIOLOGIST,CRNA,BOTH'],
                    'trauma_center_level' => ['bail', 'in:1,2,3,4,5'],
                    'avg_no_of_trauma_patients_per_month' => ['bail', 'numeric'],
                    'c_section' => ['bail', 'boolean'],
                    'dialysis_access' => ['bail', 'boolean'],
                    'dialysis_access_value' => ['bail', 'required_if:dialysis_access_required,1,true', 'in:CATHETER_ONLY,CATHETER,AV_GRAFT/FISTULA,BOTH'],
                ];
                break;
            case 'HOSPITALIST':
                return [
                    'family_practice_physicians_apply' => ['bail', 'required', 'bool'],
                    'hospital_census' => ['bail', 'required', 'numeric'],
                    'avg_admissions_per_provider_by_day' => ['bail', 'required', 'numeric'],
                    'avg_er_admissions_per_provider_by_night' => ['bail', 'required', 'numeric'],
                    'expected_patient_load' => ['bail', 'required', 'numeric'],
                    'hospitalist_in_day' => ['bail', 'required', 'numeric'],
                    'hospitalist_in_night' => ['bail', 'required', 'numeric'],
                    'mid_levels_in_day' => ['bail', 'required', 'numeric'],
                    'mid_levels_in_night' => ['bail', 'required', 'numeric'],
                    'swing_shift_available' => ['bail', 'required', 'bool'],
                    'backup_plan' => ['bail', 'in:NO_MORE_ADMISSION_ALLOWED,BACKUP_PROVIDER_AVAILABLE,POIPLFAC,PREAPLAC,PRTIPLWC'],
                    'mid_level_available' => ['bail', 'required', 'bool'],
                    'mid_level_available_for_shift' => ['bail', 'required_if:mid_level_available,1,true', 'in:DAY,NIGHT,BOTH'],
                    'runs_code' => ['bail', 'in:ER_PHYSICIAN,INTENSIVIST,HOSPITALIST'],
                    'respiratory_therapist' => ['bail', 'required', 'bool'],
                    'respiratory_therapist_allowed_intubate' => ['bail', 'required_if:respiratory_therapist,1,true', 'boolean'],
                    'procedures_required' => ['bail', 'required'],
                    'contract_period_type' => ['bail', 'required', 'in:SHORT_TERM,LONG_TERM'],
                    'flexible_shift_scheduling' => ['bail', 'required', 'bool'],
                ];
                break;
            default:
                return [
                    'job_typ1e' => ['bail', self::JOB_TYPE_ENUM],
                ];
                break;
        }
    }

    /**
     * @param Validator $validator
     * @return void
     * @throws ValidationException
     */
    protected function failedValidation(Validator $validator)
    {
        $this->setMeta('message', $validator->messages()->first());
        $this->setMeta('status', AppConstant::STATUS_FAIL);
        $response = new JsonResponse($this->setResponse(), Response::HTTP_UNPROCESSABLE_ENTITY);
        throw (new ValidationException($validator, $response))->status(Response::HTTP_UNPROCESSABLE_ENTITY);
    }
}
