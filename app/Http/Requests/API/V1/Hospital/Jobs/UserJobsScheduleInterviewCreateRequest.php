<?php

namespace App\Http\Requests\API\V1\Hospital\Jobs;

use App\Traits\ApiResponse;
use App\Utils\AppConstant;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\Response;

class UserJobsScheduleInterviewCreateRequest extends FormRequest
{
    use ApiResponse;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_job_id' => ['bail', 'required', 'exists:user_jobs,id', 'unique:user_jobs_schedule_interviews,user_job_id'],
            'user_id' => ['bail', 'required', 'exists:users,id'],
            'interview_date' => ['required', 'date', 'after:today'],
            'interview_time' => ['required'],
            'interview_type' => ['required', 'in:PORTAL,OTHER'],
            'other_interview_type' => ['required_if:interview_type,OTHER', 'in:AUDIO_CALL,VIDEO_CALL,IN_PERSON'],
            'video_call_link' => ['required_if:interview_type,VIDEO_CALL']
        ];
    }

    public function messages()
    {
        return [
            'user_job_id.unique' => 'Interview for this applicant is already scheduled',
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        $this->setMeta('message', $validator->messages()->first());
        $this->setMeta('status', AppConstant::STATUS_FAIL);
        $response = new JsonResponse($this->setResponse(), Response::HTTP_UNPROCESSABLE_ENTITY);
        throw (new ValidationException($validator, $response))->status(Response::HTTP_UNPROCESSABLE_ENTITY);
    }
}
