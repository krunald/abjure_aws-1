<?php

namespace App\Http\Requests\API\V1\Hospital\Jobs;

use App\Traits\ApiResponse;
use App\Utils\AppConstant;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\Response;

class AddJobsSchedule extends FormRequest
{
    use ApiResponse;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'job_id' => ['required', 'exists:jobs,id'],
            'job_schedule.*.start_date' => ['required', 'date'],
            'job_schedule.*.end_date' => ['required', 'date', 'after_or_equal:job_schedule.*.start_date'],
            'job_schedule.*.shift' => ['required', 'in:DAY,NIGHT,BOTH,FLEXIBLE'],
            'job_schedule.*.start_time' => ['required_if:job_schedule.*.shift,DAY', 'required_if:job_schedule.*.shift,NIGHT', 'required_if:job_schedule.*.shift,BOTH'],
            'job_schedule.*.end_time' => ['required_if:job_schedule.*.shift,DAY', 'required_if:job_schedule.*.shift,NIGHT', 'required_if:job_schedule.*.shift,BOTH'],
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        $this->setMeta('message', $validator->messages()->first());
        $this->setMeta('status', AppConstant::STATUS_FAIL);
        $response = new JsonResponse($this->setResponse(), Response::HTTP_UNPROCESSABLE_ENTITY);
        throw (new ValidationException($validator, $response))->status(Response::HTTP_UNPROCESSABLE_ENTITY);
    }
}
