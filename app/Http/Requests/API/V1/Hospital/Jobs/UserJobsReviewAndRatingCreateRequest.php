<?php

namespace App\Http\Requests\API\V1\Hospital\Jobs;

use App\Traits\ApiResponse;
use App\Utils\AppConstant;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\Response;

class UserJobsReviewAndRatingCreateRequest extends FormRequest
{
    use ApiResponse;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_jobs_id' => ['bail', 'required', 'exists:user_jobs,id', 'unique:user_job_review_and_ratings,user_jobs_id'],
            'rating' => ['bail', 'required'],
            'experience_with_doctor' => ['bail', 'nullable'],
            'quality_service_by_doctor' => ['bail', 'nullable', 'in:EXCELLENT,VERY_GOOD,SATISFACTORY,BELOW_EXPECTATION,POOR'],
            'doctors_availability' => ['bail', 'nullable'],
            'likely_to_recommend_doctor' => ['bail', 'nullable', 'in:HIGLY_RECOMMEND,RECOMMEND,NEUTRAL,WILL_NOT_RECOMMEND'],
        ];
    }

    public function messages()
    {
        return [
            'user_jobs_id.unique' => 'Feedback for this user is already added',
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        $this->setMeta('message', $validator->messages()->first());
        $this->setMeta('status', AppConstant::STATUS_FAIL);
        $response = new JsonResponse($this->setResponse(), Response::HTTP_UNPROCESSABLE_ENTITY);
        throw (new ValidationException($validator, $response))->status(Response::HTTP_UNPROCESSABLE_ENTITY);
    }
}
