<?php

namespace App\Http\Requests\API\V1\Hospital\Jobs;

use App\Traits\ApiResponse;
use App\Utils\AppConstant;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\Response;

class AddJobsRequest extends FormRequest
{
    use ApiResponse;

    public const GENERAL_SURGERY = 'GENERAL_SURGERY';
    public const HOSPITALIST = 'HOSPITALIST';
    protected const JOB_TYPE_ENUM = 'in:' . self::HOSPITALIST . ',' . self::GENERAL_SURGERY;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $arrayRule = [
            'hospital_id' => ['bail', 'required', 'exists:hospital,id'],
            'name' => ['required'],
            'job_type' => ['bail', 'required', self::JOB_TYPE_ENUM],
            'slots' => ['bail', 'required', 'numeric'],
            'min_experience' => ['bail', 'required', 'numeric', 'gte:0', 'lt:45'],
            'max_experience' => ['bail', 'gt:min_experience', 'lt:45'],
        ];
        return array_merge($arrayRule, $this->jobTypeValidation());
    }

    public function jobTypeValidation()
    {
        switch ($this->request->get('job_type')) {
            case self::HOSPITALIST:
                return [
                    'family_practice_physicians_apply' => ['bail', 'required', 'bool'],
                    'hospital_census' => ['bail', 'required', 'numeric'],
                    'avg_admissions_per_provider_by_day' => ['bail', 'required', 'numeric'],
                    'avg_er_admissions_per_provider_by_night' => ['bail', 'required', 'numeric'],
                    'expected_patient_load' => ['bail', 'required', 'numeric'],
                    'hospitalist_in_day' => ['bail', 'required', 'numeric'],
                    'hospitalist_in_night' => ['bail', 'required', 'numeric'],
                    'mid_levels_in_day' => ['bail', 'required', 'numeric'],
                    'mid_levels_in_night' => ['bail', 'required', 'numeric'],
                    'swing_shift_available' => ['bail', 'required', 'bool'],
                    'backup_plan' => ['bail', 'required', 'in:NO_MORE_ADMISSION_ALLOWED,BACKUP_PROVIDER_AVAILABLE,POIPLFAC,PREAPLAC,PRTIPLWC'],
                    'mid_level_available' => ['bail', 'required', 'bool'],
                    'mid_level_available_for_shift' => ['bail', 'required_if:mid_level_available,1,true', 'in:DAY,NIGHT,BOTH'],
                    'runs_code' => ['bail', 'required', 'in:ER_PHYSICIAN,INTENSIVIST,HOSPITALIST'],
                    'respiratory_therapist' => ['bail', 'required', 'bool'],
                    'respiratory_therapist_allowed_intubate' => ['bail', 'required_if:respiratory_therapist,true', 'bool'],
                    'procedures_required' => ['bail', 'required'],
                    'contract_period_type' => ['bail', 'required', 'in:SHORT_TERM,LONG_TERM'],
                    'flexible_shift_scheduling' => ['bail', 'required', 'bool'],
                ];
                break;
            case self::GENERAL_SURGERY:
                return [
                    'name' => ['required'],
                    'job_type' => ['bail', 'required', self::JOB_TYPE_ENUM],
                    'slots' => ['bail', 'required', 'numeric'],
                    'is_verified_clinic' => ['required', 'bool'],
                    'clinic_days_per_week' => ['bail', 'required_if:is_verified_clinic,1,true', 'numeric', 'between:1,7'],
                    'avg_no_of_patients_per_day' => ['bail', 'required_if:is_verified_clinic,1,true', 'numeric'],
                    'emr_used_in_clinic' => ['bail', 'required_if:is_verified_clinic,1,true'],
                    'avg_no_of_surgeries_per_or_day' => ['bail', 'required', 'numeric'],
                    'anesthesia_care' => ['bail', 'required', 'in:CRNA,ANESTHESIOLOGIST,BOTH'],
                    'upper_lower_endoscopy' => ['bail', 'required', 'bool'],
                    'trauma_center_level' => ['bail', 'required', 'in:1,2,3,4,5'],
                    'avg_no_of_trauma_patients_per_month' => ['bail', 'required', 'numeric'],
                    'c_section' => ['bail', 'required', 'bool'],
                    'dialysis_access' => ['bail', 'required', 'bool'],
                    'dialysis_access_value' => ['bail', 'required_if:dialysis_access_required,1,true', 'in:CATHETER_ONLY,CATHETER,AV_GRAFT/FISTULA,BOTH'],
                ];
                break;
            default:
                return [
                    'job_type' => ['bail', 'required', self::JOB_TYPE_ENUM],
                ];
                break;
        }
    }

    protected function failedValidation(Validator $validator)
    {
        $this->setMeta('message', $validator->messages()->first());
        $this->setMeta('status', AppConstant::STATUS_FAIL);
        $response = new JsonResponse($this->setResponse(), Response::HTTP_UNPROCESSABLE_ENTITY);
        throw (new ValidationException($validator, $response))->status(Response::HTTP_UNPROCESSABLE_ENTITY);
    }
}
