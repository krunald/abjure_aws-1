<?php

namespace App\Http\Requests\API\V1\Hospital\Onboarding;

use App\Traits\ApiResponse;
use App\Utils\AppConstant;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Request;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\Response;
use Tymon\JWTAuth\Facades\JWTAuth;

class UpdateHospitalRequest extends FormRequest
{
    use ApiResponse;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules(): array
    {
        $token = str_replace('Bearer ', '', Request::header('Authorization'));
        $token_data = JWTAuth::getPayload($token)->toArray();

        return [
            'name' => ['regex:/^[\pL\s]+$/u'],
            'licence_number' => ['regex:/^[\w]*$/', Rule::unique("hospital")->ignore($token_data["hospital_id"])],
            'address' => ['regex:/([- ,\/0-9a-zA-Z]+)/'],
            'contact_person' => ['regex:/^[\pL\s]+$/u'],
            'contact_person_number' => ['min:6', 'regex:/^([0-9\s\-\+\(\)]*)$/'],
            'contact_number' => ['min:6', 'regex:/^([0-9\s\-\+\(\)]*)$/'],
            'status' => ['boolean']
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        $this->setMeta('message', $validator->messages()->first());
        $this->setMeta('status', AppConstant::STATUS_FAIL);
        $response = new JsonResponse($this->setResponse(), Response::HTTP_UNPROCESSABLE_ENTITY);
        throw (new ValidationException($validator, $response))->status(Response::HTTP_UNPROCESSABLE_ENTITY);
    }
}
