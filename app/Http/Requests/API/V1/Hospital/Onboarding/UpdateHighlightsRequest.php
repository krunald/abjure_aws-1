<?php

namespace App\Http\Requests\API\V1\Hospital\Onboarding;

use App\Traits\ApiResponse;
use App\Utils\AppConstant;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\Response;

class UpdateHighlightsRequest extends FormRequest
{
    use ApiResponse;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'hospital_id' => ['bail', 'numeric'],
            'number_of_beds' => ['bail', 'integer'],
            'icu_beds' => ['bail', 'integer'],
            'emr_used_for' => ['bail'],
            'annual_er_visits' => ['bail', 'integer'],
            'paging_system' => ['bail', 'in:IN_HOUSE_MESSAGING_APPLICATION,PHYSICIAN_CELL_PHONE,PAGER'],
            'icu_status' => ['bail', 'in:OPEN,CLOSED'],
            'has_stroke_center' => ['bail', 'boolean'],
            'pci_available' => ['bail', 'boolean'],
            'trauma_center_level' => ['bail', 'numeric'],
            'robotic_surgery_platform_available' => ['bail'],
            'hemodialysis' => ['bail', 'boolean'],
            'specialist_available' => ['bail','array', 'min:1'],
            'consultant_accessibility_after_working_hours' => ['bail', 'in:EXCELLENT,GOOD,SATISFACTORY,BELOW_EXPECTATIONS'],
            'credentialing_length' => ['bail', 'numeric'],
            'credentialing_available' => ['bail', 'boolean'],
            'time_taken_for_privileges' => ['numeric'],
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        $this->setMeta('message', $validator->messages()->first());
        $this->setMeta('status', AppConstant::STATUS_FAIL);
        $response = new JsonResponse($this->setResponse(), Response::HTTP_UNPROCESSABLE_ENTITY);
        throw (new ValidationException($validator, $response))->status(Response::HTTP_UNPROCESSABLE_ENTITY);
    }
}
