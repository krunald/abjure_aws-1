<?php

namespace App\Http\Requests\API\V1\Hospital\Onboarding;

use App\Traits\ApiResponse;
use App\Utils\AppConstant;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\Response;

class HospitalOnboardingRequest extends FormRequest
{
    use ApiResponse;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'name' => ['required', 'regex:/^[\pL\s]+$/u'],
            'email' => ['required', 'email:rfc,dns', 'unique:hospital,email', 'unique:users,email'],
            'licence_number' => ['required', 'regex:/^[\w]*$/', 'unique:hospital,licence_number'],
            'address' => ['required', 'regex:/([- ,\/0-9a-zA-Z]+)/'],
            'contact_person' => ['required', 'regex:/^[\pL\s]+$/u'],
            'contact_person_number' => ['required','min:6', 'regex:/^([0-9\s\-\+\(\)]*)$/'],
            'contact_number' => ['required','min:6', 'regex:/^([0-9\s\-\+\(\)]*)$/'],
            'password' => ['required', 'min:6', 'max:30', 'regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$/', 'confirmed'],
            'status' => ['boolean'],
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        $this->setMeta('message', $validator->messages()->first());
        $this->setMeta('status', AppConstant::STATUS_FAIL);
        $response = new JsonResponse($this->setResponse(), Response::HTTP_UNPROCESSABLE_ENTITY);
        throw (new ValidationException($validator, $response))->status(Response::HTTP_UNPROCESSABLE_ENTITY);
    }
}
