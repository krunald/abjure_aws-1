<?php

namespace App\Http\Requests\API\V1\Hospital\Onboarding;

use App\Traits\ApiResponse;
use App\Utils\AppConstant;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\Response;

class HospitalHighlightsRequest extends FormRequest
{
    use ApiResponse;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'number_of_beds' => ['bail', 'required', 'integer'],
            'icu_beds' => ['bail', 'required', 'integer'],
            'emr_used_for' => ['bail', 'required'],
            'annual_er_visits' => ['bail', 'required', 'integer'],
            'paging_system' => ['bail', 'required', 'in:IN_HOUSE_MESSAGING_APPLICATION,PHYSICIAN_CELL_PHONE,PAGER'],
            'icu_status' => ['bail', 'required', 'in:OPEN,CLOSED'],
            'has_stroke_center' => ['bail', 'required', 'boolean'],
            'pci_available' => ['bail', 'required', 'boolean'],
            'trauma_center_level' => ['bail', 'required', 'numeric'],
            'robotic_surgery_platform_available' => ['bail', 'required'],
            'hemodialysis' => ['bail', 'required', 'boolean'],
            'specialist_available' => ['bail', 'required', 'present','array', 'min:1'],
            'consultant_accessibility_after_working_hours' => ['bail', 'required', 'in:EXCELLENT,GOOD,SATISFACTORY,BELOW_EXPECTATIONS'],
            'credentialing_length' => ['bail', 'required'],
            'credentialing_available' => ['bail', 'required', 'boolean'],
            'time_taken_for_privileges' => ['bail', 'required_if:credentialing_available,1,true'],

        ];
    }

    protected function failedValidation(Validator $validator)
    {
        $this->setMeta('message', $validator->messages()->first());
        $this->setMeta('status', AppConstant::STATUS_FAIL);
        $response = new JsonResponse($this->setResponse(), Response::HTTP_UNPROCESSABLE_ENTITY);
        throw (new ValidationException($validator, $response))->status(Response::HTTP_UNPROCESSABLE_ENTITY);
    }
}
