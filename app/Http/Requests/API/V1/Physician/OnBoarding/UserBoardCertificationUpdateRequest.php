<?php

namespace App\Http\Requests\API\V1\Physician\OnBoarding;

use App\Traits\ApiResponse;
use App\Utils\AppConstant;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\Response;


class UserBoardCertificationUpdateRequest extends FormRequest
{
    protected const REQUIRED_IF_ARE_YOU_CERTIFIED_TRUE = ['nullable', 'required_if:are_you_board_certified,true'];
    protected const REQUIRED_IF_ARE_YOU_CERTIFIED_TRUE_WITH_DATE = ['nullable', 'required_if:are_you_board_certified,true', 'date'];

    use ApiResponse;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'are_you_board_certified' => ['required', 'bool'],
            'issuer_name' => self::REQUIRED_IF_ARE_YOU_CERTIFIED_TRUE,
            'speciality' => self::REQUIRED_IF_ARE_YOU_CERTIFIED_TRUE,
            'date_certified' => self::REQUIRED_IF_ARE_YOU_CERTIFIED_TRUE_WITH_DATE,
            'date_recertified' => ['nullable', 'date'],
            'expiration_date' => ['nullable', 'date', 'after:today'],
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        $this->setMeta('message', $validator->messages()->first());
        $this->setMeta('status', AppConstant::STATUS_FAIL);
        $response = new JsonResponse($this->setResponse(), Response::HTTP_UNPROCESSABLE_ENTITY);
        throw (new ValidationException($validator, $response))->status(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

}
