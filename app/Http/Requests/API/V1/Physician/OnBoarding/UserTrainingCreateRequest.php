<?php

namespace App\Http\Requests\API\V1\Physician\OnBoarding;

use App\Traits\ApiResponse;
use App\Utils\AppConstant;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\Response;

class UserTrainingCreateRequest extends FormRequest
{
    use ApiResponse;

    protected const REQUIRED_IF_TYPE_TRAINING = 'required_if:training.*.type,TRAINING';

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id' => ['bail', 'required', 'exists:users,id'],
            'training.*.type' => 'required',
            'training.*.training_specialty' => [self::REQUIRED_IF_TYPE_TRAINING, 'exists:specialities,id'],
            'training.*.name' => ['nullable', self::REQUIRED_IF_TYPE_TRAINING],
            'training.*.address' => ['nullable', self::REQUIRED_IF_TYPE_TRAINING],
            'training.*.start_date' => ['nullable', self::REQUIRED_IF_TYPE_TRAINING, 'date'],
            'training.*.end_date' => ['nullable', self::REQUIRED_IF_TYPE_TRAINING, 'date', 'after:training.*.start_date'],
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        $this->setMeta('message', $validator->messages()->first());
        $this->setMeta('status', AppConstant::STATUS_FAIL);
        $response = new JsonResponse($this->setResponse(), Response::HTTP_UNPROCESSABLE_ENTITY);
        throw (new ValidationException($validator, $response))->status(Response::HTTP_UNPROCESSABLE_ENTITY);
    }
}
