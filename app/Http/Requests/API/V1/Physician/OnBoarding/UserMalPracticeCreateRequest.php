<?php

namespace App\Http\Requests\API\V1\Physician\OnBoarding;

use App\Traits\ApiResponse;
use App\Utils\AppConstant;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\Response;

class UserMalPracticeCreateRequest extends FormRequest
{
    use ApiResponse;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->request->get('claim_suits_settlements_raised') === true || $this->request->get('tofile_or_currently_pending_suits_claims') === true) {
            $requiredOrNot = ['required'];
        } else {
            $requiredOrNot = ['nullable'];
        }
        return [
            'user_id' => ['required', 'exists:users,id'],
            'mal_practice.*.ever_denied_professional_liability_insurance' => ['required', 'bool'],
            'mal_practice.*.claim_suits_settlements_raised' => ['required', 'bool'],
            'mal_practice.*.tofile_or_currently_pending_suits_claims' => ['required', 'bool', 'required_if:claim_suits_settlements_raised,true'],
            'mal_practice.*.date_of_incident' => array_merge($requiredOrNot, ['date']),
            'mal_practice.*.location' => $requiredOrNot,
            'mal_practice.*.allegation' => $requiredOrNot,
            'mal_practice.*.allegation_status' => array_merge($requiredOrNot, ['in:DISMISSED,SETTLED,JUDGEMENT,PENDING']),
            'mal_practice.*.settlement_amount' => ['nullable'],
        ];
    }

    public function messages()
    {
        return [
            'required' => ':attribute is required, please fill the field.',
            'mal_practice.*' => [
                'required' => ':attribute is required, please fill the field.',
                'bool' => ':attribute needs to select any one, please fill the appropriate detail.',
            ]
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        $this->setMeta('message', $validator->messages()->first());
        $this->setMeta('status', AppConstant::STATUS_FAIL);
        $response = new JsonResponse($this->setResponse(), Response::HTTP_UNPROCESSABLE_ENTITY);
        throw (new ValidationException($validator, $response))->status(Response::HTTP_UNPROCESSABLE_ENTITY);
    }
}
