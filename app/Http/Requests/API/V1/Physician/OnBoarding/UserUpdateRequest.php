<?php

namespace App\Http\Requests\API\V1\Physician\OnBoarding;

use App\Traits\ApiResponse;
use App\Utils\AppConstant;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\Response;

class UserUpdateRequest extends FormRequest
{
    use ApiResponse;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => ['required'],
            'last_name' => ['required'],
            'profile_pic' => ['nullable', 'image', 'mimes:jpg,png,jpeg,gif,svg'],
            'middle_name' => ['nullable'],
            'suffix' => ['nullable'],
            'title' => ['bail', 'required', 'in:MD,DO'],
            'expertise' => ['bail', 'required', 'in:HOSPITALIST,GENERAL_SURGERY'],
            'user_id' => ['exists:user_personal_infos,user_id'],
            'address' => ['required'],
            'npi' => ['required'],
            'citizen' => ['bail', 'required', 'in:US_CITIZEN,PERMANENT_RESIDENT,J1_VISA'],
            'dob' => ['bail', 'required', 'date', 'before:today'],
            'contact_no' => ['bail', 'required', 'regex:/^([0-9\s\-\+\(\)]*)$/', 'min:6'],
            'experience_month' => ['bail', 'required', 'between:0,12'],
            'experience_year' => ['bail', 'required', 'int'],
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        $this->setMeta('message', $validator->messages()->first());
        $this->setMeta('status', AppConstant::STATUS_FAIL);
        $response = new JsonResponse($this->setResponse(), Response::HTTP_UNPROCESSABLE_ENTITY);
        throw (new ValidationException($validator, $response))->status(Response::HTTP_UNPROCESSABLE_ENTITY);
    }
}

