<?php

namespace App\Http\Requests\API\V1\Physician\OnBoarding;

use App\Traits\ApiResponse;
use App\Utils\AppConstant;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\Response;

class UserCertificationAndLicensurUpdateRequest extends FormRequest
{

    use ApiResponse;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id' => 'required|exists:users,id|exists:user_board_certification_details,user_id|exists:user_clinical_certification_details,user_id|exists:user_license_issuer_states,user_id',
            'are_you_board_certified'=> 'required|bool',
            'issuer_name'=> 'required_if:are_you_board_certified,true',
            'speciality'=> 'required_if:are_you_board_certified,true',
            'date_certified'=> 'required_if:are_you_board_certified,true|date',
            'date_recertified'=> 'required_if:are_you_board_certified,true|date',
            'expiration_date'=> 'required_if:are_you_board_certified,true|date',
            'bls_certification' => 'required',
            'bls_certification_expiration' => 'required_if:bls_certification,true|date',
            'acls_certification' => 'required',
            'acls_certification_expiration' => 'required_if:acls_certification,true|date',
            'atls_certification' => 'required',
            'atls_certification_expiration' => 'required_if:atls_certification,true|date',
            'pals_certification' => 'required',
            'pals_certification_expiration' => 'required_if:pals_certification,true|date',
            'licensure.*.license_state'=> 'required',
            'licensure.*.license_number'=> 'required',
            'licensure.*.license_status'=> 'required|bool',
            'licensure.*.license_expiration_date'=> 'required|date',
        ];
    }
    public function messages()
    {
        return ['user_id.exists'=> 'Record with this user not found please check the details properly.'];
    }

    protected function failedValidation(Validator $validator)
    {
        $this->setMeta('message', $validator->messages()->first());
        $this->setMeta('status', AppConstant::STATUS_FAIL);
        $response = new JsonResponse($this->setResponse(), Response::HTTP_UNPROCESSABLE_ENTITY);
        throw (new ValidationException($validator, $response))->status(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

}
