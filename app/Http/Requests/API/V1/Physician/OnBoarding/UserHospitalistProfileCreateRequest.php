<?php

namespace App\Http\Requests\API\V1\Physician\OnBoarding;

use App\Traits\ApiResponse;
use App\Utils\AppConstant;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\Response;

class UserHospitalistProfileCreateRequest extends FormRequest
{
    use ApiResponse;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->request->get('max_desired_patient_load_flexible') !== null) {
            $requiredOrNot = ['required', 'bool'];
        } else {
            $requiredOrNot = ['nullable', 'bool'];
        }
        if ($this->request->get('max_desired_patient_load_numbers') !== null) {
            $patientLoadNumbersRequiredOrNot = ['required', 'numeric'];
        } else {
            $patientLoadNumbersRequiredOrNot = ['nullable', 'numeric'];
        }
        return [
            'icu_management' => ['required', 'bool'],
            'procedures' => ['required'],
            'max_desired_patient_load_flexible' => $requiredOrNot,
            'max_desired_patient_load_numbers' => $patientLoadNumbersRequiredOrNot,
            'high_volume_care_part_of_practice' => ['required'],
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        $this->setMeta('message', $validator->messages()->first());
        $this->setMeta('status', AppConstant::STATUS_FAIL);
        $response = new JsonResponse($this->setResponse(), Response::HTTP_UNPROCESSABLE_ENTITY);
        throw (new ValidationException($validator, $response))->status(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

}
