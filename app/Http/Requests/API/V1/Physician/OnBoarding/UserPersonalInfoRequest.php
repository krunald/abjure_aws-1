<?php

namespace App\Http\Requests\API\V1\Physician\OnBoarding;

use App\Traits\ApiResponse;
use App\Utils\AppConstant;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\Response;

class UserPersonalInfoRequest extends FormRequest
{
    use ApiResponse;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
//            'user_id' => ['required','exists:users,id','unique:user_personal_infos,user_id'],
            'address' => ['required'],
            'citizen' => ['required', 'in:US_CITIZEN,OTHER,GREEN_CARD,H-1B'],
            'npi' => ['required', 'digits:10', 'unique:user_personal_infos,npi'],
            'dob' => ['required', 'date', 'before:-18 years'],
            'contact_no' => ['required', 'min:6','max:15', 'regex:/^([0-9\s\-\+\(\)]*)$/'],
            'formatted_contact_no' => ['nullable', 'min:6','max:15', 'regex:/^\(?\d{3}\)?[- ]?\d{3}[- ]?\d{0,9}$/'],
            'experience_month' => ['required', 'min:0', 'max:11'],
            'experience_year' => ['required', 'numeric'],
        ];
    }

    public function messages()
    {
        return [
            'unique' => 'You cannot use same :attribute for adding another Personal Info.',
            'required' => ':attribute is required please fill :attribute field.',
            'experience_month.between' => ':attribute must be in between on 1 months to 12 months.',
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        $this->setMeta('message', $validator->messages()->first());
        $this->setMeta('status', AppConstant::STATUS_FAIL);
        $response = new JsonResponse($this->setResponse(), Response::HTTP_UNPROCESSABLE_ENTITY);
        throw (new ValidationException($validator, $response))->status(Response::HTTP_UNPROCESSABLE_ENTITY);
    }
}
