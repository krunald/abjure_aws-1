<?php

namespace App\Http\Requests\API\V1\Physician\OnBoarding;

use App\Traits\ApiResponse;
use App\Utils\AppConstant;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\Response;

class UserDisciplinaryActionUpdateRequest extends FormRequest
{
    use ApiResponse;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'ever_convicted' => ['required'],
            'ever_convicted_explanation' => ['required_if:ever_convicted,true'],
            'ever_denied_clinical_privileges' => 'required',
            'ever_denied_clinical_privileges_explanation' => 'required_if:ever_denied_clinical_privileges,true',
            'ever_denied_professional_liability_insurance' => 'required',
            'ever_denied_professional_liability_insurance_explanation' => 'required_if:ever_denied_professional_liability_insurance,true',
            'ever_denied_or_suspended_license_in_any_jurisdiction' => 'required',
            'ever_denied_or_suspended_license_in_any_jurisdiction_explanation' => 'required_if:ever_denied_or_suspended_license_in_any_jurisdiction,true',
            'ever_denied_or_suspended_participation' => 'required',
            'ever_denied_or_suspended_participation_explanation' => 'required_if:ever_denied_or_suspended_participation,true',
            'ever_treated_alcoholism_and_substance_abuse' => 'required',
            'ever_treated_alcoholism_and_substance_abuse_explanation' => 'required_if:ever_treated_alcoholism_and_substance_abuse,true',
            'ever_been_advised_to_seek_treatment' => 'required',
            'ever_been_advised_to_seek_treatment_explanation' => 'required_if:ever_been_advised_to_seek_treatment,true',
            'condition_that_can_affect_practicing_medicine' => 'required',
            'condition_that_can_affect_practicing_medicine_explanation' => 'required_if:condition_that_can_affect_practicing_medicine,true',
            'presently_involved_in_use_of_illegal_substance' => 'required',
            'presently_involved_in_use_of_illegal_substance_explanation' => 'required_if:presently_involved_in_use_of_illegal_substance,true',
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        $this->setMeta('message', $validator->messages()->first());
        $this->setMeta('status', AppConstant::STATUS_FAIL);
        $response = new JsonResponse($this->setResponse(), Response::HTTP_UNPROCESSABLE_ENTITY);
        throw (new ValidationException($validator, $response))->status(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

}
