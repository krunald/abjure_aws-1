<?php

namespace App\Http\Requests\API\V1\Physician\OnBoarding;

use App\Traits\ApiResponse;
use App\Utils\AppConstant;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\Response;

class UserClinicalCertificationCreateRequest extends FormRequest
{

    use ApiResponse;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
//            'user_id'=>'required|unique:user_clinical_certification_details,user_id|exists:users,id',
            'bls_certification' => 'required',
            'bls_certification_expiration' => 'required_if:bls_certification,true|date',
            'acls_certification' => 'required',
            'acls_certification_expiration' => 'required_if:acls_certification,true|date',
            'atls_certification' => 'required',
            'atls_certification_expiration' => 'required_if:atls_certification,true|date',
            'pals_certification' => 'required',
            'pals_certification_expiration' => 'required_if:pals_certification,true|date',
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        $this->setMeta('message', $validator->messages()->first());
        $this->setMeta('status', AppConstant::STATUS_FAIL);
        $response = new JsonResponse($this->setResponse(), Response::HTTP_UNPROCESSABLE_ENTITY);
        throw (new ValidationException($validator, $response))->status(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

}
