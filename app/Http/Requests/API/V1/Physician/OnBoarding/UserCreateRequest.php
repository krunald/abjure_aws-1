<?php

namespace App\Http\Requests\API\V1\Physician\OnBoarding;

use App\Traits\ApiResponse;
use App\Utils\AppConstant;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\Response;

class UserCreateRequest extends FormRequest
{
    use ApiResponse;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => ['required', 'regex:/^[\pL\s]+$/u'],
            'last_name' => ['bail', 'required', 'regex:/^[\pL\s]+$/u'],
            'middle_name' => ['bail', 'nullable', 'regex:/^[\pL\s]+$/u'],
            'email' => ['bail', 'required', 'email:rfc,dns', 'unique:users,email', 'unique:hospital,email'],
            'password' => ['bail', 'required', 'min:6', 'max:30', 'regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$/', 'confirmed'],
            'suffix' => ['nullable', 'alpha'],
            'title' => ['bail', 'required', 'in:MD,DO'],
            'expertise' => ['bail', 'required', 'in:HOSPITALIST,GENERAL_SURGERY'],
        ];
    }

    public function messages()
    {
        return [
            'required' => 'A :attribute is required',
            'unique' => 'A :attribute is already in use',
            'same' => 'The :attribute and :other must match.',
            'password.regex' => 'The :attribute must have One Captial Letter symbol and one numeric value in it.',
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        $this->setMeta('message', $validator->messages()->first());
        $this->setMeta('status', AppConstant::STATUS_FAIL);
        $response = new JsonResponse($this->setResponse(), Response::HTTP_UNPROCESSABLE_ENTITY);
        throw (new ValidationException($validator, $response))->status(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

}
