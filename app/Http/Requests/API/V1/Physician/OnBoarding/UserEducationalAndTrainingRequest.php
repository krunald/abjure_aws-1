<?php

namespace App\Http\Requests\API\V1\Physician\OnBoarding;

use App\Traits\ApiResponse;
use App\Utils\AppConstant;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\Response;

class UserEducationalAndTrainingRequest extends FormRequest
{
    use ApiResponse;

    protected const REQUIRED_IF_ATTENDED_FIFTIETH_TRUE = 'required_if:attended_fifth_way_program,1,true';

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id' => ['bail', 'required', 'exists:users,id'],
            'university_details.*.type' => ['required', 'in:UNIVERSITY'],
            'university_details.*.name' => ['required'],
            'university_details.*.address' => ['required'],
            'university_details.*.start_date' => ['required'],
            'university_details.*.end_date' => ['required', 'date', 'after:university_details.*.start_date'],
            'medical_school.*.type' => ['required', 'in:MEDICAL_SCHOOL'],
            'medical_school.*.name' => ['required'],
            'medical_school.*.address' => ['required'],
            'medical_school.*.start_date' => ['required', 'date'],
            'medical_school.*.end_date' => ['required', 'date', 'after:medical_school.*.start_date'],
            'foreign_medical_graduate' => ['required','bool'],
            'attended_fifth_way_program' => ['required_if:foreign_medical_graduate,1,true'],
            'ecfmg_certificate' => ['required_if:foreign_medical_graduate,true'],
            'ecfmg_certificate_number' => ['required_if:ecfmg_certificate,true'],
            'fifth_way_program' => [ self::REQUIRED_IF_ATTENDED_FIFTIETH_TRUE],
            'fifth_way_program.*.type' => [self::REQUIRED_IF_ATTENDED_FIFTIETH_TRUE, 'in:FIFTHWAY'],
            'fifth_way_program.*.name' => [self::REQUIRED_IF_ATTENDED_FIFTIETH_TRUE],
            'fifth_way_program.*.address' => [self::REQUIRED_IF_ATTENDED_FIFTIETH_TRUE],
            'fifth_way_program.*.start_date' => [self::REQUIRED_IF_ATTENDED_FIFTIETH_TRUE, 'date'],
            'fifth_way_program.*.end_date' => [self::REQUIRED_IF_ATTENDED_FIFTIETH_TRUE,'date' ,'after:fifth_way_program.*.start_date'],
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        $this->setMeta('message', $validator->messages()->first());
        $this->setMeta('status', AppConstant::STATUS_FAIL);
        $response = new JsonResponse($this->setResponse(), Response::HTTP_UNPROCESSABLE_ENTITY);
        throw (new ValidationException($validator, $response))->status(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

}
