<?php

namespace App\Http\Requests\API\V1\Physician\OnBoarding;

use App\Traits\ApiResponse;
use App\Utils\AppConstant;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\Response;

class UserPersonalInfoUpdateRequest extends FormRequest
{
    use ApiResponse;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id' => ['exists:user_personal_infos,user_id'],
            'address' => ['required'],
            'npi' => ['required'],
            'citizen' => ['bail', 'required', 'in:US_CITIZEN,OTHER,GREEN_CARD,H-1B'],
            'dob' => ['bail', 'required', 'date', 'before:today'],
            'contact_no' => ['required', 'min:6', 'max:15', 'regex:/^([0-9\s\-\+\(\)]*)$/'],
            'formatted_contact_no' => ['nullable', 'min:6', 'max:15', 'regex:/^\(?\d{3}\)?[- ]?\d{3}[- ]?\d{0,9}$/'],
            'experience_month' => ['bail', 'required', 'numeric', 'between:0,11'],
            'experience_year' => ['bail', 'required', 'numeric'],
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        $this->setMeta('message', $validator->messages()->first());
        $this->setMeta('status', AppConstant::STATUS_FAIL);
        $response = new JsonResponse($this->setResponse(), Response::HTTP_UNPROCESSABLE_ENTITY);
        throw (new ValidationException($validator, $response))->status(Response::HTTP_UNPROCESSABLE_ENTITY);
    }
}
