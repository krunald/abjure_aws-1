<?php

namespace App\Http\Requests\API\V1\Physician\Jobs;

use App\Traits\ApiResponse;
use App\Utils\AppConstant;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;
use Illuminate\Contracts\Validation\Validator;
use Symfony\Component\HttpFoundation\Response;


class UserJobsCreateRequest extends FormRequest
{
    use ApiResponse;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'job_id'=>'required|exists:jobs,id',
        ];
    }
    protected function failedValidation(Validator $validator)
    {
        $this->setMeta('message', $validator->messages()->first());
        $this->setMeta('status', AppConstant::STATUS_FAIL);
        $response = new JsonResponse($this->setResponse(), Response::HTTP_UNPROCESSABLE_ENTITY);
        throw (new ValidationException($validator, $response))->status(Response::HTTP_UNPROCESSABLE_ENTITY);
    }


}
