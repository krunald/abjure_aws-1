<?php

namespace App\Http\Requests\API\V1\Physician\Jobs;

use App\Traits\ApiResponse;
use App\Utils\AppConstant;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\Response;

class GenerateUserJobInvoiceRequest extends FormRequest
{
    use ApiResponse;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_jobs_id' => ['required', 'exists:user_jobs,id'],
            'invoice_status' => ['required', 'in:SENT,PENDING,PAID'],
            'want_to_add_overtime' => ['nullable'],
            'overtime_hours' => ['nullable'],
            'acknowledge' => ['nullable'],
            'date' => ['bail', 'required', 'date'],
            'hours' => ['required'],
            'rate' => ['required'],
            'total_amount' => ['required'],
        ];
    }

    public function messages()
    {
        return [
            'user_jobs_id.unique' => 'Invoice has been already generated for this Job.'
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        $this->setMeta('message', $validator->messages()->first());
        $this->setMeta('status', AppConstant::STATUS_FAIL);
        $response = new JsonResponse($this->setResponse(), Response::HTTP_UNPROCESSABLE_ENTITY);
        throw (new ValidationException($validator, $response))->status(Response::HTTP_UNPROCESSABLE_ENTITY);
    }
}
