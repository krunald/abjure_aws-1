<?php

namespace App\Http\Requests\API\V1\Physician\Account;

use App\Traits\ApiResponse;
use App\Utils\AppConstant;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\Response;

class UpdateBankDetailsRequest extends FormRequest
{
    use ApiResponse;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'company/individual_name' => ['bail', 'required'],
            'bank_name' => ['bail', 'required'],
            'account_type' => ['bail', 'required', 'in:CHECKING_ACCOUNT,SAVINGS_ACCOUNT'],
            'routing_number' => ['bail', 'required', 'numeric'],
            'account_number' => ['bail', 'required', 'numeric', 'min:3'],
            'social_security_number' => ['bail', 'required_without:tax_identification_number', 'digits:9'],
            'tax_identification_number' => ['bail', 'required_without:social_security_number', 'digits:9'],
        ];
    }

    /**
     * @param Validator $validator
     * @return void
     * @throws ValidationException
     */
    protected function failedValidation(Validator $validator)
    {
        $this->setMeta('message', $validator->messages()->first());
        $this->setMeta('status', AppConstant::STATUS_FAIL);
        $response = new JsonResponse($this->setResponse(), Response::HTTP_UNPROCESSABLE_ENTITY);
        throw (new ValidationException($validator, $response))->status(Response::HTTP_UNPROCESSABLE_ENTITY);
    }
}
