<?php

namespace App\Http\Middleware;

use App\Traits\ApiResponse;
use App\Utils\AppConstant;
use Closure;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

class Subscribed
{
    use ApiResponse;

    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return JsonResponse
     */
    public function handle(Request $request, Closure $next): JsonResponse
    {
        $hospital = Auth::user();
        if ($hospital->is_free_user === 0) {
            $this->setMeta('message', __('messages.hospital.subscription.freePlanExpired'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), Response::HTTP_UNPROCESSABLE_ENTITY);
        }
        return $next($request);
    }
}
