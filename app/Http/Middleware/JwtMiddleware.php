<?php

namespace App\Http\Middleware;

use App\Traits\ApiResponse;
use App\Utils\AppConstant;
use Closure;
use Illuminate\Http\JsonResponse;
use Exception;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Tymon\JWTAuth\Http\Middleware\BaseMiddleware;
use JWTAuth;

class JwtMiddleware extends BaseMiddleware
{
    use ApiResponse;

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try {
            JWTAuth::parseToken()->authenticate();
        } catch (TokenExpiredException $e) {
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            $this->setMeta('message', __('messages.token.expired'));
            return response()->json($this->setResponse(), AppConstant::TOKEN_INVALID);
        } catch (TokenInvalidException|JWTException $e) {
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            $this->setMeta('message', __('messages.token.invalidToken'));
            return response()->json($this->setResponse(), AppConstant::TOKEN_INVALID);
        } catch (Exception $e) {
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            $this->setMeta('message', $e->getMessage());
            return response()->json($this->setResponse(), AppConstant::BAD_REQUEST);
        }
        return $next($request);
    }
}
