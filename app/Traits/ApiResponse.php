<?php

namespace App\Traits;

use App\Utils\AppConstant;
use Illuminate\Http\Request;

trait ApiResponse
{

    protected $meta;
    protected $data;
    protected $paginate;
    protected $response;

    public function setMeta($key, $value)
    {
        return $this->meta[$key] = $value;
    }

    public function setData($key, $value)
    {
        return $this->data[$key] = $value;
    }

    protected function setPaginate($value)
    {
        $this->paginate = $value;
        //Log::info($this->data);
    }

//    public function setResponse()
//    {
//        $this->response['meta'] = $this->meta;
//        if ($this->data != null) $this->response['data'] = $this->data;
//        return $this->response;
//    }

    protected function setResponse()
    {
        $this->response['meta'] = $this->meta;
        if ($this->data !== null) {
            $this->response['data'] = $this->data;
        }
        if ($this->paginate !== null) {
            $this->response['pagination'] = $this->paginate;
        }
        $this->meta = array();
        $this->data = array();
        $this->paginate = array();
        return $this->response;
    }

    protected function setQueryExceptionResponse($message = '')
    {
        if ($message === '')
            $message = __('auth.server_error');

        $this->meta = array();
        $this->data = array();
        $this->paginate = array();

        $this->meta['status'] = AppConstant::STATUS_FAIL;
        $this->meta['message'] = $message;

        $this->response['meta'] = $this->meta;

        $this->meta = array();
        $this->data = array();
        $this->paginate = array();

        return $this->response;
    }
}
