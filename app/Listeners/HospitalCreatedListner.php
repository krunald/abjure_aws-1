<?php

namespace App\Listeners;

use App\Events\HospitalCreated;
use App\Mail\Hospital\VerifyEmail;
use App\Models\Hospital\common\NotificationPreferences;
use App\Models\Hospital\Onboarding\HospitalVerifyEmail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class HospitalCreatedListner implements ShouldQueue
{
    public $queue = "emailVerification";
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param HospitalCreated $event
     * @return void
     */
    public function handle(HospitalCreated $event)
    {
        $hospital = $event->hospital;
        NotificationPreferences::create([
            'hospital_id' => $hospital->id,
            'email' => 1,
            'text_message' => 1
        ]);

        $resetToken = Str::random(60);
        HospitalVerifyEmail::updateOrCreate(
            [
                'email' => $hospital->email
            ],
            [
                'token' => $resetToken,
            ]
        );

        //Generate, the email verification link. The token generated is embedded in the link
        $link = config('appconstants.APP_URL') . '/api/hospital/verify/email?token=' . $resetToken . '&email=' . urlencode($hospital->email);
        $hospitalData['link'] = $link;
        $hospitalData['name'] = $hospital->name;

        Mail::to($hospital->email)->send(new VerifyEmail($hospitalData));
    }
}
