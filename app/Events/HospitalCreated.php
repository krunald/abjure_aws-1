<?php

namespace App\Events;

use App\Models\Hospital\Onboarding\Hospital;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class HospitalCreated
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    public $hospital;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Hospital $hospital)
    {
        $this->hospital = $hospital;
    }
}
