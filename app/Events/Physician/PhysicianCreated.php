<?php

namespace App\Events\Physician;

use App\Models\Physician\Onboarding\User;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class PhysicianCreated
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $physician;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(User $physician)
    {
        $this->physician = $physician;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    /*public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }*/
}
