FROM ghcr.io/wmt-web/php8.0-alpine/1:main

# RUN apt install -y pcre-dev ${PHPIZE_DEPS} libressl-dev pkgconfig libevent-dev libzip-dev && pecl install event && apk del pcre-dev ${PHPIZE_DEPS}
# RUN docker-php-ext-install zip

RUN mkdir sample-app
WORKDIR sample-app/

COPY composer.json composer.json
COPY composer.lock composer.lock

RUN composer update --ignore-platform-req=ext-pcntl --prefer-dist --no-scripts --no-autoloader && rm -rf /root/.composer

COPY --chown=www-data:www-data . .
COPY . .

RUN cp .env.example .env
ADD conf/nginx/default.conf /etc/nginx/nginx.conf
ADD conf/supervisor/services.ini /etc/supervisor.d/
ADD conf/nginx/default.conf /etc/nginx/conf.d/

RUN chgrp -R www-data /var/www/html/sample-app/storage /var/www/html/sample-app/bootstrap/cache
RUN chmod -R 775 /var/www/html/sample-app/storage /var/www/html/sample-app/bootstrap/cache
RUN chmod -R 777 storage/

# RUN rc-service supervisor start
# RUN touch /run/openrc/softlevel
# RUN rc-service nginx start

RUN composer dump-autoload --no-scripts --optimize
RUN ln -s /var/www/html/sample-app/storage/app/ /var/www/html/sample-app/public/storage


RUN echo user=root >>  /etc/supervisor/supervisord.conf
CMD ["/usr/bin/supervisord","-n"]
# CMD [ "/usr/bin/supervisord" ]
# CMD php artisan serve --host=0.0.0.0s