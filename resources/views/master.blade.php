<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Abjure') }}</title>

    <!-- Scripts -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>

    <!-- JavaScript Bundle with Popper -->

    <!-- CSS only -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">


    <!-- Fonts -->

    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <style>
        body, .form-control, .btn {
            font-size: 14px !important;
        }

        body {
            background: #FFFFFF;
        }

        html, body {
            font-family: 'Regular', Work Sans;
            color: #092321;
            margin: 0;
        }

        .full-height {
            height: 100vh;
        }

        .auto-height {
            height: auto;
        }

        .title {
            font-size: 30px !important;
        }

        .white-font-color {
            color: #FFFFFF;
        }

        .secondary-color {
            background: #FF9327;
        }

        .tableClass {
            max-width: 380px !important;
        }

        .btn {
            background-color: #FF9327;
            border: 1px solid transparent;
            border-radius: .25rem;
            color: #e2edf4;
            display: inline-block;
            font-size: .95rem;
            font-weight: 400;
            line-height: 1.5;
            padding: .375rem .75rem;
            text-align: center;
            transition: color .15s ease-in-out, background-color .15s ease-in-out, border-color .15s ease-in-out, box-shadow .15s ease-in-out;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            vertical-align: middle
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .border-radius-16 {
            border: none;
            border-radius: 16px;
        }
        @media screen and (min-width: 400px ) {
            .logo-image {
                width: 180px;
            }
            .tableClass{
                max-width: 380px;
            }
        }
            .rounded-pill {
                border-radius: 50rem !important
            }
        @media screen and (min-width: 300px ) {
            .logo-image {
                width: 180px;
            }

            .rounded-pill {
                border-radius: 50rem !important
            }
        @yield('styles')
    </style>
</head>
<body>
<div id="app">
    @yield('subcontent')
</div>
</body>
