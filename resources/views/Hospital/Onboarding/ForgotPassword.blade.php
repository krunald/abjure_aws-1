@extends('master')

@section('subcontent')

    <div class="p-0 m-0" style="background-color: #FFFFFF">
        <div class="row m-0 h-100">
            <div class="d-none d-sm-block col-md-5 col-lg-4 p-0 m-0" style="height: 100vh; max-width: 100vw">
                <div style="background-image: url({{asset('images/BG.png')}}); height: 100%; width: 100%; background-size: cover;position: absolute" alt="">
                </div>
            </div>
            <div class="col-sm-0 col-md-0 col-lg-1 p-0 m-0 my-auto"></div>
            <div class="col-sm-12 col-sm col-md-7 col-lg-6 p-0 m-0 my-auto" style="max-height: 100% !important;background-color: #FFFFFF">
                <div class="flex-row align-items-center col-lg-12 my-auto" style="min-height: 100vh;height: auto; padding-bottom: 8px">
                    <div class="d-flex flex-column align-content-center justify-content-center mx-5">
                        <div class="d-flex justify-content-center mb-1 mt-5"><img
                                src="{{asset('images/Logo.svg')}}"
                                width="300" class="img-fluid img-responsive" alt=""></div>
                        <div class="text-bold text-center mt-0 text-white" style="font-size: 40px">
                            <h1>{{ __('Reset Password') }}</h1></div>

                        <div class="card-body mx-lg-5">
                            <form method="POST" action="{{ url('password/confirm') }}">
                                @csrf
                                <input type="hidden" name="token" value="{{ $data['token'] }}">
                                <div class="form-group row mx-lg-5">
                                    {{--                                    <label for="email" class="col-md-12 col-lg-12 col-form-label text-dark mb-2">{{ __('Email') }}</label>--}}
                                    <div class="col-md-12 col-lg-12">
                                        <input id="email" type="email" hidden
                                               class="border-bottom form-control @error('email') is-invalid @enderror" name="email"
                                               value="{{ $data['email'] ?? old('email') }}" autocomplete="email"
                                               autofocus readonly>
                                        @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row mx-lg-5">
                                    <label for="password" class="col-md-12 col-lg-12 col-form-label text-dark">{{ __('Password') }}</label>
                                    <div class="col-md-12 col-md-12 mb-2">
                                        <input id="password" type="password"
                                               placeholder="Enter Password"
                                               class="border-bottom form-control @error('password') is-invalid @enderror" name="password"
                                               autocomplete="new-password">
                                        @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row mx-lg-5">
                                    <label for="password-confirm"
                                           class="col-md-12 col-lg-12 col-form-label text-dark">Confirm Password</label>
                                    <div class="col-md-12 col-lg-12 mb-2">
                                        <input id="password-confirm" type="password" class="border-bottom form-control"
                                               placeholder="Confirm Password"
                                               name="password_confirmation" autocomplete="new-password">
                                    </div>
                                </div>
                                <div class="form-group row mb-5 mx-lg-5 flex-center">
                                    <div class="col-lg-12">
                                        <button type="submit" class="btn-sm secondary-color rounded-pill border-0 d-flex login-form-btn text-white align-items-center justify-content-center mt-4 d-block m-auto">
                                            Reset Password
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{--<div class="container">--}}
    {{--    <div class="row justify-content-center">--}}
    {{--        <div class="col-md-4">--}}
    {{--            aa--}}
    {{--        </div>--}}
    {{--        <div class="col-md-8">--}}
    {{--            <div class="card">--}}
    {{--                <div class="card-header">{{ __('Reset Password') }}</div>--}}

    {{--                <div class="card-body">--}}
    {{--                    <form method="POST" action="{{ url('password/confirm') }}">--}}
    {{--                        @csrf--}}

    {{--                        <input type="hidden" name="token" value="{{ $data['token'] }}">--}}

    {{--                        <div class="row mb-3">--}}

    {{--                            <div class="col-md-6">--}}
    {{--                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $data['email'] ?? old('email') }}" required autocomplete="email" autofocus>--}}

    {{--                                @error('email')--}}
    {{--                                    <span class="invalid-feedback" role="alert">--}}
    {{--                                        <strong>{{ $message }}</strong>--}}
    {{--                                    </span>--}}
    {{--                                @enderror--}}
    {{--                            </div>--}}
    {{--                        </div>--}}

    {{--                        <div class="row mb-3">--}}
    {{--                            <label for="password" class="col-md-4 col-form-label text-md-end">{{ __('Password') }}</label>--}}

    {{--                            <div class="col-md-6">--}}
    {{--                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">--}}

    {{--                                @error('password')--}}
    {{--                                    <span class="invalid-feedback" role="alert">--}}
    {{--                                        <strong>{{ $message }}</strong>--}}
    {{--                                    </span>--}}
    {{--                                @enderror--}}
    {{--                            </div>--}}
    {{--                        </div>--}}

    {{--                        <div class="row mb-3">--}}
    {{--                            <label for="password-confirm" class="col-md-4 col-form-label text-md-end">{{ __('Confirm Password') }}</label>--}}

    {{--                            <div class="col-md-6">--}}
    {{--                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">--}}
    {{--                            </div>--}}
    {{--                        </div>--}}

    {{--                        <div class="row mb-0">--}}
    {{--                            <div class="col-md-6 offset-md-4">--}}
    {{--                                <button type="submit" class="btn btn-primary">--}}
    {{--                                    {{ __('Reset Password') }}--}}
    {{--                                </button>--}}
    {{--                            </div>--}}
    {{--                        </div>--}}
    {{--                    </form>--}}
    {{--                </div>--}}
    {{--            </div>--}}
    {{--        </div>--}}
    {{--    </div>--}}
    {{--</div>--}}
@endsection
