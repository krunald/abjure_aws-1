<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>404 Error</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    </head>
    <style>
        html, body {
            font-family: 'Regular', Work Sans;
            color: #092321;
            height: 100vh;
            margin: 0;
        }
    </style>
    <body>
        <div class="container mt-5 pt-5">
            <div class="alert alert-danger text-center" style="background: #FF9327 0% 0% no-repeat padding-box;">
                <h2 class="display-3">404</h2>
                <p class="display-5">Oops! Something is wrong.</p>
            </div>
        </div>
    </body>
</html>
