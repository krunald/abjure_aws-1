@extends('master')

@section('content')
    <div class="container mt-5 pt-5 text-center">
        <img width="150" src="{{asset('images/Logo.svg')}}" title="logo" alt="logo" class="mb-4">

        <div class="alert alert-danger text-center" style="background: #FF9327 0% 0% no-repeat padding-box;">
            <h2 class="display-3" style="color: #092321">Password Changed successfully</h2>
        </div>
    </div>

