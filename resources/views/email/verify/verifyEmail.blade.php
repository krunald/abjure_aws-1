@component('mail::message')
# Dear, {{$content['name']}}

You are receiving this email because you  have registered on Abjure with this email. Please click on below button to verify your email.

@component('mail::button', ['url' => $content['link']])
    Verify Email
@endcomponent
If you did not register , no further action is required.

Thanks,
{{config('app.name')}}

@endcomponent
