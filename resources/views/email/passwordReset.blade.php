@component('mail::message')
# Dear, {{$content['name']}}

You are receiving this email because we received a reset password request for your account. Please click on below button for reset your password.

@component('mail::button', ['url' => $content['link']])
    Reset Password
@endcomponent

If you did not request a password reset , no further action is required.

Thanks,
{{config('app.name')}}

@endcomponent
