<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'reset' => 'Your password has been reset!',
    'sent' => 'We have emailed your password reset link!',
    'throttled' => 'Please wait before retrying.',
    'token' => 'This password reset token is invalid.',
    'user' => "We can't find a user with that email address.",
    'admin' => "We can't find a admin with given credentials.",
    'password' => [
        'currentPassword' => 'Your current password does not matches with the password you provided. Please try again',
        'samePassword' => 'New Password cannot be same as your current password. Please choose a different password'
    ],
    'verifyPassword' => [
        'sent' => 'We have emailed your password reset code!'
    ],

    'custom' => [
        'new_password' => [
            'regex' => 'Password must contain at least one number, both uppercase and lowercase letters and one special character.',
        ],
        'phone_number' => [
            'regex' => 'Phone number must contain numbers only',
        ],
    ],

];
