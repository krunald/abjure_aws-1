<?php


return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'login' => 'User has been logged in Successfully',
    'logout' => 'User has been logged out Successfully',
    'registered' => 'User has been registered Successfully, Please check your email to verify and access your account.',
    'token_refresh' => 'Token has been refreshed Successfully',
    'user_details' => 'User details have been fetched Successfully',
    'tokenExpired' => 'Provided Token is expired',
    'tokenInvalid' => 'Provided Token is Invalid',
    'tokenRequired' => 'Token is required',
    'tokenBlacklisted' => 'The token has been blacklisted',

    'invalidToken' => 'Please provide valid token',

    'somethingWrong' => 'Something went wrong. Please try again later.',
    'parameterMissing' => 'Parameter Missing.',
    'videoNotFound' => 'Video not found.',
    'emailNotFound' => 'Email not found.',
    'dataNotFoundWithUser' => 'Data with this user not found.',
    'recordNotFound' => 'Details not found with this record.',
    'invalidCredentials' => 'Credentials are Invalid.',
    'userNotFound' => 'User not found with this record.',
    'user' => [
        'profile_updated' => 'Profile updated successfully',
        'picture_updated' => "Profile picture updated successfully",
        'get_video_list' => "User's video list fetched successfully",
        'delete_video' => 'Video successfully deleted',
    ],
    'unauthorizedAccessToRecord' => "Trying to access unauthorized record.",

    'physician' => [
        'notifications' => [
            'update' => 'Physician preferences updated successfully',
            'fetch' => 'Physician preferences fetched successfully',
        ],
        'bankDetails' => [
            'update' => 'Physician bank details updated successfully',
            'fetch' => 'Physician bank details fetched successfully',
        ],
        'availability' => [
            'created' => 'Your availability is added successfully.',
            'fetched' => 'Your availability fetched successfully.',
            'conflict' => 'Your availability is already added for this date and shift.',
        ],

        'fetchedInterViewListSuccessfully' => 'Interview list fetched successfully.',
        'emailSuccessfullyVerified' => 'Your email is successfully verified.',
        'emailAlreadyVerified' => 'Email is already verified.',
        'resendEmailVerification' => 'Email verification link is sent again. Please check your email and login again.',
        'calendarDetailsFetched' => 'Physician Calendar Details fetched successfully',
        'recommendation_list_fetched' => 'Recommendation list fetched successfully',
        'onBoarding' => [
            'boardCertificationAdded' => 'Board Certification Details Added Successfully',
            'boardCertificationUpdated' => 'Board Certification Details Updated Successfully',
            'boardCertificationFetched' => 'Board Certification Details Fetched Successfully',
            'clinicalCertificationAdded' => 'Clinical Certification Details Added Successfully',
            'clinicalCertificationUpdated' => 'Clinical Certification Details Updated Successfully',
            'clinicalCertificationFetched' => 'Clinical Certification Details Fetched Successfully',
            'licenseIssuerStateAdded' => 'License Issuer Details Added Successfully',
            'licenseIssuerStateUpdated' => 'License Issuer Details Updated Successfully',
            'licenseIssuerStateFetched' => 'License Issuer Details Fetched Successfully',
            'personInfoAdded' => 'Physician`s Personal Information Added Successfully',
            'personInfoUpdated' => 'Physician`s Personal Information Updated Successfully',
            'personInfoFetched' => 'Physician`s Personal Information Fetched Successfully',
            'educationAndTrainingDetailsAdded' => 'Educational Details Added Successfully',
            'educationAndTrainingDetailsUpdated' => 'Educational Details Updated Successfully',
            'educationAndTrainingDetailsFetched' => 'Educational Details Fetched Successfully',
            'trainingDetailsAdded' => 'Training Details Added Successfully',
            'trainingDetailsUpdated' => 'Training Details Updated Successfully',
            'trainingDetailsFetched' => 'Training Details Fetched Successfully',
            'generalSurgeryDetailsAdded' => 'General Surgery Details Added Successfully',
            'generalSurgeryDetailsUpdated' => 'General Surgery Details Updated Successfully',
            'generalSurgeryDetailsFetched' => 'General Surgery Details Fetched Successfully',
            'hospitalistDetailsAdded' => 'Hospitalist Details Added Successfully',
            'hospitalistDetailsUpdated' => 'Hospitalist Details Updated Successfully',
            'hospitalistDetailsFetched' => 'Hospitalist Details Fetched Successfully',
            'malPracticeDetailsAdded' => 'Mal Practice Details Added Successfully',
            'malPracticeDetailsUpdated' => 'Mal Practice Details Updated Successfully',
            'malPracticeDetailsDeleted' => 'Mal Practice Detail Deleted Successfully',
            'malPracticeDetailsFetched' => 'Mal Practice Detail Fetched Successfully',
            'workHistoryDetailsAdded' => 'Work History Details Added Successfully',
            'workHistoryDetailsUpdated' => 'Work History Details Updated Successfully',
            'workHistoryDetailsDeleted' => 'Work History Detail Deleted Successfully',
            'workHistoryDetailsFetched' => 'Work History Detail Fetched Successfully',
            'workReferencesDetailsAdded' => 'Work References Details Added Successfully',
            'workReferencesDetailsUpdated' => 'Work References Details Updated Successfully',
            'workReferencesDetailsDeleted' => 'Work References Detail Deleted Successfully',
            'workReferencesDetailsFetched' => 'Work References Detail Fetched Successfully',
            'disciplinaryActionDetailsAdded' => 'Disciplinary Action Details Added Successfully',
            'disciplinaryActionDetailsUpdated' => 'Disciplinary Action Details Updated Successfully',
            'disciplinaryActionDetailsDeleted' => 'Disciplinary Action Detail Deleted Successfully',
            'disciplinaryActionDetailsFetched' => 'Disciplinary Action Detail Fetched Successfully',
        ],
        'jobs' => [
            'invoiceSent' => 'Invoice Sent Successfully',
            'detailsFetchedSuccessfully' => "Job Details Fetched Successfully",
            'savedJobSuccessfully' => "Thank you! Your application is saved. You can apply in future.",
            'alreadySavedJob' => "Your application is already saved. Please check saved jobs tab.",
            'unsavedJobSuccessfully' => "Your application is removed from saved jobs successfully.",
            'canceledJobSuccessfully' => "Your application is canceled successfully.",
            'canceledAppliedJobOnly' => "You can cancel application with applied status only.",
            'cannotGenerateInvoiceForOtherStatus' => "You cannot generate invoice for application whose offer is not approved yet.",
            'appliedSuccessfully' => "You have successfully applied to job.",
            'cannotApplyToIncompleteJobs' => "You cannot apply to incomplete job.",
            'cannotApplyToJobsWithDifferentExpertise' => "You cannot apply to job with different expertise.",
            'cannotAcknowledgeUnPaidInvoice' => "You cannot acknowledge unpaid invoice.",
            'acknowledgePaidInvoice' => "Only Paid invoice can be acknowledged, Please wait for you payment to get Paid.",
        ],
        'delete_video' => 'Video successfully deleted'
    ],
    'hospital' => [
        'login' => 'Hospital profile has been logged in Successfully',
        'logout' => 'Hospital profile has been logged out Successfully',
        'hospitalNotFound' => 'Hospital details not found with the given data',
        'profile_updated' => 'Hospital Profile updated successfully',
        'highlights_added' => 'Hospital highlights added successfully',
        'highlights_updated' => 'Hospital highlights updated successfully',
        'profile_fetched' => 'Hospital Profile fetched successfully',
        'highlights_created' => 'Hospital highlights created successfully',
        'highlight_fetched' => 'Hospital highlight fetched successfully',
        'speciality_fetched' => 'Hospital speciality fetched successfully',
        'calendarDetailsFetched' => 'Hospital Calendar Details fetched successfully',
        'list_fetched' => 'list fetched successfully',
        'hospitalHighlightsNotFound' => 'Hospital Highlights not found with the given data',
        'subscription' => [
            'subscribed' => 'Hospital profile has been subscribed successfully',
            'subscribedToFree' => 'Hospital profile has been subscribed to free plan successfully',
            'alreadySubscribed' => 'Hospital profile has been already subscribed',
            'alreadySubscribedToFreePlan' => 'Hospital profile has already subscribed to paid plan',
            'fetchStripePlan' => 'Stripe plans fetched successfully',
            'subscriptionNotFound' => 'Hospital does not have any subscription plan',
            'subscriptionCanceled' => 'Hospital profile subscription plan has been canceled successfully',
        ],
        'dashboard' => [
            'fetchRecruitmentStats' => 'Recruitment Stats fetched successfully',
            'fetchPendingApplications' => 'Pending Applications Stats fetched successfully'
        ],
        'calendar' => [
            'meetingAlreadyExists' => 'Meeting already exists for specified time slot . Please Select another time . ',
        ],
        'template' => [
            'add' => 'Job template created successfully',
            'update' => 'Job template updated successfully',
            'fetch' => 'Job template fetched successfully',
            'list_fetch' => 'Job template list fetched successfully',
            'templateNotFound' => 'Job template not found with the given data',
        ],
        'notifications' => [
            'update' => 'Hospital preferences updated successfully',
            'fetch' => 'Hospital preferences fetched successfully',
        ],
        'job' => [
            'jobCanNotCreate' => 'Hospital can not create more than 5 job in free plan',
            'added' => "Job added successfully",
            'updated' => "Job updated successfully",
            'recordNotFound' => 'Job Sub Details not found with this record . ',
            'descriptionNotFound' => 'Job Description not found with this record . ',
            'hospitalistRecordNotFound' => 'Job Hospitalist details not found with this record . ',
            'generalSurgeryRecordNotFound' => 'Job General Surgery details not found with this record . ',
            'jobStatusUpdated' => 'Job status updated successfully . ',
            'jobStatusUpdateAttemptedForCompleteJobs' => 'You cannot change the status of job which are moved to completed . ',
            'jobNotFound' => "Job details not found with given data.",
            'jobActive' => "Job is move to active state successfully.",
            'invoice' => [
                'invoicePaid' => 'Invoice has been paid . ',
                'invoiceFetched' => 'Invoice data has been fetched . ',
            ],
            'schedule' => [
                'added' => "Job Schedule Added Successfully",
                'updated' => "Job Schedule Updated Successfully",
                'cannotUpdateCompletedJobSchedule' => "Cannot update schedule for completed jobs.",
            ],
            'payment' => [
                'added' => "Job Payment Added Successfully",
                'scheduleHoursFetched' => "Job Schedules total hours fetched successfully",
                'updated' => "Job Payment Updated Successfully",
            ],
            'description' => [
                'added' => "Job Description Added Successfully",
                'updated' => "Job Description Updated Successfully",
            ],

            'list_fetch' => 'Job list fetched successfully',
        ],
        'userJobs' => [
            'statusUpdated' => 'Applicants Job status Updated . ',
            'applicantInterviewScheduled' => 'Applicants Job Interview is Scheduled . ',
            'applicantInterviewScheduledUpdated' => 'Applicants Job Interview Scheduled is updated . ',
            'applicantInterviewScheduleList' => 'Applicants Job Interview Schedule list.',

            'reviewAndRating' => [
                'added' => "Review For Doctor Added Successfully",
                'fetched' => "Review For Doctor Fetched Successfully",
            ],
        ],
    ],
    'verifyPasswordResetCode' => [
        'verified' => 'Code verified successfully',
        'resetCodeExpired' => 'Reset code expired',
        'invalidCode' => 'Please provide a valid code',
        'invalidData' => "Provided data is not valid"
    ],
    'token' => [
        'verified' => 'Token verified successfully',
        'resetTokenExpired' => 'Reset token expired',
        'resetTokenExpiredOrInvalidData' => 'Reset token expired or provided data is not valid',
        'invalidToken' => 'Please provide valid token',
        'expired' => 'Your token has expired . ',
        'invalidData' => "Provided data is not valid",
        'resetTokenExpiredOrInvalidData' => 'Reset token expired or provided data is not valid',
        'verifyTokenExpiredOrInvalidData' => 'Verify token expired or provided data is not valid'
    ],

    'password' => [
        'verifyPassword' => [
            "sent" => "Password reset link sent successfully",
        ],
        'set' => "Password updated Successfully",
        'error' => "Something went wrong while updating password"
    ],

];
