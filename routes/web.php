<?php

use App\Http\Controllers\API\V1\Physician\Home\HomeController;
use App\Http\Controllers\API\V1\ResetPasswordController;
use App\Http\Controllers\StripeWebhookController;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Route;

//use App\Http\Controllers\Hospital\Onboarding\ForgotPasswordController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Auth::routes();

Route::get('/success', function () {
    Log::info("Post Max Size: ".ini_get('post_max_size'));
    Log::info("File Max Size: ".ini_get('upload_max_filesize'));
    exit();
    return view('success/success', ['updatePassword' => __('messages.resetPassword')]);
})->name('success');

Route::get('/home', [HomeController::class, 'index'])->name('home');
Route::get('/password/{token}', [ResetPasswordController::class, 'index']);
Route::post('/password/confirm', [ResetPasswordController::class, 'resetPassword'])->name('password.confirm');
Route::get('password/success', [ResetPasswordController::class, 'success']);
Route::get('aa/view', function (){
    return view('email.verify.emailVerifiedSuccessfully');
});

Route::post('/test', [StripeWebhookController::class, 'test']);
