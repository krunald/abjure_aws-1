<?php

use App\Http\Controllers\API\V1\HealthApiController;
use App\Http\Controllers\API\V1\Hospital\common\NotificationPreferencesController;
use App\Http\Controllers\API\V1\Hospital\Dashboard\DashboardController;
use App\Http\Controllers\API\V1\Hospital\Jobs\JobsController;
use App\Http\Controllers\API\V1\Hospital\Jobs\JobsDescriptionController;
use App\Http\Controllers\API\V1\Hospital\Jobs\JobsPaymentController;
use App\Http\Controllers\API\V1\Hospital\Jobs\JobsScheduleController;
use App\Http\Controllers\API\V1\Hospital\Jobs\UserJobReviewAndRatingController;
use App\Http\Controllers\API\V1\Hospital\Jobs\UserJobsController as HospitalUserJobsController;
use App\Http\Controllers\API\V1\Hospital\Jobs\UserJobsScheduleInterviewController;
use App\Http\Controllers\API\V1\Hospital\NotificationController;
use App\Http\Controllers\API\V1\Hospital\Onboarding\ForgotPasswordController;
use App\Http\Controllers\API\V1\Hospital\Onboarding\HospitalController;
use App\Http\Controllers\API\V1\Hospital\Template\HospitalAddTemplateController;
use App\Http\Controllers\API\V1\Payment\StripeSubscriptionController;
use App\Http\Controllers\API\V1\Physician\Account\EmploymentPreferenceController;
use App\Http\Controllers\API\V1\Physician\Account\PhysicianBankDetailsController;
use App\Http\Controllers\API\V1\Physician\Calendar\CalendarController;
use App\Http\Controllers\API\V1\Physician\Common\PhysicianNotificationPreferenceController;
use App\Http\Controllers\API\V1\Physician\Home\HomeController;
use App\Http\Controllers\API\V1\Physician\Jobs\JobsController as PhysicianJobsController;
use App\Http\Controllers\API\V1\Physician\Jobs\UserJobsController;
use App\Http\Controllers\API\V1\Physician\NotificationController as PhysicianNotificationController;
use App\Http\Controllers\API\V1\Physician\Onboarding\ForgotPasswordController as PhysicianForgotPasswordController;
use App\Http\Controllers\API\V1\Physician\Onboarding\UserCertificationAndLicenseController;
use App\Http\Controllers\API\V1\Physician\Onboarding\UserController;
use App\Http\Controllers\API\V1\Physician\Onboarding\UserDetailsStatusController;
use App\Http\Controllers\API\V1\Physician\Onboarding\UserDisciplinaryActionController;
use App\Http\Controllers\API\V1\Physician\Onboarding\UserEducationalAndtrainingController;
use App\Http\Controllers\API\V1\Physician\Onboarding\UserGeneralSurgeryController;
use App\Http\Controllers\API\V1\Physician\Onboarding\UserHospitalistProfileController;
use App\Http\Controllers\API\V1\Physician\Onboarding\UserMalPracticeController;
use App\Http\Controllers\API\V1\Physician\Onboarding\UserPersonalInfoController;
use App\Http\Controllers\API\V1\Physician\Onboarding\UserWorkHistoryController;
use App\Http\Controllers\API\V1\Physician\Onboarding\UserWorkReferencesController;
use App\Http\Controllers\API\V1\Physician\SpecialitiesController;
use App\Http\Controllers\API\V1\Physician\StateListController;
use App\Http\Controllers\API\V1\Physician\Statistics\StatisticsController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'v1/hospital', 'middleware' => ['assign.guard:hospital', 'jwt.auth']], function () {
    Route::group(['prefix' => 'subscriptions'], function () {
        Route::get('stripe', [StripeSubscriptionController::class, 'getPlans']);
        Route::post('stripe', [StripeSubscriptionController::class, 'createSubscription']);
        Route::post('cancel', [StripeSubscriptionController::class, 'cancelSubscription']);
        Route::get('customer/stripe', [StripeSubscriptionController::class, 'getCustomerPlans']);
    });
});

Route::group(['prefix' => 'v1/hospital', 'middleware' => ['assign.guard:hospital', 'api']], function () {
    Route::post('register', [HospitalController::class, 'register']);
    Route::post('login', [HospitalController::class, 'login']);
    Route::post('forgot_password', [ForgotPasswordController::class, 'forgotPassword'])->middleware('throttle:20,60');
    Route::post('password/verify-token', [ForgotPasswordController::class, 'verifyPasswordResetToken']);
    Route::get('password/reset', [ForgotPasswordController::class, 'showResetPasswordForm']);
    Route::post('password/update', [ForgotPasswordController::class, 'updatePassword']);

    Route::get('health', [HomeController::class, 'healthCHeck']);
});


Route::group(['prefix' => 'v1/hospital', 'middleware' => ['assign.guard:hospital', 'jwt.auth']], function () {
    Route::post('/resend-verification-email', [HospitalController::class, 'reSendEmailVerificationLink']);

    Route::post('password/change', [HospitalController::class, 'changePassword']);
    Route::get('specialities', [SpecialitiesController::class, 'all']);
    Route::group(['prefix' => 'dashboard'], function () {
        Route::get('/', [DashboardController::class, 'displayCounts']);
        Route::get('/recentApplies', [DashboardController::class, 'recentApplies']);
        Route::get('/jobCountByStatus', [DashboardController::class, 'jobCountByStatus']);
        Route::get('/recruitment-stats', [DashboardController::class, 'fetchRecruitmentDetails']);
        Route::get('/pending-applications', [DashboardController::class, 'fetchPendingApplicationDetails']);
    });
    Route::get('/', [HospitalController::class, 'fetchLoggedInHospital']);
    Route::post('password/change', [HospitalController::class, 'changePassword']);

    Route::get('/calendar', [UserJobsScheduleInterviewController::class, 'hospitalCalendarList']);
    Route::put('update', [HospitalController::class, 'updateHospital']);
    Route::get('/notificationList', [NotificationController::class, 'index']);
    Route::put('/notificationList/{notification_id}', [NotificationController::class, 'updateStatus']);
    Route::get('specialities', [SpecialitiesController::class, 'all']);


    Route::group(['prefix' => 'highlights'], function () {
        Route::post('/', [HospitalController::class, 'createHighlights']);
        Route::put('/{hospital_id}', [HospitalController::class, 'updateHighlights']);
        Route::get('/', [HospitalController::class, 'fetchHighlights']);
    });

    Route::group(['prefix' => 'preference'], function () {
        Route::put('/update', [NotificationPreferencesController::class, 'updatePreferences']);
        Route::get('/', [NotificationPreferencesController::class, 'fetchPreferences']);
    });

    Route::get('defaults/{type}', [HomeController::class, 'fetchDefaults']);
    Route::get('template/list', [HospitalAddTemplateController::class, 'fetchList']);
    Route::resource('template', HospitalAddTemplateController::class);
    Route::post('logout', [HospitalController::class, 'logout']);

    Route::group(['prefix' => 'invoices'], function () {
        Route::get('/', [HospitalUserJobsController::class, 'getAllInvoice']);
        Route::get('{invoices_id}', [HospitalUserJobsController::class, 'getUserInvoice']);
        Route::put('{invoices_id}', [HospitalUserJobsController::class, 'payUserInvoice']);

    });

    Route::group(['prefix' => 'jobs'], function () {
        Route::get('/', [JobsController::class, 'index']);
        Route::post('/', [JobsController::class, 'JobUpdateOrCreate']);
        Route::get('/{jobs_id}', [JobsController::class, 'show']);
        Route::put('/{jobs_id}', [JobsController::class, 'update']);
        Route::get('/totalHours/{jobs_id}', [JobsController::class, 'getTotalJobHours']);
        Route::put('/jobStatus/{jobs_id}', [JobsController::class, 'updateJobCurrentStatus']);
        Route::put('/re-open/{jobs_id}', [JobsController::class, 'reOpenJob']);
        Route::put('/activeInactive/{jobs_id}', [JobsController::class, 'activeInactive']);
        Route::post('/schedule', [JobsScheduleController::class, 'store']);
        Route::post('/payment_details', [JobsPaymentController::class, 'store']);
        Route::post('/description', [JobsDescriptionController::class, 'store']);
        Route::post('/doctorsFeedback', [UserJobReviewAndRatingController::class, 'store']);
        Route::get('/doctorsFeedback/{user_job_review_and_rating_id}', [UserJobReviewAndRatingController::class, 'show']);
        Route::put('/doctorsFeedback/{user_job_review_and_rating_id}', [UserJobReviewAndRatingController::class, 'update']);
        Route::group(['prefix' => 'applicants'], function () {
            Route::get('/work_history/{applicant_id}', [JobsController::class, 'getApplicantsWorkHistory']);
            Route::get('/profile/{applicant_id}', [JobsController::class, 'getApplicantsProfile']);
            Route::put('/jobs_status/{applicant_job_id}', [JobsController::class, 'updateUserJobStatus']);
            Route::post('/schedule_interview', [UserJobsScheduleInterviewController::class, 'store']);
            Route::get('schedule_interview/{user_job_id}', [UserJobsScheduleInterviewController::class, 'show']);
            Route::put('schedule_interview/{user_jobs_schedule_interview_id}', [UserJobsScheduleInterviewController::class, 'update']);
            Route::get('{job_id}', [JobsController::class, 'getJobApplicants']);
        });
    });


});
Route::group(['prefix' => 'v1/user'], function () {
    Route::post('/', [UserController::class, 'store']);
    Route::post('/login', [UserController::class, 'authenticate']);

    Route::post('/forgot-password', [PhysicianForgotPasswordController::class, 'forgotPassword']);

    Route::group(['prefix' => 'password'], function () {
        Route::post('verify-token', [PhysicianForgotPasswordController::class, 'verifyPasswordResetToken']);
        Route::get('reset', [PhysicianForgotPasswordController::class, 'showResetPasswordForm']);
        Route::post('update', [PhysicianForgotPasswordController::class, 'updatePassword']);
    });

    Route::group(['prefix' => 'preference'], function () {
        Route::put('/update', [PhysicianNotificationPreferenceController::class, 'updatePreferences']);
        Route::get('/', [PhysicianNotificationPreferenceController::class, 'fetchPreferences']);
    });

    Route::group(['middleware' => ['jwt.verify']], function () {
        Route::get('/recommendation', [HomeController::class, 'recommendation']);
        Route::post('password/change', [UserController::class, 'changePassword']);
        Route::get('/statesList', [StateListController::class, 'index']);
        Route::post('logout', [UserController::class, 'logout']);
        Route::put('/{user_id}', [UserController::class, 'update']);
        Route::post('uploadProfilePicture', [UserController::class,'uploadProfilePicture']);

        Route::post('/resend-verification-email', [UserController::class, 'reSendEmailVerificationLink']);
        Route::get('/user/{user_id}', [UserController::class, 'show']);

        Route::get('/', [UserController::class, 'currentUser']);
        Route::get('/notificationList', [PhysicianNotificationController::class, 'index']);
        Route::put('/notificationList/{notification_id}', [PhysicianNotificationController::class, 'updateStatus']);


        Route::group(['prefix' => 'calendar'], function () {
            Route::get('/', [CalendarController::class, 'physicianCalendarList']);
            Route::group(['prefix' => 'availability'], function () {
                Route::post('/', [CalendarController::class, 'setPhysicianAvailability']);
                Route::get('/', [CalendarController::class, 'getPhysicianAvailability']);
                Route::put('/{availability_id}', [CalendarController::class, 'updatePhysicianAvailability']);
            });
        });

        Route::get('/user_details/{user_id}', [UserDetailsStatusController::class, 'show']);


        Route::group(['prefix' => 'personal_info'], function () {
            Route::post('/', [UserPersonalInfoController::class, 'store']);
            Route::get('/', [UserPersonalInfoController::class, 'show']);
            Route::put('/{user_id}', [UserPersonalInfoController::class, 'update']);
        });

        Route::group(['prefix' => 'educational_details'], function () {
            Route::post('/', [UserEducationalAndtrainingController::class, 'store']);
            Route::put('/{user_id}', [UserEducationalAndtrainingController::class, 'update']);
            Route::get('/', [UserEducationalAndtrainingController::class, 'showEducationDetails']);
        });


        Route::group(['prefix' => 'training_details'], function () {
            Route::post('/', [UserEducationalAndtrainingController::class, 'storeTrainingDetails']);
            Route::put('/{user_id}', [UserEducationalAndtrainingController::class, 'updateTrainingDetails']);
            Route::get('/', [UserEducationalAndtrainingController::class, 'showTrainingDetails']);

        });
        /* optional
        Route::post('/certification_and_licensure', [UserCertificationAndLicenseController::class, 'storeCertificationAndLicensureDetail']);
        Route::put('/certification_and_licensure/{user_id}', [UserCertificationAndLicenseController::class, 'updateCertificationAndLicensureDetail']);
        */
        Route::group(['prefix' => 'work_history'], function () {
            Route::post('/', [UserWorkHistoryController::class, 'store']);
            Route::get('/', [UserWorkHistoryController::class, 'show']);
            Route::put('/{user_id}', [UserWorkHistoryController::class, 'update']);
            Route::delete('/{id}', [UserWorkHistoryController::class, 'destroy']);
        });
        Route::group(['prefix' => 'disciplinary_actions'], function () {
            Route::post('/', [UserDisciplinaryActionController::class, 'store']);
            Route::put('/{user_id}', [UserDisciplinaryActionController::class, 'update']);
            Route::get('/', [UserDisciplinaryActionController::class, 'show']);
        });
        /*Route::post('/work_history', [UserWorkHistoryController::class, 'store']);
        Route::put('/work_history/{user_id}', [UserWorkHistoryController::class, 'update']);
        Route::delete('/work_history/{id}', [UserWorkHistoryController::class, 'destroy']);
        Route::get('/work_history', [UserWorkHistoryController::class, 'show']);*/
        Route::group(['prefix' => 'mal_practice'], function () {
            Route::post('/', [UserMalPracticeController::class, 'store']);
            Route::put('/{user_id}', [UserMalPracticeController::class, 'update']);
            Route::delete('/{id}', [UserMalPracticeController::class, 'destroy']);
            Route::get('/', [UserMalPracticeController::class, 'show']);
        });
        Route::group(['prefix' => 'user_references'], function () {
            Route::post('/', [UserWorkReferencesController::class, 'store']);
            Route::put('/{user_id}', [UserWorkReferencesController::class, 'update']);
            Route::delete('/{id}', [UserWorkReferencesController::class, 'destroy']);
            Route::get('/', [UserWorkReferencesController::class, 'show']);

        });
        Route::group(['prefix' => 'hospitalist_profile'], function () {
            Route::post('/', [UserHospitalistProfileController::class, 'store']);
            Route::put('/{user_id}', [UserHospitalistProfileController::class, 'update']);
            Route::get('/', [UserHospitalistProfileController::class, 'show']);
        });

        Route::group(['prefix' => 'general_surgery'], function () {
            Route::post('/', [UserGeneralSurgeryController::class, 'store']);
            Route::put('/{user_id}', [UserGeneralSurgeryController::class, 'update']);
            Route::get('/', [UserGeneralSurgeryController::class, 'show']);
        });

        Route::group(['prefix' => 'board_certification'], function () {
            Route::post('/', [UserCertificationAndLicenseController::class, 'storeBoardCertificationDetail']);
            Route::put('/{user_id}', [UserCertificationAndLicenseController::class, 'updateBoardCertificationDetail']);
            Route::get('/', [UserCertificationAndLicenseController::class, 'showBoardCertificationDetail']);
        });

        Route::group(['prefix' => 'clinical_certification'], function () {
            Route::post('/', [UserCertificationAndLicenseController::class, 'storeClinicalCertificationDetail']);
            Route::put('/{user_id}', [UserCertificationAndLicenseController::class, 'updateClinicalCertificationDetail']);
            Route::get('/', [UserCertificationAndLicenseController::class, 'showClinicalCertificationDetail']);
        });
        Route::group(['prefix' => 'license_issuer'], function () {
            Route::post('/', [UserCertificationAndLicenseController::class, 'storeLicenseIssuerDetail']);
            Route::put('/{user_id}', [UserCertificationAndLicenseController::class, 'updateLicenseIssuerDetail']);
            Route::get('/', [UserCertificationAndLicenseController::class, 'showLicenseIssuerDetail']);
        });


        Route::get('specialities', [SpecialitiesController::class, 'all']);
        Route::get('invoiceList', [UserJobsController::class, 'invoiceList']);
        Route::post('invoice/acknowledgePaidJobInvoice/{user_job_invoice}', [UserJobsController::class, 'acknowledgePaidJobInvoice']);

        Route::group(['prefix' => 'statistics'], function () {
            Route::get('/cards', [StatisticsController::class, 'jobsAppliedCounts']);
            Route::get('/application-status', [StatisticsController::class, 'fetchApplicationStatus']);
            Route::get('/upcomingInterviews', [StatisticsController::class, 'fetchUpcomingInterviews']);
        });

        Route::group(['prefix' => 'account'], function () {
            Route::get('employment-preference', [EmploymentPreferenceController::class, 'fetchEmploymentPreference']);
            Route::put('employment-preference/update', [EmploymentPreferenceController::class, 'updateEmploymentPreference']);
            Route::get('bank-details', [PhysicianBankDetailsController::class, 'fetchBankDetails']);
            Route::put('bank-details/update', [PhysicianBankDetailsController::class, 'updateBankDetails']);
        });

        Route::group(['prefix' => 'jobs'], function () {
            Route::get('/details/{jobs_id}', [PhysicianJobsController::class, 'show']);
            Route::get('/list', [PhysicianJobsController::class, 'index']);
            Route::get('/', [UserJobsController::class, 'index']);
            Route::get('/{user_job_id}', [UserJobsController::class, 'show']);
            Route::post('generateInvoice', [UserJobsController::class, 'generateUserJobInvoice']);
            Route::post('/', [UserJobsController::class, 'applyToJobs']);
            Route::post('/feedBack', [UserJobsController::class, 'jobsFeedBack']);
            Route::get('/feedBack/{user_job_id}', [UserJobsController::class, 'getJobsFeedBack']);
            Route::post('/save-unsave-job/{job_id}', [UserJobsController::class, 'saveUnSaveJob']);
            Route::post('/cancel/{job_id}', [UserJobsController::class, 'cancelJob']);
        });
    });


});
Route::get('/health_api', [HealthApiController::class, 'index']);
Route::get('user/verify/email', [UserController::class, 'verifyEmailLink']);
Route::get('hospital/verify/email', [HospitalController::class, 'verifyEmailLink']);
Route::get('password/reset-password', [ForgotPasswordController::class, 'showResetPasswordForm']);
Route::post('update_password', [ForgotPasswordController::class, 'updatePassword'])->name('updatePassword');
